package communicators;

import java.net.URI;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class CommunicatorUtilities {
	private static CommunicatorUtilities communicatorInstance;
	private static final String URI = "http://localhost:5055/";
	
	public static synchronized CommunicatorUtilities getInstance() {
	        if (communicatorInstance == null) {
	            communicatorInstance = new CommunicatorUtilities();
	        }
	        return communicatorInstance;
	    }
	  
	  
	  public String getMethod(String path, String queryName, int queryVal) {
		  Client client = Client.create(new DefaultClientConfig());
	        URI uri = UriBuilder.fromUri(URI).build();        
	        ClientResponse response = client.resource(uri).path(path)
	        		.queryParam(queryName, String.valueOf(queryVal))
	                .type(MediaType.APPLICATION_JSON)
	                .get(ClientResponse.class);
	        return response.getEntity(String.class);
	  }
	  
	  public String getMethod(String path1, String path2,String path3, String queryName, int queryVal) {
		  Client client = Client.create(new DefaultClientConfig());
	        URI uri = UriBuilder.fromUri(URI).build();        
	        ClientResponse response = client.resource(uri).path(path1).path(path2).path(path3)
	        		.queryParam(queryName, String.valueOf(queryVal))
	                .type(MediaType.APPLICATION_JSON)
	                .get(ClientResponse.class);
	        return response.getEntity(String.class);
	  }
	  
	  public String getMethod(String path1, String path2) {
		  Client client = Client.create(new DefaultClientConfig());
	        URI uri = UriBuilder.fromUri(URI).build();        
	        ClientResponse response = client.resource(uri).path(path1)
	        		.path(path2)
	                .type(MediaType.APPLICATION_JSON)
	                .get(ClientResponse.class);
	        return response.getEntity(String.class);
	  }
	  
	  public String getMethod(String path) {
		  Client client = Client.create(new DefaultClientConfig());
	        URI uri = UriBuilder.fromUri(URI).build();        
	        ClientResponse response = client.resource(uri).path(path)
	                .type(MediaType.APPLICATION_JSON)
	                .get(ClientResponse.class);
	        return response.getEntity(String.class);
	  }
	  
	  public String getMethod(String path1, String path2, String queryName, int queryVal) {
		  Client client = Client.create(new DefaultClientConfig());
	        URI uri = UriBuilder.fromUri(URI).build();        
	        ClientResponse response = client.resource(uri).path(path1).path(path2)
	        		.queryParam(queryName, String.valueOf(queryVal))
	                .type(MediaType.APPLICATION_JSON)
	                .get(ClientResponse.class);
	        return response.getEntity(String.class);
	  }
	  
	  public String getMethod(String path1, String path2, String path3) {
		  Client client = Client.create(new DefaultClientConfig());
	        URI uri = UriBuilder.fromUri(URI).build();        
	        ClientResponse response = client.resource(uri).path(path1)
	        		.path(path2).path(path3)
	                .type(MediaType.APPLICATION_JSON)
	                .get(ClientResponse.class);
	        return response.getEntity(String.class);
	  }
}
