package teal.chatbot.controllers;

import java.io.IOException;
import java.util.Locale;

import org.springframework.ui.Model;

import teal.chatbot.backoffice.Communicator;
import teal.chatbot.backoffice.Utilities;
import teal.chatbot.beans.Application;
import teal.chatbot.beans.Profile;
import teal.chatbot.beans.User;

public class UserController {

	private static UserController userInstance;
    
    public static synchronized UserController getInstance() {
        if (userInstance == null) {
        	userInstance = new UserController();
        }
        return userInstance;
    }
      
    public User[] displayUsersApp(Model model,String app){  
    	User[] users=null;
		if(app.equals("all")) {
    		users=Communicator.getInstance().getUsers();
    	}else {
    		users=Communicator.getInstance().getUsersApp(app);
    	}

        return users;  
    } 
    
    public String displayUsers(Model model){  
    	User[] users=Communicator.getInstance().getUsers();
	   Profile[] profiles=Communicator.getInstance().getProfiles();
	   Application[] apps=Communicator.getInstance().getApps();
	   Application[] appsMod=Communicator.getInstance().getAppsNotAdmin();

		model.addAttribute("appsMod", appsMod);
		model.addAttribute("profiles", profiles);
		model.addAttribute("users", users);
		model.addAttribute("apps", apps);

        return "utilisateur";  
    }   

    public String addUser(Locale locale,Model model,String loginUserconnecte,String login,String email,String mdp,String profil,String application) throws IOException{  	
		User u=new User();
		u.setLogin(login);
		u.setEmail(email);
		u.setMdp(mdp);
		u.setProfil(Communicator.getInstance().getProfile(Long.parseLong(profil)));
		if(application.equals("Administration")) {
			u.setApplication(Communicator.getInstance().getApp(1));
			Communicator.getInstance().addUser(u);
			Utilities.getInstance().ajouterHistorique(locale,"Ajouter Utilisateur", login, loginUserconnecte,String.valueOf(1));
		}else {
			u.setApplication(Communicator.getInstance().getApp(Long.parseLong(application)));
			Communicator.getInstance().addUser(u);
			Utilities.getInstance().ajouterHistorique(locale,"Ajouter Utilisateur", login, loginUserconnecte,application);
		}
		return "redirect:/utilisateurs";   
    } 
    
    public String deleteUser(Locale locale,Model model,String loginUserconnecte,String login) throws IOException{  
		User u=Communicator.getInstance().getUserProfile(login);	
		Communicator.getInstance().deleteUser(login);
		if(u.getApplication().getName().equals("Administration")) {
			Utilities.getInstance().ajouterHistorique(locale,"Supprimer Utilisateur", login, loginUserconnecte,String.valueOf(1));
		}else {
			Utilities.getInstance().ajouterHistorique(locale,"Supprimer Utilisateur", login, loginUserconnecte,String.valueOf(u.getApplication().getId_application()));
		}
		return "redirect:/utilisateurs";  
    } 

    public String updateUser(Locale locale,String loginUserconnecte,String login,String email,String mdp,
			String profil,String application,String loginDatabase) throws IOException{  
		User u=new User();
		u.setLogin(login);
		u.setEmail(email);
		u.setMdp(mdp);
		u.setProfil(Communicator.getInstance().getProfile(Long.parseLong(profil)));
		if(application.equals("Administration")) {
			u.setApplication(Communicator.getInstance().getApp(1));
			Communicator.getInstance().updateUser(u,loginDatabase);
			Utilities.getInstance().ajouterHistorique(locale,"Mettre � jour Utilisateur", login, loginUserconnecte,String.valueOf(1));
		}else {
			u.setApplication(Communicator.getInstance().getApp(Long.parseLong(application.substring(0, 1))));
			Communicator.getInstance().updateUser(u,loginDatabase);
			Utilities.getInstance().ajouterHistorique(locale,"Mettre � jour Utilisateur", login, loginUserconnecte,application.substring(0, 1));
		}
		return "redirect:/utilisateurs";
	}

    public User[] displayUsersList() throws IOException {  
    	String login="";
		 User[] users=Communicator.getInstance().getUsersSearch(login);
		return users;  
    }   
    
    public User[] displayUsersListByLogin(Model model,String login) throws IOException{  
		 User[] users=Communicator.getInstance().getUsersSearch(login);
		return users;  
    }   
}
