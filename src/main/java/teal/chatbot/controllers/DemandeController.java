package teal.chatbot.controllers;

import java.io.IOException;
import java.util.Locale;
import org.springframework.ui.Model;
import teal.chatbot.backoffice.Communicator;
import teal.chatbot.backoffice.Utilities;
import teal.chatbot.beans.Application;
import teal.chatbot.beans.Demande;

public class DemandeController {

	private static DemandeController demandeInstance;
    
    public static synchronized DemandeController getInstance() {
        if (demandeInstance == null) {
        	demandeInstance = new DemandeController();
        }
        return demandeInstance;
    }
    
    public String displayRequests(Model model, int page) throws IOException{  
		Demande[] listeDemande=Communicator.getInstance().getDemandes(page);
		Application[] listeApp=Communicator.getInstance().getApps();
		model.addAttribute("apps", listeApp);
		model.addAttribute("listeDemande", listeDemande);
        return "demande";  
    }  
	
    public Demande[] displayDemandesApp(Model model,String app, int page) throws IOException{  
    	Demande[] listeDemande=null;
		if(app.equals("all")) {
			listeDemande=Communicator.getInstance().getDemandes(page);
    	}else {
    		listeDemande=Communicator.getInstance().getDemandesApp(app,page);
    	}
        return listeDemande;  
    }  

    public String updateRequests(Locale locale,String loginUserconnecte,String id_demande) throws IOException  {  
    	Demande d=Communicator.getInstance().getDemandeById(id_demande);
    	d.setId_demande(Long.valueOf(id_demande));
		d.setTraitee(true);
		Communicator.getInstance().updateDemande(d);
		Utilities.getInstance().ajouterHistorique(locale,"R�soudre demande", d.getDemande(), loginUserconnecte,String.valueOf(d.getApplication().getId_application()));
		return "redirect:/demandes?page=0";  
    }

	public String nbrPagedisplayRequests(Model model,int page) {
		return Communicator.getInstance().nbrPagedisplayRequests(page);
	}

	public String nbrPagedisplayDemandessApp(Model model, String app, int page) throws IOException {
		return Communicator.getInstance().nbrPagedisplayDemandessApp(app,page);
	}

	

	
	
	
    
    
}
