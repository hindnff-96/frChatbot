package teal.chatbot.controllers;

import java.io.IOException;
import java.sql.Blob;
import java.util.Locale;
import org.springframework.ui.Model;
import teal.chatbot.backoffice.Communicator;
import teal.chatbot.backoffice.Utilities;
import teal.chatbot.beans.Application;
import teal.chatbot.beans.Branche;
import teal.chatbot.beans.DecisionTree;
import teal.chatbot.beans.EtapeBranche;
import teal.chatbot.beans.Image;
import teal.chatbot.beans.MotCle;
import teal.chatbot.beans.User;


public class RootsController {

private static RootsController rootInstance;
    
    public static synchronized RootsController getInstance() {
        if (rootInstance == null) {
        	rootInstance = new RootsController();
        }
        return rootInstance;
    }
    
    public Branche consulterBranche(String id) throws IOException {
		return Communicator.getInstance().searchBrunchById(id);
	} 
    
	public String displayDashboard(Model model,int page) throws IOException{  
		Branche[] branches=Communicator.getInstance().getRoots(page);
		Application[] listeAppAd=Communicator.getInstance().getApps();
		model.addAttribute("apps", listeAppAd);
		
		Application[] listeApp=Communicator.getInstance().getAppsNotAdmin();
		
		model.addAttribute("appsAddRacine", listeApp);
		model.addAttribute("branches", branches);
		return "racines"; 
    }  
    
    public String displayNbrPagesRoots(Model model,int page, String app) throws IOException{  
    	String pages="";
    	if(app.equals("Administration")) {
    		pages=Communicator.getInstance().getRootsNbrPages(page);
    	}else {
    		pages=Communicator.getInstance().getRootsNbrPagesByApp(page, app);
    	}
			
		return pages; 
    } 
    
    public Branche[] displayRoots(int page, String app) throws IOException{  
    	if(app.equals("Administration")) {
			return Communicator.getInstance().getRoots(page);
    	}else {
    		return Communicator.getInstance().getRootsByApp(page, app);
    	}
    }

	public String addRoot(Locale locale, Model model, String loginUserconnecte, String racine,
			String application) throws IOException {
		Branche b=new Branche();
		b.setApplication(Communicator.getInstance().getApp(Long.parseLong(application)));
		b.setTitre(racine);
		b.setDescription("");
		b.setRacine(true);
		b.setNiveau(1);
		Communicator.getInstance().addRoot(b);
		Utilities.getInstance().ajouterHistorique(locale,"Ajouter Racine", racine, loginUserconnecte,application);
		return "redirect:/arbreDecisionnel?page=0";  
	}

	public Branche[] searchRoots(int page,String titre) throws IOException {
		return Communicator.getInstance().searchRoots(page,titre);
	}
    
	public String displayNbrPagesSearchRoots(int page, String titre) throws IOException{  
    	String pages="";
    		pages=Communicator.getInstance().getRootsNbrsearchRoots(page, titre);	
		return pages; 
    }

	public Branche searchTree(String titre) throws IOException {
		return Communicator.getInstance().searchTree(titre);
	}

	public String deleteBranche(Locale locale, Model model, String loginUserconnecte, String branche, String brancheDEletedName, String root) throws IOException {
		User u=Communicator.getInstance().getUserProfile(loginUserconnecte);
		Utilities.getInstance().ajouterHistorique(locale,"Supprimer Branche", brancheDEletedName, loginUserconnecte,String.valueOf(u.getApplication().getId_application()));
		Branche br=Communicator.getInstance().searchBrunchById(branche);
		Communicator.getInstance().deletBranche(locale, loginUserconnecte, branche);
		if(br.isRacine()) {
			return "redirect://arbreDecisionnel?page=0";
		}else {
			return "redirect://arbreDecisionnel/racine/"+root;
		}
		
	}

	public String updateBranche(Locale locale, Model model, String loginUserconnecte, String brancheUpdated,
			String brancheUpdatedName, String titre, String description, String principale, String root) throws IOException {
		Branche b=Communicator.getInstance().searchBrunchById(brancheUpdated);
		b.setPrincipale(Boolean.valueOf(principale));
		b.setTitre(titre);
		b.setDescription(description);
		b.setId_branche(Long.valueOf(brancheUpdated));
		Utilities.getInstance().ajouterHistorique(locale,"Modifier Branche", brancheUpdatedName, loginUserconnecte,String.valueOf(b.getApplication().getId_application()));
		Communicator.getInstance().updateBranche(b);
		return "redirect://arbreDecisionnel/racine/"+root;
		//return "redirect://branche/"+brancheUpdated;
	}

	public String addNvBranche(Locale locale, Model model, String loginUserconnecte, String brunchSlectedAdd,
			String brancheAddedName, String titre, String description, String root) throws IOException {
		Branche superB=Communicator.getInstance().searchBrunchById(brunchSlectedAdd);
		Branche b=new Branche();
		b.setTitre(titre);
		b.setDescription(description);
		b.setRacine(false);
		b.setApplication(superB.getApplication());
		b.setRacine(false);
		b.setNiveau(0);
		Utilities.getInstance().ajouterHistorique(locale,"Ajouter Nouvelle Branche", titre, loginUserconnecte,String.valueOf(superB.getApplication().getId_application()));
		Communicator.getInstance().addNvBranche(brunchSlectedAdd,b);
		return "redirect://arbreDecisionnel/racine/"+root;
	}

	public Branche[] displayBrunches(String app) throws IOException {
		return Communicator.getInstance().displayBrunches(app);
	}

	public String addExBranche(Locale locale, Model model, String loginUserconnecte, String brunchSlectedAdd,
			String brancheAddedName, String branche, String root) throws IOException {		
		Branche superB=Communicator.getInstance().searchBrunchById(brunchSlectedAdd);
		Branche sousB=Communicator.getInstance().searchBrunchById(branche);
		Utilities.getInstance().ajouterHistorique(locale,"Ajouter Branche Existante", sousB.getTitre(), loginUserconnecte,String.valueOf(superB.getApplication().getId_application()));
		Communicator.getInstance().addExBranche(brunchSlectedAdd,branche);
		return "redirect://arbreDecisionnel/racine/"+root;
	}

	public String addStep(Locale locale,String loginUserconnecte, String brancheAddStep, String brancheAddStepName,String num,
			String stepDescription, String imagePath, String imageTitre) throws IOException {
		Image image=new Image();
		if(imagePath.equals("")) {
			image=null;
		}else {
			image.setUrl(imagePath);
			image.setTitre(imageTitre);
		}

			EtapeBranche etape=new EtapeBranche();
			etape.setBranche(Communicator.getInstance().searchBrunchById(brancheAddStep));
			etape.setDescription(stepDescription);
			etape.setNum_etape(Long.valueOf(num));
			etape.setImage(image);
			Utilities.getInstance().ajouterHistorique(locale,"Ajouter Une Nouvelle Etape pour ", brancheAddStepName, loginUserconnecte,String.valueOf(Communicator.getInstance().searchBrunchById(brancheAddStep).getApplication().getId_application()));
			Communicator.getInstance().addStep(etape);
			return "redirect://branche/"+brancheAddStep;
	}

	public EtapeBranche[] displaySteps(String id) {
		
		return Communicator.getInstance().displaySteps(id);
	}

	public String deleteStep(Locale locale, String loginUserconnecte, String stepDeleted,
			String brancheDeeleteStepName, String brancheDeeleteStep) throws IOException {
		Utilities.getInstance().ajouterHistorique(locale,"Supprimer Etape pour ", brancheDeeleteStepName, loginUserconnecte,String.valueOf(Communicator.getInstance().searchBrunchById(brancheDeeleteStep).getApplication().getId_application()));
		Communicator.getInstance().deleteStep(locale, stepDeleted);
		return "redirect://branche/"+brancheDeeleteStep;
		
	}

	public String updateStep(Locale locale, String loginUserconnecte, String stepUpdated, String brancheUpStepName,String brancheUpStep,
			String num, String stepDescription, String imagePath, String imageTitre) throws IOException {
		Utilities.getInstance().ajouterHistorique(locale,"Modifier Etape pour ", brancheUpStepName, loginUserconnecte,String.valueOf(Communicator.getInstance().searchBrunchById(brancheUpStep).getApplication().getId_application()));
		Image image=new Image();
		if(imagePath.equals("")) {
			image=null;
		}else {
			image.setUrl(imagePath);
			image.setTitre(imageTitre);
		}

		EtapeBranche etape=new EtapeBranche();
		etape.setBranche(Communicator.getInstance().searchBrunchById(brancheUpStep));
		etape.setDescription(stepDescription);
		etape.setNum_etape(Long.valueOf(num));
		etape.setId_etape(Long.valueOf(stepUpdated));
		etape.setImage(image);
		Communicator.getInstance().updateStep(etape);
		return "redirect://branche/"+brancheUpStep;
	}

	public String addKeyword(Locale locale, String loginUserconnecte, String brancheAddStep, String brancheAddStepName,
			String keyword) throws IOException {
			
			System.out.println(keyword);
			MotCle motCle=new MotCle();
			motCle.setMotCle(keyword);
			motCle.setBranche(Communicator.getInstance().searchBrunchById(brancheAddStep));
			Utilities.getInstance().ajouterHistorique(locale,"Ajouter Un mot cl� pour ", brancheAddStepName, loginUserconnecte,String.valueOf(Communicator.getInstance().searchBrunchById(brancheAddStep).getApplication().getId_application()));
			
			Communicator.getInstance().addKeyword(motCle);
			return "redirect://branche/"+brancheAddStep;
	}

	public String updateKeyword(Locale locale, String loginUserconnecte, String keywordUpdated,
			String brancheUpKeywordName, String brancheUpKeyword, String keywordpUp)  throws IOException {
			Utilities.getInstance().ajouterHistorique(locale,"Modifier Mot Cl� pour ", brancheUpKeywordName, loginUserconnecte,String.valueOf(Communicator.getInstance().searchBrunchById(brancheUpKeyword).getApplication().getId_application()));
			MotCle motCle=new MotCle();
			motCle.setBranche(Communicator.getInstance().searchBrunchById(brancheUpKeyword));
			motCle.setMotCle(keywordpUp);
			motCle.setId_motCle(Long.valueOf(keywordUpdated));
			Communicator.getInstance().updateKeyword(motCle);
			return "redirect://branche/"+brancheUpKeyword;
	}

	public String deleteKeyword(Locale locale, String loginUserconnecte, String keywordDeleted,
			String brancheDeeleteKeywordName, String brancheDeeleteKeyword) throws IOException {
		Utilities.getInstance().ajouterHistorique(locale,"Supprimer mot cl� pour ", brancheDeeleteKeywordName, loginUserconnecte,String.valueOf(Communicator.getInstance().searchBrunchById(brancheDeeleteKeyword).getApplication().getId_application()));
		Communicator.getInstance().deleteKeyword(locale, keywordDeleted);
		return "redirect://branche/"+brancheDeeleteKeyword;
	}

	public String removeBranche(Locale locale, Model model, String loginUserconnecte, String idRelation ,String superBranche,String root) throws IOException {
		User u=Communicator.getInstance().getUserProfile(loginUserconnecte);
		Branche  br=Communicator.getInstance().searchBrunchById(superBranche);
		DecisionTree relation=Communicator.getInstance().getDecisionTree(idRelation);
		Utilities.getInstance().ajouterHistorique(locale,"Annuler Relation pour: "+ br.getTitre(), relation.getSousBranche().getTitre(), loginUserconnecte,String.valueOf(u.getApplication().getId_application()));
		Communicator.getInstance().removeRelation(idRelation);
		return "redirect://arbreDecisionnel/racine/"+root;
	}

	

}
