package teal.chatbot.controllers;

import java.io.IOException;
import java.util.Locale;

import org.springframework.ui.Model;
import teal.chatbot.backoffice.Communicator;
import teal.chatbot.backoffice.Utilities;
import teal.chatbot.beans.User;

public class ProfileUserController {

	private static ProfileUserController profileUserInstance;
    
    public static synchronized ProfileUserController getInstance() {
        if (profileUserInstance == null) {
        	profileUserInstance = new ProfileUserController();
        }
        return profileUserInstance;
    }
      
    public String displayUserProfile(Model model,String login) throws IOException {  
		User user=Communicator.getInstance().getUserProfile(login);
		model.addAttribute("user", user);
        return "profilUtilisateur";  
    }  
    
    public String updateProfile(Locale locale,String login, String mdp,String email) throws IOException {
    	User u=Communicator.getInstance().getUserProfile(login);
		u.setLogin(login);
		u.setEmail(email);
		u.setMdp(mdp);
		if(u.getApplication().getName().equals("Administration")) {
			u.setApplication(Communicator.getInstance().getApp(1));
			Communicator.getInstance().updateUser(u,login);
			Utilities.getInstance().ajouterHistorique(locale,"Mettre � jour Utilisateur", login, login,String.valueOf(1));
		}else {
			try {
				Communicator.getInstance().updateUser(u,login);
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				Utilities.getInstance().ajouterHistorique(locale,"Mettre � jour Utilisateur", login, login,String.valueOf(u.getApplication().getId_application()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "redirect://profil/"+login;
    }
}
