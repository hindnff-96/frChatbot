package teal.chatbot.controllers;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.ui.Model;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import teal.chatbot.backoffice.Communicator;
import teal.chatbot.beans.Application;
import teal.chatbot.beans.Historique;
import teal.chatbot.beans.HistoriqueRecherche;
import teal.chatbot.beans.Utilisation;
import teal.chatbot.beans.UtilisationCount;

public class DashboardController {

	private static DashboardController dashboardInstance;
    
    public static synchronized DashboardController getInstance() {
        if (dashboardInstance == null) {
        	dashboardInstance = new DashboardController();
        }
        return dashboardInstance;
    }
    
    public String displayDashboard(Model model, int page) throws IOException{  
		Historique[] historique=Communicator.getInstance().getHistoriqueBackOffice(page);
		Application[] listeApp=Communicator.getInstance().getApps();
		model.addAttribute("apps", listeApp);
		model.addAttribute("historiqueBackoffice", historique);
        return "dashboard";  
    }  
    
    public Historique[] displayDashboardByApp(Model model,String app, int page) throws IOException{
    	Historique[] listeHistorique=null;
		if(app.equals("Administration")) {
			listeHistorique=Communicator.getInstance().getAllHistoriqueBackOffice(page);
    	}else {
    		listeHistorique=Communicator.getInstance().getAllHistoriqueBackOfficeApp(app, page);
    	}

        return listeHistorique;  
    }

	

	public String displayDashboardByAppBackOfficeNbr(Model model, String app, int page) {
		if(app.equals("Administration")) {
			return Communicator.getInstance().getNbrPagesBackOffice(page);
		}else {
			return Communicator.getInstance().displayDashboardByAppBackOfficeNbr(app,page);
		}
		
	}

	public String displayHistChatbotNBR(Model model, String app, int page) {
		if(app.equals("Administration")) {
			return Communicator.getInstance().getNbrPagesChatbot(page);
		}else {
			return Communicator.getInstance().displayDashboardByAppChatbotNbr(app,page);
		}
	}

	public HistoriqueRecherche[] displayDashboardByAppBackOffice(Model model, String app, int page) throws IOException {
		HistoriqueRecherche[] listeHistorique=null;
		if(app.equals("Administration")) {
			listeHistorique=Communicator.getInstance().getAllHistoriqueChatbot(page);
    	}else {
    		listeHistorique=Communicator.getInstance().getAllHistoriqueChatbotApp(app, page);
    	}

        return listeHistorique;
	}

	public UtilisationCount[] displaychatbotChartBackOffice(String app) throws IOException, JSONException {
		if(app.equals("Administration")) {
			return Communicator.getInstance().getchartChatbot();
			/*String json=Communicator.getInstance().getchartChatbot().toString();
			System.out.println(json);
			JSONObject jObject = new JSONObject(json);
			JSONArray jsonArray = jObject.getJSONArray("utilisations");
			System.out.println(jsonArray.toString());
			return jsonArray;*/
			/*for (int i = 0; i < jsonArray.length(); i++) {
			    JSONObject explrObject = jsonArray.getJSONObject(i);
			    System.out.println(explrObject.get("date").toString());
			    System.out.println(explrObject.get("count").toString());
			}*/
			//System.out.println(x);
			//return Communicator.getInstance().getchartChatbot();
    	}else { 
    		return Communicator.getInstance().getchartChatbotApp(app);
    	}

	}

	public UtilisationCount[] displaychatbotChartBackOfficeByApps() throws IOException {
		return Communicator.getInstance().displaychatbotChartBackOfficeByApps();
	}

	public String displayStatsBranches(String app) {
		if(app.equals("Administration")) {
			return Communicator.getInstance().displayStatsBranches();
    	}else { 
    		return Communicator.getInstance().displayStatsBranchesApp(app);
    	}
	}

	public String displayStatsDemandes(String app) {
		if(app.equals("Administration")) {
			return Communicator.getInstance().displayStatsDemandes();
    	}else { 
    		return Communicator.getInstance().displayStatsDemandesApp(app);
    	}
	}

	public String displayStatsRecherches(String app) {
		if(app.equals("Administration")) {
			return Communicator.getInstance().displayStatsRecherches();
    	}else { 
    		return Communicator.getInstance().displayStatsRecherchesApp(app);
    	}
	}  
    
    

    
}
