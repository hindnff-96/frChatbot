package teal.chatbot.controllers;

import java.io.IOException;
import java.util.Locale;
import org.springframework.ui.Model;
import teal.chatbot.backoffice.Communicator;
import teal.chatbot.backoffice.Utilities;
import teal.chatbot.beans.Application;
import teal.chatbot.beans.User;

public class ApplicationController {

	private static ApplicationController applicationInstance;
    
    public static synchronized ApplicationController getInstance() {
        if (applicationInstance == null) {
        	applicationInstance = new ApplicationController();
        }
        return applicationInstance;
    }
    
    public String displayApps(Model model){  
    	Application[] listeAppAd=Communicator.getInstance().getApps();
    	
    	Application[] listeApp=Communicator.getInstance().getAppsNotAdmin();
		
		model.addAttribute("apps", listeApp);
		
		model.addAttribute("listeApp", listeAppAd);
        return "applications";  
    }
  
    public String addApp(Locale locale,Model model,String loginUserconnecte,String name, String description) throws IOException{  	
		User u=Communicator.getInstance().getUserProfile(loginUserconnecte);
    	Application a=new Application();
		a.setName(name);
		a.setDescription(description);
		Communicator.getInstance().addApp(a);
		Utilities.getInstance().ajouterHistorique(locale,"Ajouter Application", name, loginUserconnecte,String.valueOf((u.getApplication().getId_application())));
		return "redirect:/apps";   
    }  

    public String deleteApp(Locale locale,Model model,String loginUserconnecte,String name) throws IOException{  
    	User u=Communicator.getInstance().getUserProfile(loginUserconnecte);
    	Communicator.getInstance().deleteApp(name);
		Utilities.getInstance().ajouterHistorique(locale,"Supprimer Application", name, loginUserconnecte, String.valueOf((u.getApplication().getId_application())));
		return "redirect:/apps";  
    } 
    
    public String updateApp(Locale locale,String loginUserconnecte,String name, String description,String appUpdated) throws IOException{  
    	User u=Communicator.getInstance().getUserProfile(loginUserconnecte);
    	Application a=new Application();
		a.setName(name);
		a.setDescription(description);
		Communicator.getInstance().updateApp(a,appUpdated);
		Utilities.getInstance().ajouterHistorique(locale,"Mettre � jour Application", name, loginUserconnecte, String.valueOf((u.getApplication().getId_application())));
		return "redirect:/apps";
    } 

    public Application[] displayListApps() throws IOException{  
    	String name="";
		 Application[] apps=Communicator.getInstance().getAppsSearch(name);
		return apps;  
    }  

    public Application[] displayListAppsSearch(Model model,String name) throws IOException{  
		 Application[] apps=Communicator.getInstance().getAppsSearch(name);
		return apps;  
    } 
    
    
}
