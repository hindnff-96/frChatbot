package teal.chatbot.controllers;

import java.io.IOException;
import org.springframework.ui.Model;
import teal.chatbot.backoffice.Communicator;
import teal.chatbot.beans.Application;
import teal.chatbot.beans.Historique;

public class HistoryController {

	private static HistoryController historyInstance;
    
    public static synchronized HistoryController getInstance() {
        if (historyInstance == null) {
        	historyInstance = new HistoryController();
        }
        return historyInstance;
    }
    
    public String displayHistoriqueByUser(Model model,String login, int page) throws IOException{  
		Historique[] historique=Communicator.getInstance().getHistoriqueByLogin(login, page);
		Application[] listeApp=Communicator.getInstance().getApps();
		model.addAttribute("apps", listeApp);
		model.addAttribute("historique", historique);
		return "historique"; 
    }  
     
    public Historique[] displayHistoriqueByApp(Model model,String login,String app, int page) throws IOException{  
    	Historique[] historique=null;
		if(app.equals("Administration")) {
			historique=Communicator.getInstance().getHistoriqueByLogin(login, page);
		}else {
			historique=Communicator.getInstance().getHistoriqueByLoginApp(login,app,page);
		}
		return historique; 
    } 
    
    public String displayHistoriqueByUserNbrPages(Model model,String login, int page) throws IOException{  
        

		String nbrPages=Communicator.getInstance().getHistoriqueByLoginNbrPages(login, page);
        

		return nbrPages; 
    } 
   
    public String displayHistoriqueByAppNbrPages(Model model,String login,String app, int page) throws IOException{  
    	String pages="";
		if(app.equals("Administration")) {
			pages=Communicator.getInstance().getHistoriqueByLoginNbrPages(login, page);
		}else {
			pages=Communicator.getInstance().getHistoriqueByLoginAppNbrPages(login,app,page);
		}
		return pages; 
    } 
}
