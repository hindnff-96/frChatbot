package teal.chatbot.controllers;

import java.util.ArrayList;
import java.util.List;
import org.springframework.ui.Model;
import teal.chatbot.backoffice.Communicator;
import teal.chatbot.beans.Permission_to_access;
import teal.chatbot.beans.Profile;

public class ProfileController {

	private static ProfileController profileInstance;
    
    public static synchronized ProfileController getInstance() {
        if (profileInstance == null) {
        	profileInstance = new ProfileController();
        }
        return profileInstance;
    }
      
    public String displayProfiles(Model model){  	
    	Profile[] profiles=Communicator.getInstance().getProfiles();
    	/*for(int i=0;i<profiles.length;i++) {
    		String[] stringPermissions=Communicator.getInstance().getDroitsByProfil(String.valueOf(profiles[i].getIdProfil()));
    		List<Permission_to_access> permissions=new ArrayList<Permission_to_access>();
    		for(int j=0;j<stringPermissions.length;j++) {
    			Permission_to_access permission=new Permission_to_access();
    			permission.setDroit(stringPermissions[j]);
    			permissions.add(permission);
    		}
    		profiles[i].setPermissions(permissions);
    	}*/
		model.addAttribute("profiles", profiles);
		return "profil";  
    } 
    
    public String displayProfilesView(){  	
    	
		return "profil";  
    } 
    


    
}
