package teal.chatbot.beans;

import java.util.List;





public class Application{
	
	private Long id_application;
	private String name;
	  private List<User> utilisateurs;
	  private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Application() {
		super();
	}

	public Long getId_application() {
		return id_application;
	}

	public void setId_application(Long id_application) {
		this.id_application = id_application;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<User> getUtilisateurs() {
		return utilisateurs;
	}

	public void setUtilisateurs(List<User> utilisateurs) {
		this.utilisateurs = utilisateurs;
	}

	

	
}
