package teal.chatbot.beans;



import java.io.Serializable;
import java.util.List;


public class Permission_to_access implements Serializable{

	private Long id_droit;
	private String droit;
    private List<Profile> profiles;
	public Long getId_droit() {
		return id_droit;
	}
	public void setId_droit(Long id_droit) {
		this.id_droit = id_droit;
	}
	public String getDroit() {
		return droit;
	}
	public void setDroit(String droit) {
		this.droit = droit;
	}
	public List<Profile> getProfiles() {
		return profiles;
	}
	public void setProfiles(List<Profile> profiles) {
		this.profiles = profiles;
	}
		
}
