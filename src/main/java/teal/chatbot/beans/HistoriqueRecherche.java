package teal.chatbot.beans;


public class HistoriqueRecherche{

	
	private long id_historique_recherche;
	private String recherche;

	private Application application;

	
	public HistoriqueRecherche() {
		super();
	}

	public long getId_historique_recherche() {
		return id_historique_recherche;
	}

	public void setId_historique_recherche(long id_historique_recherche) {
		this.id_historique_recherche = id_historique_recherche;
	}

	public String getRecherche() {
		return recherche;
	}

	public void setRecherche(String recherche) {
		this.recherche = recherche;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}
	
	

}
