package teal.chatbot.beans;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DecisionTree {

	private long id_super_sous_branche;
  
    private Branche superBranche;
   
    private Branche sousBranche;

    
	public DecisionTree() {
		super();
	}

	public Branche getSousBranche() {
		return sousBranche;
	}

	public void setSousBranche(Branche sousBranche) {
		this.sousBranche = sousBranche;
	}

	public void setId_super_sous_branche(long id_super_sous_branche) {
		this.id_super_sous_branche = id_super_sous_branche;
	}

	public void setSuperBranche(Branche superBranche) {
		this.superBranche = superBranche;
	}

	public long getId_super_sous_branche() {
		return id_super_sous_branche;
	}

	public Branche getSuperBranche() {
		return superBranche;
	}
    
	
	
}
