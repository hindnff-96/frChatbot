package teal.chatbot.beans;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EtapeBranche{

	private Long id_etape;
	private String description;
	private Branche branche;
	private Long num_etape;
	private Image image;
	public Image getImage() {
		return image;
	}
	public void setImage(Image image) {
		this.image = image;
	}
	public Long getNum_etape() {
		return num_etape;
	}
	public void setNum_etape(Long num_etape) {
		this.num_etape = num_etape;
	}
	public EtapeBranche() {
		super();
	}
	public Long getId_etape() {
		return id_etape;
	}
	public void setId_etape(Long id_etape) {
		this.id_etape = id_etape;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Branche getBranche() {
		return branche;
	}
	public void setBranche(Branche branche) {
		this.branche = branche;
	}
	
	

}
