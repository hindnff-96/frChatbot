package teal.chatbot.beans;



public class MotCle {

	/**
	 * 
	 */
	private long id_motCle;
	private String motCle;
	private Branche branche;
	public MotCle() {
		super();
	}
	public long getId_motCle() {
		return id_motCle;
	}
	public void setId_motCle(long id_motCle) {
		this.id_motCle = id_motCle;
	}
	public String getMotCle() {
		return motCle;
	}
	public void setMotCle(String motCle) {
		this.motCle = motCle;
	}
	public Branche getBranche() {
		return branche;
	}
	public void setBranche(Branche branche) {
		this.branche = branche;
	}
	
	

}
