package teal.chatbot.beans;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Branche{

	private Long id_branche;
	private String titre;
	private String description;
	private boolean racine;
	private Application application;
    private List<DecisionTree> decisionTree = new ArrayList<DecisionTree>();
    private List<EtapeBranche> etapes= new ArrayList<EtapeBranche>();
    private List<MotCle> keywords= new ArrayList<MotCle>();
    private int niveau;
    
    public int getNiveau() {
		return niveau;
	}

	public void setNiveau(int niveau) {
		this.niveau = niveau;
	}

	public List<MotCle> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<MotCle> keywords) {
		this.keywords = keywords;
	}

	private boolean principale;
	public boolean isPrincipale() {
		return principale;
	}

	public void setPrincipale(boolean principale) {
		this.principale = principale;
	}

	public Branche() {
		super();
	}

	public Long getId_branche() {
		return id_branche;
	}

	public void setId_branche(Long id_branche) {
		this.id_branche = id_branche;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isRacine() {
		return racine;
	}

	public void setRacine(boolean racine) {
		this.racine = racine;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public List<DecisionTree> getDecisionTree() {
		return decisionTree;
	}

	public void setDecisionTree(List<DecisionTree> decisionTree) {
		this.decisionTree = decisionTree;
	}

	public List<EtapeBranche> getEtapes() {
		return etapes;
	}

	public void setEtapes(List<EtapeBranche> etapes) {
		this.etapes = etapes;
	}
	
	
}
