package teal.chatbot.beans;

import java.util.ArrayList;
import java.util.List;

public class Image{

	private long id_image;
	private String titre;
	private String url;
	  private List<EtapeBranche> etapes= new ArrayList<EtapeBranche>();
	 
	public Image() {
		super();
	}
	public long getId_image() {
		return id_image;
	}
	public void setId_image(long id_image) {
		this.id_image = id_image;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	public void setEtapes(List<EtapeBranche> etapes) {
		this.etapes = etapes;
	}
	
	 
	
}
