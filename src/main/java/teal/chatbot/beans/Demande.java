package teal.chatbot.beans;


public class Demande{

	
	private long id_demande;
	private String demande;
	private String date;
	private String email;
	private boolean traitee;
	private Application application;

	
	public Demande() {
		super();
	}
	public long getId_demande() {
		return id_demande;
	}
	public void setId_demande(long id_demande) {
		this.id_demande = id_demande;
	}
	public String getDemande() {
		return demande;
	}
	public void setDemande(String demande) {
		this.demande = demande;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isTraitee() {
		return traitee;
	}
	public void setTraitee(boolean traitee) {
		this.traitee = traitee;
	}
	public Application getApplication() {
		return application;
	}
	public void setApplication(Application application) {
		this.application = application;
	}
	
	
	
	

}
