package teal.chatbot.beans;


public class Historique{
		
	private Long id_historique;
	private String action;
    private String date;
    private User createdBy;
	private Application forApplication;

	
	
	

	public Application getForApplication() {
		return forApplication;
	}

	public void setForApplication(Application forApplication) {
		this.forApplication = forApplication;
	}

	public Historique() {
		super();
	}

	public Historique(Long id_historique, String action, String date, User createdBy) {
		super();
		this.id_historique = id_historique;
		this.action = action;
		this.date = date;
		this.createdBy = createdBy;
	}

	public Long getId_historique() {
		return id_historique;
	}

	public void setId_historique(Long id_historique) {
		this.id_historique = id_historique;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	
	
	
	
}
