package teal.chatbot.beans;


public class Utilisation{

	/**
	 * 
	 */
	
	private long id_utilisation;
	private String date;
	private Application application;
	
	
	public Utilisation() {
		super();
	}
	public long getId_utilisation() {
		return id_utilisation;
	}
	public void setId_utilisation(long id_utilisation) {
		this.id_utilisation = id_utilisation;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Application getApplication() {
		return application;
	}
	public void setApplication(Application application) {
		this.application = application;
	}
	
	

}
