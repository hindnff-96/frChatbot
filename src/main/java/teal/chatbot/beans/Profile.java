package teal.chatbot.beans;

import java.util.List;



public class Profile {
	
	private Long id_profil;
	private String profil;
	private List<Permission_to_access> permissions;

	  public List<Permission_to_access> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<Permission_to_access> permissions) {
		this.permissions = permissions;
	}

	public Profile() {
		super();
	}

	public Long getIdProfil() {
		return id_profil;
	}


	public void setIdProfil(Long idProfil) {
		this.id_profil = idProfil;
	}


	public String getProfil() {
		return profil;
	}


	public void setProfil(String profil) {
		this.profil = profil;
	}
	
	
	
}
