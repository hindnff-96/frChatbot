package teal.chatbot.backoffice;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.JsonArray;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import teal.chatbot.beans.Application;
import teal.chatbot.beans.Branche;
import teal.chatbot.beans.Demande;
import teal.chatbot.beans.EtapeBranche;
import teal.chatbot.beans.Historique;
import teal.chatbot.beans.HistoriqueRecherche;
import teal.chatbot.beans.User;
import teal.chatbot.beans.Utilisation;
import teal.chatbot.beans.UtilisationCount;
import teal.chatbot.controllers.ApplicationController;
import teal.chatbot.controllers.DashboardController;
import teal.chatbot.controllers.DemandeController;
import teal.chatbot.controllers.HistoryController;
import teal.chatbot.controllers.ProfileController;
import teal.chatbot.controllers.ProfileUserController;
import teal.chatbot.controllers.RootsController;
import teal.chatbot.controllers.UserController;

@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
	@RequestMapping("login")
	public String login() throws IOException {
		return "login";  
	}
	
	@ResponseBody
	@RequestMapping("getConnection")
	public String getConnection(@RequestParam String login, @RequestParam String password) {
		return Communicator.getInstance().getConnection(login,password);
	}
	
	@RequestMapping("connexion")
	public String connexion(Locale locale,@RequestBody String jsonBody) {
		System.out.println(jsonBody+"   ok  "+jsonBody);
		return null;
		//return Communicator.getInstance().getConnection(login,password);
	}
	
	
	
	//Branche
	@RequestMapping("branche/brancheConf/addKeyword")
	public String addKeyword(Locale locale,@RequestParam String loginUserconnecte,@RequestParam String brancheAddStep,@RequestParam String brancheAddStepName,@RequestParam String keyword) throws IOException {
		return RootsController.getInstance().addKeyword(locale,loginUserconnecte, brancheAddStep, brancheAddStepName,keyword);  
	}
	
	@RequestMapping("branche/brancheConf/updateKeyword")
	public String updateKeyword(Locale locale,@RequestParam String loginUserconnecte,@RequestParam String keywordUpdated,@RequestParam String brancheUpKeywordName,@RequestParam String brancheUpKeyword, @RequestParam String keywordpUp) throws IOException {
		return RootsController.getInstance().updateKeyword(locale,loginUserconnecte, keywordUpdated, brancheUpKeywordName,brancheUpKeyword, keywordpUp);  
	}
	
	@RequestMapping("branche/brancheConf/deleteKeyword")
	public String deleteKeyword(Locale locale,@RequestParam String loginUserconnecte,@RequestParam String keywordDeleted,@RequestParam String brancheDeeleteKeywordName, @RequestParam String brancheDeeleteKeyword) throws IOException {
		return RootsController.getInstance().deleteKeyword(locale,loginUserconnecte, keywordDeleted, brancheDeeleteKeywordName, brancheDeeleteKeyword);  
	}
	
	
	@RequestMapping(value="branche/brancheConf/addStep", consumes = "multipart/form-data")
	public String addStep(Locale locale,@RequestParam String loginUserconnecte,@RequestParam String brancheAddStep,@RequestParam String brancheAddStepName,
			@RequestParam String stepDescription, @RequestParam String num, @RequestParam MultipartFile image) throws IOException {
		String imagePth="";
		System.out.println(image.getOriginalFilename()+"   ok");
		if(!image.getOriginalFilename().equals("")) {
			File temp_file = new File(image.getOriginalFilename());
			imagePth=temp_file.getAbsolutePath();
		}
		
		return RootsController.getInstance().addStep(locale,loginUserconnecte, brancheAddStep, brancheAddStepName,num,stepDescription, imagePth, image.getOriginalFilename()); 

	}
	
	@RequestMapping("{root}/branche/brancheConf/deleteStep")
	public String deleteStep(Locale locale,@RequestParam String loginUserconnecte,@RequestParam String stepDeleted,@RequestParam String brancheDeeleteStepName, @RequestParam String brancheDeeleteStep) throws IOException {
		return RootsController.getInstance().deleteStep(locale,loginUserconnecte, stepDeleted, brancheDeeleteStepName, brancheDeeleteStep);  
	}
	
	@RequestMapping("branche/brancheConf/updateStep")
	public String updateStep(Locale locale,@RequestParam String loginUserconnecte,@RequestParam String stepUpdated,@RequestParam String brancheUpStepName,@RequestParam String brancheUpStep, 
			@RequestParam String num, @RequestParam String stepDescription, @RequestParam MultipartFile image) throws IOException {
		String imagePth="";
		System.out.println(image.getOriginalFilename()+"   ok  hh "+ image);
		if(!image.getOriginalFilename().equals("")) {
			File temp_file = new File(image.getOriginalFilename());
			imagePth=temp_file.getAbsolutePath();
		}
		return RootsController.getInstance().updateStep(locale,loginUserconnecte, stepUpdated, brancheUpStepName,brancheUpStep, num, stepDescription, imagePth, image.getOriginalFilename());  
	}
	
	@RequestMapping("{root}/branche/{id}")  
	public String consulterBranche(Model model, @PathVariable String id, @PathVariable String root) throws IOException{  
		Branche b=RootsController.getInstance().consulterBranche(id);
		model.addAttribute("branche", b);
		Application[] listeApp=Communicator.getInstance().getAppsNotAdmin();
		model.addAttribute("appsAddRacine", listeApp);
		Branche[] branchesApp=RootsController.getInstance().displayBrunches(b.getApplication().getName());
		model.addAttribute("branchesApp", branchesApp);
		EtapeBranche[] etapes=RootsController.getInstance().displaySteps(id);
		model.addAttribute("etapes", etapes);
		model.addAttribute("root", root);
		return "branche"; 
	}
	
	
	@RequestMapping(value="{root}/branche/brancheConf/deleteBranche")
	public String deleteBranche(Locale locale,Model model,@RequestParam String root,@RequestParam(value="loginUserconnecte") String loginUserconnecte,@RequestParam(value="brancheDEleted") String branche,@RequestParam(value="brancheDEletedName") String brancheDEletedName) throws IOException{  
		return RootsController.getInstance().deleteBranche(locale, model, loginUserconnecte, branche, brancheDEletedName,root);  
    } 
	
	@RequestMapping(value="{root}/branche/brancheConf/removeRelation")
	public String RemoveBranche(Locale locale,Model model,@RequestParam String root,@RequestParam String loginUserconnecte,@RequestParam String idRelation, @RequestParam String superBranche) throws IOException{  
				return RootsController.getInstance().removeBranche(locale, model, loginUserconnecte, idRelation,superBranche,root);  
    } 
	
	@RequestMapping(value="{root}/branche/brancheConf/updateBranche")
	public String updateBranche(Locale locale,Model model,@RequestParam String root,@RequestParam String loginUserconnecte,@RequestParam String brancheUpdated,@RequestParam String brancheUpdatedName, @RequestParam String titre, @RequestParam String description, @RequestParam String principale) throws IOException{ 
		return RootsController.getInstance().updateBranche(locale, model, loginUserconnecte, brancheUpdated, brancheUpdatedName,titre, description, principale,root);  
    } 
	
	@RequestMapping(value="{root}/branche/brancheConf/addNvBranche")
	public String addNvBranche(Locale locale,Model model,@RequestParam String root,@RequestParam String loginUserconnecte,@RequestParam String brunchSlectedAdd,@RequestParam String brancheAddedName, @RequestParam String titre, @RequestParam String description) throws IOException{  
		return RootsController.getInstance().addNvBranche(locale, model, loginUserconnecte, brunchSlectedAdd, brancheAddedName,titre, description,root);  
    }
	
	@RequestMapping(value="{root}/branche/brancheConf/addExBranche")
	public String addExBranche(Locale locale,Model model,@RequestParam String root,@RequestParam String loginUserconnecte,@RequestParam String brunchSlectedAdd,@RequestParam String brancheAddedName, @RequestParam String branche) throws IOException{  
		return RootsController.getInstance().addExBranche(locale, model, loginUserconnecte, brunchSlectedAdd, brancheAddedName,branche,root);  
    }
	
	//Arbre Décisionnel
	
	
	@ResponseBody
	@RequestMapping("/arbreDecisionnel/{titre}")  
	public Branche arbreDecisionnel(@PathVariable String titre) throws IOException{  
		return RootsController.getInstance().searchTree(titre);
    } 
	
	@RequestMapping("/arbreDecisionnel/racine/{titre}")  
	public String arbreDecisionnelRcaine(Model model, @PathVariable String titre) throws IOException{  
		model.addAttribute("titre", titre);
		return "arbreRacine"; 
	}
	
	@RequestMapping("/arbreDecisionnel")  
	public String arbreDecisionnel(Model model,@RequestParam int page) throws IOException{  
		return RootsController.getInstance().displayDashboard(model, page);
    } 
	
	@ResponseBody
	@RequestMapping("/arbreDecisionnel/racines/search/{titre}")  
	public Branche[] ArbreDecisionnelSearch(@RequestParam int page,@PathVariable String titre) throws IOException{  
		return RootsController.getInstance().searchRoots(page,titre);
    } 
	
	@ResponseBody
	@RequestMapping("/arbreDecisionnel/racines/search/{titre}/pages")  
	public String ArbreDecisionnelSearchNbrPages(Model model,@RequestParam int page, @PathVariable String titre) throws IOException{  
		return RootsController.getInstance().displayNbrPagesSearchRoots(page,titre);
    }
	
	@ResponseBody
	@RequestMapping("/arbreDecisionnel/racines/{app}")  
	public Branche[] racinesArbreDecisionnel(@RequestParam int page,  @PathVariable String app) throws IOException{  
		return RootsController.getInstance().displayRoots(page, app);
    } 
	
	@ResponseBody
	@RequestMapping("/arbreDecisionnel/racines/{app}/pages")  
	public String arbreDecisionnelNbrPages(Model model,@RequestParam int page, @PathVariable String app) throws IOException{  
		return RootsController.getInstance().displayNbrPagesRoots(model, page,app);
    }
	
	@RequestMapping("/addRoot")
	public String addRoot(Locale locale,Model model,@RequestParam(value="loginUserconnecte") String loginUserconnecte,@RequestParam String racine,@RequestParam String application) throws IOException{  	
		return RootsController.getInstance().addRoot(locale, model, loginUserconnecte, racine, application);   
    }
	
		
	//Dashboard
	@RequestMapping("/dashboard")  
	public String displayDashboard(Model model) throws IOException{  
		return DashboardController.getInstance().displayDashboard(model, 0);
    }   
	
	/*@ResponseBody
	@RequestMapping("/dashboard/histBackOffice/pages")  
	public String displayDashboardBackOfficeNbr(Model model,@RequestParam int page) throws IOException{  
		return DashboardController.getInstance().displayDashboardBackOfficeNbr(model, page);
    } */
	
	@ResponseBody
	@RequestMapping("/dashboard/{app}")  
	public Historique[] displayDashboardByApp(Model model,@PathVariable("app") String app,@RequestParam int page) throws IOException{  
		return DashboardController.getInstance().displayDashboardByApp(model, app,page);
    } 
	
	@ResponseBody
	@RequestMapping("/dashboard/histChatbot/{app}/pages")  
	public String displayHistChatbotNBR(Model model,@PathVariable("app") String app,@RequestParam int page) throws IOException{  
		return DashboardController.getInstance().displayHistChatbotNBR(model, app,page);
    } 
	
	
	@ResponseBody
	@RequestMapping("/dashboard/histBackOffice/{app}/pages")  
	public String displayDashboardByAppBackOfficeNbr(Model model,@PathVariable("app") String app,@RequestParam int page) throws IOException{  
		return DashboardController.getInstance().displayDashboardByAppBackOfficeNbr(model, app,page);
    } 
	
	@ResponseBody
	@RequestMapping("/dashboard/chatbot/{app}")  
	public HistoriqueRecherche[] displayDashboardByAppBackOffice(Model model,@PathVariable("app") String app,@RequestParam int page) throws IOException{  
		return DashboardController.getInstance().displayDashboardByAppBackOffice(model, app,page);
    } 
	
	@ResponseBody
	@RequestMapping("/dashboard/chatbotChat/{app}")  
	public UtilisationCount[] displaychatbotChartBackOffice(@PathVariable("app") String app) throws IOException, JSONException{  
		return DashboardController.getInstance().displaychatbotChartBackOffice(app);
    } 
	
	@ResponseBody
	@RequestMapping("/dashboard/applicationUse")  
	public UtilisationCount[] displaychatbotChartBackOfficeByApps() throws IOException, JSONException{  
		return DashboardController.getInstance().displaychatbotChartBackOfficeByApps();
    } 
	
	@ResponseBody
	@RequestMapping("/dashboard/statistique/branches/{app}")  
	public String displayStatsBranches(@PathVariable("app") String app) throws IOException, JSONException{  
		return DashboardController.getInstance().displayStatsBranches(app);
    } 
	
	@ResponseBody
	@RequestMapping("/dashboard/statistique/demandes/{app}")  
	public String displayStatsDemandes(@PathVariable("app") String app) throws IOException, JSONException{  
		return DashboardController.getInstance().displayStatsDemandes(app);
    } 
	
	@ResponseBody
	@RequestMapping("/dashboard/statistique/recherches/{app}")  
	public String displayStatsRecherches(@PathVariable("app") String app) throws IOException, JSONException{  
		return DashboardController.getInstance().displayStatsRecherches(app);
    } 
	
	//User Profile
	@RequestMapping("/profil/{login}")  
	public String display(Model model,@PathVariable String login) throws IOException  
    {  
        return ProfileUserController.getInstance().displayUserProfile(model, login);
    }
	
	@RequestMapping("profil/up/updateProfile")  
	public String updateProfile(Locale locale,@RequestParam String login, @RequestParam String mdp, @RequestParam String email) throws IOException  {  
        return ProfileUserController.getInstance().updateProfile(locale, login,mdp,email);
    }
	
	//Demandes
	@RequestMapping("/demandes")  
	public String displayRequests(Model model,@RequestParam int page) throws IOException {  
        return DemandeController.getInstance().displayRequests(model,page);
    }
	
	@ResponseBody
	@RequestMapping("/demandes/{app}")  
	public Demande[] displayDemandessApp(Model model,@PathVariable("app") String app,@RequestParam int page) throws IOException  {  
		return DemandeController.getInstance().displayDemandesApp(model, app,page);
    }  
	
	@ResponseBody
	@RequestMapping("/demandes/pages")  
	public String nbrPagedisplayRequests(Model model,@RequestParam int page) throws IOException{ 
		return DemandeController.getInstance().nbrPagedisplayRequests(model,page); 
    }  
	
	@ResponseBody
	@RequestMapping("/demandes/{app}/pages")  
	public String nbrPagedisplayDemandessApp(Model model,@RequestParam int page,@PathVariable String app) throws IOException{ 
		if(app.equals("Administration")) {
			return DemandeController.getInstance().nbrPagedisplayRequests(model,page); 
		}else {
			return DemandeController.getInstance().nbrPagedisplayDemandessApp(model, app,page); 
		}
		
    }
	
	@RequestMapping("/demanderesolue")  
	public String updateRequests(Locale locale,@RequestParam(value="loginUserconnecte") String loginUserconnecte,@RequestParam(value="demande") String demande) throws IOException  {  
		return DemandeController.getInstance().updateRequests(locale, loginUserconnecte, demande);  
    }
		
	//Applications
	@RequestMapping("/apps")  
	public String displayApps(Model model){  
        return ApplicationController.getInstance().displayApps(model);  
    }
	 
	@RequestMapping("/addApp")  
	public String addApp(Locale locale,Model model,@RequestParam(value="loginUserconnecte") String loginUserconnecte,@RequestParam(value="name") String name, @RequestParam String description) throws IOException{  	
		return ApplicationController.getInstance().addApp(locale, model, loginUserconnecte, name, description);   
    }   
	
	@RequestMapping(value="/deleteApp")
	public String deleteApp(Locale locale,Model model,@RequestParam(value="loginUserconnecte") String loginUserconnecte,@RequestParam(value="appSelected") String name) throws IOException{  
		return ApplicationController.getInstance().deleteApp(locale, model, loginUserconnecte, name);  
    } 
	
	@RequestMapping(value="/updateApp")
	public String updateApp(Locale locale,@RequestParam(value="loginConnecte") String loginUserconnecte,@RequestParam(value="name") String name,@RequestParam String description, @RequestParam(value="appUpdated") String appUpdated) throws IOException{  
		return ApplicationController.getInstance().updateApp(locale, loginUserconnecte, name,description, appUpdated);
    } 
	
	@ResponseBody
	@RequestMapping("/getApps")  
	public Application[] displayListApps() throws IOException{  
		return ApplicationController.getInstance().displayListApps();  
    }   	
	
	@ResponseBody
	@RequestMapping("/getApps/{name}")  
	public Application[] displayListAppsSearch(Model model,@PathVariable String name) throws IOException{  
		return ApplicationController.getInstance().displayListAppsSearch(model, name);  
    } 
	
	//Historique
	
	@RequestMapping("/historique/{login}/pages")  
	@ResponseBody
	public String nbrPagedisplayHistorique(Model model,@PathVariable String login,@RequestParam int page) throws IOException{ 
		return HistoryController.getInstance().displayHistoriqueByUserNbrPages(model, login,page); 
    }  
	
	@ResponseBody
	@RequestMapping("/historique/{login}/{app}/pages")  
	public String nbrPagedisplayHistoriqueByApp(Model model,@RequestParam int page,@PathVariable String login,@PathVariable String app) throws IOException{  
		return HistoryController.getInstance().displayHistoriqueByAppNbrPages(model, login, app,page); 
    }
	
	@RequestMapping("/historique/{login}")  
	public String displayHistorique(Model model,@PathVariable String login,@RequestParam int page) throws IOException{ 
		return HistoryController.getInstance().displayHistoriqueByUser(model, login,page); 
    }  
	
	@ResponseBody
	@RequestMapping("/historique/{login}/{app}")  
	public Historique[] displayHistoriqueByApp(Model model,@PathVariable String login,@PathVariable String app,@RequestParam int page) throws IOException{  
		return HistoryController.getInstance().displayHistoriqueByApp(model, login, app,page); 
    }  
	
	//Profils
	@RequestMapping("/profils")  
	public String displayProfiles(){  	
		return ProfileController.getInstance().displayProfilesView();  
    }   

	//Users
	@ResponseBody
	@RequestMapping("/utilisateurs/{app}")  
	public User[] displayUsersApp(Model model,@PathVariable("app") String app){ 
        return UserController.getInstance().displayUsersApp(model, app);  
    }  
	
	@RequestMapping("/utilisateurs")  
	public String displayUsers(Model model){  
        return UserController.getInstance().displayUsers(model);  
    }   
	 
	@RequestMapping("/addUser")  
	public String addUser(Locale locale,Model model,@RequestParam(value="loginUserconnecte") String loginUserconnecte,@RequestParam(value="login") String login,@RequestParam(value="email") String email,@RequestParam(value="mdp") String mdp,
			@RequestParam(value="profil") String profil,@RequestParam(value="application") String application) throws IOException{  	
		return UserController.getInstance().addUser(locale, model, loginUserconnecte, login, email, mdp, profil, application);   
    }   
	
	@RequestMapping(value="/deleteUser")
	public String deleteUser(Locale locale,Model model,@RequestParam(value="loginUserconnecte") String loginUserconnecte,@RequestParam(value="login") String login) throws IOException{  
		return UserController.getInstance().deleteUser(locale, model, loginUserconnecte, login);  
    } 
	
	@RequestMapping(value="/updateUser")
	public String updateUser(Locale locale,@RequestParam(value="loginUserconnecte") String loginUserconnecte,@RequestParam(value="login") String login,@RequestParam(value="email") String email,@RequestParam(value="mdp") String mdp,
			@RequestParam(value="profil") String profil,@RequestParam(value="application") String application, @RequestParam(value="loginDatabase") String loginDatabase) throws IOException{  
		return UserController.getInstance().updateUser(locale, loginUserconnecte, login, email, mdp, profil, application, loginDatabase);
    } 
	
	@ResponseBody
	@RequestMapping("/getUsers")  
	public User[] displayUsers() throws IOException{  
		return UserController.getInstance().displayUsersList();  
    }   	
	
	@ResponseBody
	@RequestMapping("/getUsers/{login}")  
	public User[] displayUsers(Model model,@PathVariable String login) throws IOException {  
		return UserController.getInstance().displayUsersListByLogin(model, login);  
    }   	
	
}
