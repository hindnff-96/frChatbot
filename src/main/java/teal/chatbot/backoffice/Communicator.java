package teal.chatbot.backoffice;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import communicators.CommunicatorUtilities;
import java.io.IOException;
import java.net.URI;
import java.util.Locale;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import org.codehaus.jackson.map.ObjectMapper;
import teal.chatbot.beans.Application;
import teal.chatbot.beans.Branche;
import teal.chatbot.beans.DecisionTree;
import teal.chatbot.beans.Demande;
import teal.chatbot.beans.EtapeBranche;
import teal.chatbot.beans.Historique;
import teal.chatbot.beans.HistoriqueRecherche;
import teal.chatbot.beans.MotCle;
import teal.chatbot.beans.Profile;
import teal.chatbot.beans.User;
import teal.chatbot.beans.UtilisationCount;

public class Communicator {
	private static Communicator communicatorInstance;
    
	private static final String URI = "http://localhost:5055/";
    //Profile
    private static final String PROFILES_END = "profils";
    //User
    private static final String USERS_END="users";
    //Historique
    private static final String HISTORY_END="history";
    private static final String HISTORY_END_Chatbot="chatbot/history";
    //Application
    private static final String APPLICATIONS_END="applications";
    //Demande
    private static final String DEMANDES_END="demandes";
    //Decision Tree
	private static final String BRUNCH_END = "branches";
	private static final String DECISION_TREE_END = "decisionTree";
	private static final String STEP_END = "etape";

	private static final String KEYWORD_END = "keyword";

	private static final String SIGNIN = "api/auth/signin";
	
	
    public static synchronized Communicator getInstance() {
        if (communicatorInstance == null) {
            communicatorInstance = new Communicator();
        }
        return communicatorInstance;
    }
    
    //Demandes
      public Demande[] getDemandes(int page) throws IOException {       
          String queryName= "page";
          String httpresponse = CommunicatorUtilities.getInstance().getMethod(DEMANDES_END, queryName, page);   
          ObjectMapper mapper = new ObjectMapper();       
          JsonParser jsonParser = new JsonParser();
          JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
          Demande[] demandes = null;
          demandes =mapper.readValue(objectFromString.get("content").toString(), Demande[].class);
          return demandes;
  	}
      
      public String nbrPagedisplayRequests(int page) {
      	 String queryName= "page";
           String httpresponse = CommunicatorUtilities.getInstance().getMethod(DEMANDES_END, queryName, page);   
             JsonParser jsonParser = new JsonParser();
             JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
             String pages=objectFromString.get("totalPages").toString();
             return pages;
     	}
      
      public String nbrPagedisplayDemandessApp(String app, int page) throws IOException {
          String queryName= "page";
          String httpresponse = CommunicatorUtilities.getInstance().getMethod(DEMANDES_END, "application",app, queryName,page);         
          JsonParser jsonParser = new JsonParser();
          JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
          String pages=objectFromString.get("totalPages").toString();
          return pages;
  	}

      public Demande[] getDemandesApp(String app, int page) throws IOException {
      	String queryName= "page";
          String httpresponse = CommunicatorUtilities.getInstance().getMethod(DEMANDES_END, "application",app, queryName,page);         
          ObjectMapper mapper = new ObjectMapper();
          JsonParser jsonParser = new JsonParser();
          JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
          Demande[] demandes = null;
          demandes =mapper.readValue(objectFromString.get("content").toString(), Demande[].class);
          return demandes;
  	}
      
      public Demande getDemandeById(String id) {
      	String httpresponse=CommunicatorUtilities.getInstance().getMethod(DEMANDES_END,id);
          ObjectMapper mapper = new ObjectMapper();
          Demande demande = null;
           try {
          	 demande = mapper.readValue(httpresponse, Demande.class);
  		} catch (IOException e) {
  			e.printStackTrace();
  		} 
          return demande;
      }
      
      public void updateDemande(Demande d) throws IOException {
          ObjectMapper mapper = new ObjectMapper(); 
      	Client client = Client.create(new DefaultClientConfig());
          URI uri = UriBuilder.fromUri(URI).build();        
          	client.resource(uri).path(DEMANDES_END)
          	.path(String.valueOf(d.getId_demande()))
              .type(MediaType.APPLICATION_JSON)
              .put(ClientResponse.class, mapper.writeValueAsString(d));
        }
      
      //Users
      public User[] getUsersSearch(String login) throws IOException {
      	String httpresponse=CommunicatorUtilities.getInstance().getMethod("/userSearch",login);
           ObjectMapper mapper = new ObjectMapper();
           User[] user=mapper.readValue(httpresponse, User[].class);
           return user;
      }
      
      public User getUserProfile(String login) throws IOException {
      	String httpresponse=CommunicatorUtilities.getInstance().getMethod("/user",login);
          ObjectMapper mapper = new ObjectMapper();
      	User user=mapper.readValue(httpresponse, User.class);
          return user;
  	}
      
      public User[] getUsers(){
      	String httpresponse=CommunicatorUtilities.getInstance().getMethod(USERS_END);
          ObjectMapper mapper = new ObjectMapper();
          User[] users = null;
           try {
  			users = mapper.readValue(httpresponse, User[].class);
  		} catch (IOException e) {
  			e.printStackTrace();
  		} 
          return users;
      }
      
      public User[] getUsersApp(String app) {
      	String httpresponse=CommunicatorUtilities.getInstance().getMethod(USERS_END,"application",app);
          ObjectMapper mapper = new ObjectMapper();
          User[] users = null;
           try {
   			users = mapper.readValue(httpresponse, User[].class);

  		} catch (IOException e) {
  			e.printStackTrace();
  		} 
          return users;
  	}
      
      public String getConnection(String login,String password) {
    	  JsonObject u=new JsonObject();
	  		u.addProperty("login", login);
	  		u.addProperty("password", password);
        	Client client = Client.create(new DefaultClientConfig());
            URI uri = UriBuilder.fromUri(URI).build();        
            ClientResponse response = client.resource(uri).path(SIGNIN)
                .type(MediaType.APPLICATION_JSON)
                .post(ClientResponse.class, u.toString());
            String httpresponse = response.getEntity(String.class);
            System.out.println(httpresponse);
            return httpresponse;
      }
      
      public String addUser(User u) throws IOException {
          ObjectMapper mapper = new ObjectMapper(); 
      	Client client = Client.create(new DefaultClientConfig());
          URI uri = UriBuilder.fromUri(URI).build();        
          ClientResponse response = client.resource(uri).path(USERS_END)
              .type(MediaType.APPLICATION_JSON)
              .post(ClientResponse.class, mapper.writeValueAsString(u));
          String httpresponse = response.getEntity(String.class);
          return httpresponse;
        }
      
      public String deleteUser(String login) {
       	Client client = Client.create(new DefaultClientConfig());
       	URI uri = UriBuilder.fromUri(URI).build();        
          ClientResponse response = client.resource(uri).path(USERS_END)
          	 .path(login)
               .type(MediaType.APPLICATION_JSON)
               .delete(ClientResponse.class);
          String httpresponse = response.getEntity(String.class);
          return httpresponse;
      }
      
      public void updateUser(User u , String loginDatabase) throws IOException {
      	u.setId_utilisateur(getUserProfile(loginDatabase).getId_utilisateur());
          ObjectMapper mapper = new ObjectMapper(); 
      	Client client = Client.create(new DefaultClientConfig());
          URI uri = UriBuilder.fromUri(URI).build();        
          	client.resource(uri).path(USERS_END)
          	.path(String.valueOf(u.getId_utilisateur()))
              .type(MediaType.APPLICATION_JSON)
              .put(ClientResponse.class, mapper.writeValueAsString(u));
        }
      
      //Application
      public Application getApplicationByName(String name) throws IOException {
      	String httpresponse=CommunicatorUtilities.getInstance().getMethod("/application",name);
          ObjectMapper mapper = new ObjectMapper();
          Application app=mapper.readValue(httpresponse, Application.class);
  	
        return app;
      }
      
      public Application getApplicationById(String application) throws IOException {
      	String httpresponse=CommunicatorUtilities.getInstance().getMethod("/application/id",application);
          ObjectMapper mapper = new ObjectMapper();
          Application app=mapper.readValue(httpresponse, Application.class);
  	
        return app;
  	}
      
      public Application[] getAppsSearch(String name) throws IOException {
      	String httpresponse=CommunicatorUtilities.getInstance().getMethod("/applicationSearch",name);
          ObjectMapper mapper = new ObjectMapper();
          Application[] app=mapper.readValue(httpresponse, Application[].class);
          return app;
     }
      
      public Application getApp(long id){
      	String httpresponse=CommunicatorUtilities.getInstance().getMethod(APPLICATIONS_END, String.valueOf(id));
          ObjectMapper mapper = new ObjectMapper();
          Application app = null;
           try {
  			app = mapper.readValue(httpresponse, Application.class);
  		} catch (IOException e) {
  			e.printStackTrace();
  		} 
          return app;
      }
      
      public Application[] getApps() {
  		String httpresponse=CommunicatorUtilities.getInstance().getMethod(APPLICATIONS_END);
          ObjectMapper mapper = new ObjectMapper();
          Application[] applications = null;
           try {
          	 applications = mapper.readValue(httpresponse, Application[].class);
  		} catch (IOException e) {
  			e.printStackTrace();
  		} 
          return applications;
  	  }
      
      public Application[] getAppsNotAdmin(){
    		String httpresponse=CommunicatorUtilities.getInstance().getMethod(APPLICATIONS_END,"notAdministration");
    	  ObjectMapper mapper = new ObjectMapper();
    	  Application[] apps = null;
    	         try {
    	        	 apps = mapper.readValue(httpresponse, Application[].class);
    	         } catch (IOException e) {
    				e.printStackTrace();
    			} 
    	        return apps;
      }
    
      public String addApp(Application u) throws IOException {
          ObjectMapper mapper = new ObjectMapper(); 
      	Client client = Client.create(new DefaultClientConfig());
          URI uri = UriBuilder.fromUri(URI).build();        
          ClientResponse response = client.resource(uri).path(APPLICATIONS_END)
              .type(MediaType.APPLICATION_JSON)
              .post(ClientResponse.class, mapper.writeValueAsString(u));
          String httpresponse = response.getEntity(String.class);
          return httpresponse;
        }
      
      public String deleteApp(String name) {
       	Client client = Client.create(new DefaultClientConfig());
       	URI uri = UriBuilder.fromUri(URI).build();        
          ClientResponse response = client.resource(uri).path(APPLICATIONS_END)
          	 .path(name)
               .type(MediaType.APPLICATION_JSON)
               .delete(ClientResponse.class);
          String httpresponse = response.getEntity(String.class);
          return httpresponse;
      }
      
      public void updateApp(Application u , String loginDatabase) throws IOException {
      	u.setId_application(getApplicationByName(loginDatabase).getId_application());
          ObjectMapper mapper = new ObjectMapper(); 
      	Client client = Client.create(new DefaultClientConfig());
          URI uri = UriBuilder.fromUri(URI).build();        
          	client.resource(uri).path(APPLICATIONS_END)
          	.path(String.valueOf(u.getId_application()))
              .type(MediaType.APPLICATION_JSON)
              .put(ClientResponse.class, mapper.writeValueAsString(u));
        }
      
      //Historique
      
      public String addAction(Historique u) throws IOException {
          ObjectMapper mapper = new ObjectMapper(); 
      	Client client = Client.create(new DefaultClientConfig());
          URI uri = UriBuilder.fromUri(URI).build();        
          ClientResponse response = client.resource(uri).path(HISTORY_END)
              .type(MediaType.APPLICATION_JSON)
              .post(ClientResponse.class, mapper.writeValueAsString(u));
          String httpresponse = response.getEntity(String.class);
          return httpresponse;
        }
      
      public Historique[] getHistoriqueBackOffice(int page) throws IOException {
      	 String queryName= "page";
           String httpresponse = CommunicatorUtilities.getInstance().getMethod(HISTORY_END, queryName, page);  
           System.out.println(httpresponse);
          		  ObjectMapper mapper = new ObjectMapper();        
                    JsonParser jsonParser = new JsonParser();
                    JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
                    Historique[] historiques = null;
                    historiques =mapper.readValue(objectFromString.get("content").toString(), Historique[].class);
                    return historiques;
       }
      
      public Historique[] getAllHistoriqueBackOffice(int page) throws IOException {
      	String queryName= "page";
          String httpresponse = CommunicatorUtilities.getInstance().getMethod(HISTORY_END, queryName, page);   
          ObjectMapper mapper = new ObjectMapper();
          JsonParser jsonParser = new JsonParser();
          JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
          Historique[] historiques = null;
          historiques =mapper.readValue(objectFromString.get("content").toString(), Historique[].class);
          return historiques;
  	}
      
      public String getNbrPagesBackOffice(int page) {
      	String queryName= "page";
          String httpresponse = CommunicatorUtilities.getInstance().getMethod(HISTORY_END, queryName, page);    
          JsonParser jsonParser = new JsonParser();
          JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
          String pages=objectFromString.get("totalPages").toString();
          return pages;
  	}

      public String getHistoriqueNbrPages(int page) throws IOException {
      	String queryName= "page";
          String httpresponse = CommunicatorUtilities.getInstance().getMethod(HISTORY_END, queryName, page);             
                  JsonParser jsonParser = new JsonParser();
                  JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
           String pages=objectFromString.get("totalPages").toString();
           return pages;
      }
      
      public Historique[] getHistoriqueByLoginApp(String login, String app, int page) throws IOException {
      	String queryName= "page";
          String httpresponse = CommunicatorUtilities.getInstance().getMethod(HISTORY_END, login,app, queryName,page);         
          ObjectMapper mapper = new ObjectMapper();
          JsonParser jsonParser = new JsonParser();
          JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
          Historique[] historiques = null;
          historiques =mapper.readValue(objectFromString.get("content").toString(), Historique[].class);
          return historiques;
  	}

      public String getHistoriqueByLoginAppNbrPages(String login, String app, int page) throws IOException {
      	String queryName= "page";
          String httpresponse = CommunicatorUtilities.getInstance().getMethod(HISTORY_END, login,app, queryName,page);            
          JsonParser jsonParser = new JsonParser();
          JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
          String pages=objectFromString.get("totalPages").toString();
          return pages;
  	}
      
      public Historique[] getAllHistoriqueBackOfficeApp(String app, int page) throws IOException {
      	String queryName= "page";
          String httpresponse = CommunicatorUtilities.getInstance().getMethod(HISTORY_END, "application",app, queryName,page);         
          ObjectMapper mapper = new ObjectMapper();
          JsonParser jsonParser = new JsonParser();
          JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
          Historique[] historiques = null;
          historiques =mapper.readValue(objectFromString.get("content").toString(), Historique[].class);
          return historiques;
  	}

  	public String displayDashboardByAppBackOfficeNbr(String app, int page) {
  		String queryName= "page";
          String httpresponse = CommunicatorUtilities.getInstance().getMethod(HISTORY_END, "application",app, queryName,page);         
  	        JsonParser jsonParser = new JsonParser();
  	        JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
  	        String pages=objectFromString.get("totalPages").toString();
  	        return pages;
  	}
  	
  	 public Historique[] getHistoriqueByLogin(String login, int page) throws IOException {
  		String queryName= "page";
        String httpresponse = CommunicatorUtilities.getInstance().getMethod(HISTORY_END, login, queryName,page);         
           ObjectMapper mapper = new ObjectMapper();
           JsonParser jsonParser = new JsonParser();
           JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
           Historique[] historiques = null;
           historiques =mapper.readValue(objectFromString.get("content").toString(), Historique[].class);
           return historiques;
      }  

  	public String getHistoriqueByLoginNbrPages(String login, int page) throws IOException {
  		String queryName= "page";
        String httpresponse = CommunicatorUtilities.getInstance().getMethod(HISTORY_END, login, queryName,page);                
         JsonParser jsonParser = new JsonParser();
         JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
         String pages=objectFromString.get("totalPages").toString();
         return pages;
    }  
     
  	//Roots
  	public Branche[] searchRoots(int page, String titre) throws IOException {
  		String queryName= "page";
          String httpresponse = CommunicatorUtilities.getInstance().getMethod("brancheSearch", titre, queryName,page);         
          ObjectMapper mapper = new ObjectMapper();
          JsonParser jsonParser = new JsonParser();
          JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
          Branche[] branches = null;
          branches =mapper.readValue(objectFromString.get("content").toString(), Branche[].class);
          return branches;
  	}
  	
  	public String getRootsNbrsearchRoots(int page, String titre) throws IOException {
  		String queryName= "page";
        String httpresponse = CommunicatorUtilities.getInstance().getMethod("brancheSearch", titre, queryName,page);         
          JsonParser jsonParser = new JsonParser();
	        JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
	        String pages=objectFromString.get("totalPages").toString();
	        return pages;
  	}
  	
  	public Branche[] getRoots(int page) throws IOException {
  		String queryName= "page";
          String httpresponse = CommunicatorUtilities.getInstance().getMethod(BRUNCH_END, "racines", queryName,page);         
          ObjectMapper mapper = new ObjectMapper();
          JsonParser jsonParser = new JsonParser();
          JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
          Branche[] branches = null;
          branches =mapper.readValue(objectFromString.get("content").toString(), Branche[].class);
          return branches;
  	}
  	
  	public String getRootsNbrPages(int page) throws IOException {
  		String queryName= "page";
          String httpresponse = CommunicatorUtilities.getInstance().getMethod(BRUNCH_END, "racines", queryName,page);         
          JsonParser jsonParser = new JsonParser();
	        JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
	        String pages=objectFromString.get("totalPages").toString();
	        return pages;
  	}
  	
  	public Branche[] getRootsByApp(int page, String app) throws IOException {
  		String queryName= "page";
          String httpresponse = CommunicatorUtilities.getInstance().getMethod(BRUNCH_END, "racines",app, queryName,page);         
          ObjectMapper mapper = new ObjectMapper();
          JsonParser jsonParser = new JsonParser();
          JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
          Branche[] branches = null;
          branches =mapper.readValue(objectFromString.get("content").toString(), Branche[].class);
          return branches;
  	}
      
  	public String getRootsNbrPagesByApp(int page, String app) {
  		String queryName= "page";
          String httpresponse = CommunicatorUtilities.getInstance().getMethod(BRUNCH_END, "racines",app, queryName,page);         
          JsonParser jsonParser = new JsonParser();
          JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
          String pages=objectFromString.get("totalPages").toString();
          return pages;
  	}
     
  	public Branche searchTree(String titre) throws IOException {
      	String httpresponse=CommunicatorUtilities.getInstance().getMethod("arbre",titre);
          ObjectMapper mapper = new ObjectMapper();
          return mapper.readValue(httpresponse, Branche.class);
  	}
    
  	public Branche searchBrunchById(String id) {
      	String httpresponse=CommunicatorUtilities.getInstance().getMethod(BRUNCH_END,id);
        ObjectMapper mapper = new ObjectMapper();
        Branche branche = null;
         try {
        	 branche = mapper.readValue(httpresponse, Branche.class);
		} catch (IOException e) {
			e.printStackTrace();
		} 
        return branche;
	}

  	public Branche[] displayBrunches(String app) throws  IOException {
      	String httpresponse=CommunicatorUtilities.getInstance().getMethod(BRUNCH_END,"application",app);
        ObjectMapper mapper = new ObjectMapper();
        Branche[] 
     	   branches = mapper.readValue(httpresponse, Branche[].class);
		
       return branches;
	}
  	
  	public String addRoot(Branche b) throws IOException {
  		b.setNiveau(1);
		ObjectMapper mapper = new ObjectMapper(); 
    	Client client = Client.create(new DefaultClientConfig());
        URI uri = UriBuilder.fromUri(URI).build();        
        ClientResponse response = client.resource(uri).path(BRUNCH_END)
            .type(MediaType.APPLICATION_JSON)
            .post(ClientResponse.class, mapper.writeValueAsString(b));
        String httpresponse = response.getEntity(String.class);
        return httpresponse;
		
	}

  	public String deletBranche(Locale locale, String loginUserconnecte, String branche) {
  		deleteDecisionTree(branche);
  		Client client = Client.create(new DefaultClientConfig());
     	URI uri = UriBuilder.fromUri(URI).build();        
        ClientResponse response = client.resource(uri).path(BRUNCH_END)
        	 .path(branche)
             .type(MediaType.APPLICATION_JSON)
             .delete(ClientResponse.class);
        String httpresponse = response.getEntity(String.class);
        
        
        return httpresponse;
	}

  	
  	public String deleteDecisionTree(String id) {
  		Client client = Client.create(new DefaultClientConfig());
     	URI uri = UriBuilder.fromUri(URI).build();        
        ClientResponse response = client.resource(uri).path(DECISION_TREE_END)
        	 .path(id)
             .type(MediaType.APPLICATION_JSON)
             .delete(ClientResponse.class);
        String httpresponse = response.getEntity(String.class);
        return httpresponse;
  	}
	public void updateBranche(Branche b) throws IOException {
		
        ObjectMapper mapper = new ObjectMapper(); 
    	Client client = Client.create(new DefaultClientConfig());
        URI uri = UriBuilder.fromUri(URI).build();        
        	client.resource(uri).path(BRUNCH_END)
        	.path(String.valueOf(b.getId_branche()))
            .type(MediaType.APPLICATION_JSON)
            .put(ClientResponse.class, mapper.writeValueAsString(b));
	}

	public String addNvBranche(String brunchSlectedAdd, Branche b) throws IOException {
		ObjectMapper mapper = new ObjectMapper(); 
    	Client client = Client.create(new DefaultClientConfig());
        URI uri = UriBuilder.fromUri(URI).build();        
        DecisionTree ligne=new DecisionTree();
        ligne.setSousBranche(b);
        ligne.setSuperBranche(searchBrunchById(brunchSlectedAdd));  
        ClientResponse response = client.resource(uri).path(DECISION_TREE_END).path("add").path(brunchSlectedAdd)
            .type(MediaType.APPLICATION_JSON)
            .post(ClientResponse.class, mapper.writeValueAsString(ligne));
        String httpresponse = response.getEntity(String.class);
        return httpresponse;
	}

	public String addExBranche(String brunchSlectedAdd, String branche) throws IOException {
		DecisionTree ligne=new DecisionTree();
        ligne.setSousBranche(searchBrunchById(branche));
        ligne.setSuperBranche(searchBrunchById(brunchSlectedAdd)); 
    	ObjectMapper mapper = new ObjectMapper(); 
        Client client = Client.create(new DefaultClientConfig());
        URI uri = UriBuilder.fromUri(URI).build();  
        ClientResponse response = client.resource(uri).path(DECISION_TREE_END).path("exist")
            .type(MediaType.APPLICATION_JSON)
            .post(ClientResponse.class, mapper.writeValueAsString(ligne));
        String httpresponse = response.getEntity(String.class);
        return httpresponse;
	}
	
  	//Profiles
      public Profile getProfile(long id){
      	String httpresponse=CommunicatorUtilities.getInstance().getMethod(PROFILES_END,String.valueOf(id));
      	ObjectMapper mapper = new ObjectMapper();
           Profile profile = null;
           try {
  			profile = mapper.readValue(httpresponse, Profile.class);
  		} catch (IOException e) {
  			e.printStackTrace();
  		} 
          return profile;
      }

      public Profile[] getProfiles(){
      	String httpresponse=CommunicatorUtilities.getInstance().getMethod(PROFILES_END);
          ObjectMapper mapper = new ObjectMapper();
           Profile[] profiles = null;
           try {
  			profiles = mapper.readValue(httpresponse, Profile[].class);
  		} catch (IOException e) {
  			e.printStackTrace();
  		} 
          return profiles;
      }

	public String addStep(EtapeBranche etape) throws IOException {
		ObjectMapper mapper = new ObjectMapper(); 
    	Client client = Client.create(new DefaultClientConfig());
        URI uri = UriBuilder.fromUri(URI).build();        
        ClientResponse response = client.resource(uri).path(STEP_END)
            .type(MediaType.APPLICATION_JSON)
            .post(ClientResponse.class, mapper.writeValueAsString(etape));
        String httpresponse = response.getEntity(String.class);
        return httpresponse;
		
	}

	public EtapeBranche[] displaySteps(String id) {
		String httpresponse=CommunicatorUtilities.getInstance().getMethod(STEP_END,"branche",id);
        ObjectMapper mapper = new ObjectMapper();
         EtapeBranche[] etapes = null;
         try {
        	 etapes = mapper.readValue(httpresponse, EtapeBranche[].class);
		} catch (IOException e) {
			e.printStackTrace();
		} 
        return etapes;
	}

	public String deleteStep(Locale locale, String stepDeleted) {
		Client client = Client.create(new DefaultClientConfig());
       	URI uri = UriBuilder.fromUri(URI).build();        
          ClientResponse response = client.resource(uri).path(STEP_END)
          	 .path(stepDeleted)
               .type(MediaType.APPLICATION_JSON)
               .delete(ClientResponse.class);
          String httpresponse = response.getEntity(String.class);
          return httpresponse;
		
	}

	public void updateStep(EtapeBranche etape) throws IOException {
		 ObjectMapper mapper = new ObjectMapper(); 
	      	Client client = Client.create(new DefaultClientConfig());
	          URI uri = UriBuilder.fromUri(URI).build();        
	          	client.resource(uri).path(STEP_END)
	          	.path(String.valueOf(etape.getId_etape()))
	              .type(MediaType.APPLICATION_JSON)
	              .put(ClientResponse.class, mapper.writeValueAsString(etape));
		
	}

	public String addKeyword(MotCle motCle) throws IOException {
		ObjectMapper mapper = new ObjectMapper(); 
    	Client client = Client.create(new DefaultClientConfig());
        URI uri = UriBuilder.fromUri(URI).build();        
        ClientResponse response = client.resource(uri).path(KEYWORD_END)
            .type(MediaType.APPLICATION_JSON)
            .post(ClientResponse.class, mapper.writeValueAsString(motCle));
        System.out.println(motCle.getBranche().getTitre());
        String httpresponse = response.getEntity(String.class);
        return httpresponse;
		
	}

	public void updateKeyword(MotCle motCle) throws IOException {
		ObjectMapper mapper = new ObjectMapper(); 
      	Client client = Client.create(new DefaultClientConfig());
          URI uri = UriBuilder.fromUri(URI).build();        
          	client.resource(uri).path(KEYWORD_END)
          	.path(String.valueOf(motCle.getId_motCle()))
              .type(MediaType.APPLICATION_JSON)
              .put(ClientResponse.class, mapper.writeValueAsString(motCle));
		
	}

	public String deleteKeyword(Locale locale, String keywordDeleted) {
		Client client = Client.create(new DefaultClientConfig());
       	URI uri = UriBuilder.fromUri(URI).build();        
          ClientResponse response = client.resource(uri).path(KEYWORD_END)
          	 .path(keywordDeleted)
               .type(MediaType.APPLICATION_JSON)
               .delete(ClientResponse.class);
          String httpresponse = response.getEntity(String.class);
          return httpresponse;
		
	}

	public String getNbrPagesChatbot(int page) {
		String queryName= "page";
        String httpresponse = CommunicatorUtilities.getInstance().getMethod(HISTORY_END_Chatbot, queryName, page);    
        JsonParser jsonParser = new JsonParser();
        JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
        String pages=objectFromString.get("totalPages").toString();
        return pages;
	}

	public String displayDashboardByAppChatbotNbr(String app, int page) {
		String queryName= "page";
        String httpresponse = CommunicatorUtilities.getInstance().getMethod(HISTORY_END_Chatbot, "application",app, queryName,page);         
	        JsonParser jsonParser = new JsonParser();
	        JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
	        String pages=objectFromString.get("totalPages").toString();
	        return pages;
	}

	public HistoriqueRecherche[] getAllHistoriqueChatbot(int page) throws IOException {
		String queryName= "page";
        String httpresponse = CommunicatorUtilities.getInstance().getMethod(HISTORY_END_Chatbot, queryName, page);   
        ObjectMapper mapper = new ObjectMapper();
        JsonParser jsonParser = new JsonParser();
        JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
        HistoriqueRecherche[] historiques = null;
        historiques =mapper.readValue(objectFromString.get("content").toString(), HistoriqueRecherche[].class);
        return historiques;
	}

	public HistoriqueRecherche[] getAllHistoriqueChatbotApp(String app, int page) throws IOException {
		String queryName= "page";
        String httpresponse = CommunicatorUtilities.getInstance().getMethod(HISTORY_END_Chatbot, "application",app, queryName,page);         
        ObjectMapper mapper = new ObjectMapper();
        JsonParser jsonParser = new JsonParser();
        JsonObject objectFromString = jsonParser.parse(httpresponse).getAsJsonObject();
        HistoriqueRecherche[] historiques = null;
        historiques =mapper.readValue(objectFromString.get("content").toString(), HistoriqueRecherche[].class);
        return historiques;
	}

	public UtilisationCount[] getchartChatbot() throws IOException{
		String httpresponse = CommunicatorUtilities.getInstance().getMethod("chatbot", "utilisation");         
        //ObjectMapper mapper = new ObjectMapper();
        //Utilisation[] historiques = null;
        //System.out.println(httpresponse);
        //JsonParser parser=new JsonParser();
        //JsonArray json=(JsonArray)parser.parse(httpresponse);
        //historiques =mapper.readValue(httpresponse, Utilisation[].class);
		 ObjectMapper mapper = new ObjectMapper();
		 UtilisationCount[] counts=mapper.readValue(httpresponse, UtilisationCount[].class);
		 //Utilisation[] historiques = null;
        return counts;
	}

	public UtilisationCount[] getchartChatbotApp(String app) throws IOException {
		String httpresponse = CommunicatorUtilities.getInstance().getMethod("chatbot", "utilisation",app);         
        //ObjectMapper mapper = new ObjectMapper();
        //Utilisation[] historiques = null;
        //historiques =mapper.readValue(httpresponse, Utilisation[].class);
        //return historiques;
		 ObjectMapper mapper = new ObjectMapper();
		 UtilisationCount[] counts=mapper.readValue(httpresponse, UtilisationCount[].class);
        return counts;
	}

	public UtilisationCount[] displaychatbotChartBackOfficeByApps() throws IOException {
		String httpresponse = CommunicatorUtilities.getInstance().getMethod("chatbot", "applicationUse");  
		ObjectMapper mapper = new ObjectMapper();
		 UtilisationCount[] counts=mapper.readValue(httpresponse, UtilisationCount[].class);
       return counts;
	}

	public String displayStatsBranches() {
		return CommunicatorUtilities.getInstance().getMethod("chatbot/statistique/branches");  
	}

	public String displayStatsBranchesApp(String app) {
		return CommunicatorUtilities.getInstance().getMethod("chatbot/statistique/branches",app);  
	}

	public String displayStatsDemandes() {
		return CommunicatorUtilities.getInstance().getMethod("chatbot/statistique/demandes");
	}

	public String displayStatsDemandesApp(String app) {
		return CommunicatorUtilities.getInstance().getMethod("chatbot/statistique/demandes",app); 
	}

	public String displayStatsRecherches() {
		return CommunicatorUtilities.getInstance().getMethod("chatbot/statistique/recherches");
	}

	public String displayStatsRecherchesApp(String app) {
		return CommunicatorUtilities.getInstance().getMethod("chatbot/statistique/recherches",app); 
	}

	
	public DecisionTree findRelation(String sousBranche,String superBranche) throws IOException {
		String response=CommunicatorUtilities.getInstance().getMethod(DECISION_TREE_END, sousBranche,superBranche);
		ObjectMapper mapper = new ObjectMapper();
		DecisionTree relation=mapper.readValue(response, DecisionTree.class);
	       return relation;
	}
	
	public String removeRelation(String idRelation) throws IOException {
		Client client = Client.create(new DefaultClientConfig());
       	URI uri = UriBuilder.fromUri(URI).build();        
          ClientResponse response = client.resource(uri).path(DECISION_TREE_END).path("removeLink")
          	 .path(idRelation)
               .type(MediaType.APPLICATION_JSON)
               .delete(ClientResponse.class);
          String httpresponse = response.getEntity(String.class);
          return httpresponse;
	}

	public DecisionTree getDecisionTree(String idRelation) throws IOException  {
		String response=CommunicatorUtilities.getInstance().getMethod(DECISION_TREE_END, idRelation);
		ObjectMapper mapper = new ObjectMapper();
		DecisionTree relation=mapper.readValue(response, DecisionTree.class);
	       return relation;
	}
  	

    //ajouter action � l'historique
    
/*         
    public String getConnexion(String uri_end) {
    	 Client client = Client.create(new DefaultClientConfig());
         URI uri = UriBuilder.fromUri(URI).build();        
         ClientResponse response = client.resource(uri).path(uri_end)
                 .type(MediaType.APPLICATION_JSON)
                 .get(ClientResponse.class);
         String httpresponse = response.getEntity(String.class);
         return httpresponse;
    }
    
    public String getConnexionByApp(String uri_end,String app) {
   	 Client client = Client.create(new DefaultClientConfig());
        URI uri = UriBuilder.fromUri(URI).build();        
        ClientResponse response = client.resource(uri).path(uri_end).path("application").path(app)
                .type(MediaType.APPLICATION_JSON)
                .get(ClientResponse.class);
        String httpresponse = response.getEntity(String.class);
        return httpresponse;
   }
    

    public String getConnexionbyID(String uri_end, String id) {
   	 Client client = Client.create(new DefaultClientConfig());
        URI uri = UriBuilder.fromUri(URI).build();        
        ClientResponse response = client.resource(uri).path(uri_end)
        		.path(id)
                .type(MediaType.APPLICATION_JSON)
                .get(ClientResponse.class);
        String httpresponse = response.getEntity(String.class);
        return httpresponse;
   }
    
    public User getConnexionbyLogin(String login) throws IOException {
      	 Client client = Client.create(new DefaultClientConfig());
           URI uri = UriBuilder.fromUri(URI).build();        
           ClientResponse response = client.resource(uri).path("/user")
           		.path(login)
                   .type(MediaType.APPLICATION_JSON)
                   .get(ClientResponse.class);
           ObjectMapper mapper = new ObjectMapper();
           String httpresponse = response.getEntity(String.class);
           User user=mapper.readValue(httpresponse, User.class);
           return user;
      }*/
    


    
    
    
    

    ///Applications
	
   
    
    /*public Application getConnexionbyNameApp(String name) throws IOException {
    	
    		Client client = Client.create(new DefaultClientConfig());
            URI uri = UriBuilder.fromUri(URI).build();        
            ClientResponse response = client.resource(uri).path("/application")
            		.path(name)
                    .type(MediaType.APPLICATION_JSON)
                    .get(ClientResponse.class);
            String httpresponse = response.getEntity(String.class);
            ObjectMapper mapper = new ObjectMapper();
            Application app=mapper.readValue(httpresponse, Application.class);
    	
          return app;
     }
    
    public Application getConnexionbyIdApp(String application) throws IOException {
    	Client client = Client.create(new DefaultClientConfig());
        URI uri = UriBuilder.fromUri(URI).build();        
        ClientResponse response = client.resource(uri).path("/application/id")
        		.path(application)
                .type(MediaType.APPLICATION_JSON)
                .get(ClientResponse.class);
        String httpresponse = response.getEntity(String.class);
        ObjectMapper mapper = new ObjectMapper();
        Application app=mapper.readValue(httpresponse, Application.class);
	
      return app;
	}*/



    
 
	

	

	
	


	

	
    
	
    
}
