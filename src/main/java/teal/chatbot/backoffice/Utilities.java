package teal.chatbot.backoffice;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import teal.chatbot.beans.Application;
import teal.chatbot.beans.Historique;
import teal.chatbot.beans.User;

public class Utilities {
	private static Utilities utilitiesInstance;
	
	public static synchronized Utilities getInstance() {
        if (utilitiesInstance == null) {
        	utilitiesInstance = new Utilities();
        }
        return utilitiesInstance;
    }
	
	public void ajouterHistorique(Locale locale,String action, String paramAction, String user, String application) throws IOException {
		Historique historique = new Historique();
		historique.setAction(action+": "+paramAction);
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		String formattedDate = dateFormat.format(date);
		historique.setForApplication(Communicator.getInstance().getApplicationById(application));
		historique.setDate(formattedDate);
		historique.setCreatedBy(Communicator.getInstance().getUserProfile(user));
		Communicator.getInstance().addAction(historique);
	}

	public void ajouterHistorique(Locale locale, String action, String paramAction, String user) throws IOException{
		Historique historique = new Historique();
		historique.setAction(action+": "+paramAction);
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		String formattedDate = dateFormat.format(date);
		historique.setForApplication(null);
		historique.setDate(formattedDate);
		historique.setCreatedBy(Communicator.getInstance().getUserProfile(user));
		Communicator.getInstance().addAction(historique);
		
	}
}
