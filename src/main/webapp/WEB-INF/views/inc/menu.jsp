<div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item dashboard" id="dashboardMenu">
            <a class="nav-link" href="<%=request.getContextPath() %>/dashboard">
              <i class="material-icons">dashboard</i>
              <p>Tableau de bord</p>
            </a>
          </li>
          
          <!-- your sidebar here -->
           <li class="nav-item arbre" id="arbreMenu">
            <a class="nav-link" href="<%=request.getContextPath() %>/arbreDecisionnel?page=0">
              <i class="material-icons">build</i>
              <p>Arbre décisionnel</p>
            </a>
          </li>
          
            <li class="nav-item demandes" id="demandesMenu">
            <a class="nav-link" href="<%=request.getContextPath() %>/demandes?page=0">
              <i class="material-icons">notifications</i>
              <p>Demandes</p>
            </a>
          </li>
          
           <li class="nav-item historique" id="historiqueMenu">
            <a class="nav-link" href="<%=request.getContextPath()%>/historique/" id="historiqueLien">
              <i class="material-icons">restore</i>
              <p>Historique</p>
            </a>
          </li>
          
           <li class="nav-item applications" id="applicationsMenu">
            <a class="nav-link" href="<%=request.getContextPath() %>/apps">
              <i class="material-icons">description</i>
              <p>Applications</p>
            </a>
          </li>
          
           <li class="nav-item utilisateurs" id="utilisateursMenu">
            <a class="nav-link" href="<%=request.getContextPath() %>/utilisateurs">
              <i class="material-icons">supervisor_account</i>
              <p>Utilisateurs</p>
            </a>
          </li>
          
          
           <li class="nav-item profils" id="profilsMenu">
            <a class="nav-link" href="<%=request.getContextPath() %>/profils">
              <i class="material-icons">assignment_ind</i>
              <p>Profils</p>
            </a>
          </li>
        </ul>
      </div>