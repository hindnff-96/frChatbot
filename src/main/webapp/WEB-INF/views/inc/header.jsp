    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
        
          <div class="navbar-wrapper" id="appsHeader">     
          <div class="dropdown">
			<button  style="padding: 10%; background: transparent; border: red; color: #3c4858" class="btn dropdown-toggle btAppSelected" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			Administration
			<span class="caret"></span>
			</button>
			<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
			<c:forEach items="${apps}" var="app">
				<li><a href="${app.name}" class="appSelected">${app.name}</a></li>
      		</c:forEach>
			</ul>
		</div>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="#pablo">
                  <i class="material-icons">cancel_presentation</i> Déconnexion
                </a>
              </li>
              <!-- your navbar here -->
            </ul>
          </div>
        </div>
      </nav>