<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!doctype html>
<html lang="en">

<head>
  <title>Teal -Chatbot</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- Material Kit CSS -->
 <link href="<%=request.getContextPath()%>/resources//assets/css/material-dashboard.css?v=2.1.1" rel="stylesheet" />
    <link href="<%=request.getContextPath()%>/resources//assets/css/material-dashboard.min.css?v=2.1.1" rel="stylesheet" /> 
    
    
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  
  
</head>

<body class="usersPage">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white">
      <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
      <jsp:include page="/WEB-INF/views/inc/user.jsp" />
      <jsp:include page="/WEB-INF/views/inc/menu.jsp" />
    </div>
    <div class="main-panel">
      <!-- Navbar -->
        <jsp:include page="/WEB-INF/views/inc/header.jsp" />
  
      <div class="content">
        <div class="container-fluid">
    	<form class="navbar-form col-md-3" style="float: right;">
              <span class="bmd-form-group"><div class="input-group no-border">
                <input type="text" value="" id="search-user" class="form-control searchUser" placeholder="Rechercher" autocomplete="off">&nbsp;
                
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div></span>
            </form>
            
            
          <button type="button" class="btn btn-default" style="margin: 3%"  data-toggle="modal" data-target="#addModal" >Ajouter Utilisateur</button>
          
          
          <div class=" col-md-12">
              <div class="card">
                <div class="card-header card-header-warning" style="background: #899fb4">
                  <h4 class="card-title">Utilisateurs</h4>
                </div>
                <div class="card-body table-responsive">
                  <table class="table table-hover tableUsers">
                    <thead class="text-warning">
                      <tr  style="color: #899fb4">
                      <th hidden="true"></th>
                      <th>Login</th>
                      <th>Application</th>
                      <th>Profil</th>
                      <th>Email</th>
                      <th hidden="true">Mot de passe</th>
                      <th>Modifier</th>
                      <th>Supprimer</th>
                      
                    </tr></thead>
                    <tbody id="feedback">
                    <c:set var="listeUser" value="${users}" />
                    <c:forEach items="${listeUser}" var="user">
     						<tr>
     							<td hidden="true">${user.application.id_application}</td>
				        		<td class ="idSelected" hidden="true">${user.profil.idProfil}</td>
				        		<td class ="loginSelected">${user.login}</td>
				        		<td>${user.application.name}</td>
				        		<td >${user.profil.profil}</td>
				        		<td >${user.email}</td>
				        		<td hidden="true">${user.mdp}</td>
				        		<td ><a href=#><i class="material-icons updateUser" data-toggle="modal" data-target="#updateModal">update</i></a></td>
				        		<td><a href=#><i class="material-icons deleteUser" data-toggle="modal" data-target="#deleteModal">delete</i></a></td>
				      		</tr>
      					</c:forEach>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
   
   <!-- Update Modal -->      
  <div class="modal fade" id="updateModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Utilisateurs / <span class="userSlected" style="color: #2a3f54"><b> Maria Jonas</b> </span> / Modifier utilisateur</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 <form class="col" action="updateUser" method="post">
        	  <input type="hidden" class="form-control loginUpdated" name ="loginDatabase">
        	  <input type="hidden" class="form-control loginConnecte" name ="loginUserconnecte">
        	  
        	  
              <div class="form-group">
                <input type="text" class="form-control loginUpdated" name ="login" id="login" placeholder="Login">
              </div>
              <div class="form-group">
                <input type="email" class="form-control emailUpdated" name="email" id="email" placeholder="Email">
              </div>
              <div class="form-group">
                <input type="password" class="form-control mdpUpdated" name="mdp" id="mdp" placeholder="Mot de Passe">
              </div>
              
              <div class="form-group ">
				<div class="input-group mb-3">
				  <select class="custom-select id profileUpUser" id="inputGroupSelect02" name="profil">
				    <option value="1" selected  class="profileUpdated" hidden="true">Profil</option>
				    <c:forEach items="${profiles}" var="profil">
     					<option value="${profil.idProfil}">${profil.profil}</option>
      				</c:forEach>
				  </select>
				</div>
              </div>
              
              
              <div class="form-group appUpUser">
				<div class="input-group mb-3">
				  <select class="custom-select id" id="inputGroupSelect02" name="application">
				    <option value="1" selected  class="appUpdated" hidden="true">Application</option>
				    <c:forEach items="${appsMod}" var="app">
     					<option value="${app.id_application}">${app.name}</option>
      				</c:forEach>
				  </select>
				</div>
              </div>
              
              <div class="form-group appUpUserAdmin" hidden="true">
				<div class="input-group mb-3">
				 <input type="text" disabled="disabled" class="form-control appUpdated" name ="application"  value="Administration">
				</div>
              </div>
              
              <button type="submit" class="btn btn-primary pull-right">Modifier</button>
            </form>
      </div>

    </div>
  </div>
</div>

<!-- Delete Modal -->

  <div class="modal fade" id="deleteModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Utilisateurs / <span class="userSlected" style="color: #2a3f54"><b>Maria Jonas</b></span> / Supprimer utilisateur</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 Voulez-vous vraiment supprimer cet utilisateur?
        	 <form class="col" action="deleteUser" method="post" style="padding: 5%">
        	  <input type="hidden" class="form-control loginConnecte" name ="loginUserconnecte">
        	 	 <input type="hidden" class="userSlectedDelete" name="login" value="login"/>
        		 <button type="submit" class="btn btn-primary pull-right test">Supprimer</button>
        	</form>
        	
      </div>

    </div>
  </div>
</div>
          <!-- Add Modal -->      
  <div class="modal fade" id="addModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Utilisateurs / Ajouter utilisateur</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 <form class="col" action="addUser" method="post">
        	  <input type="hidden" class="form-control loginConnecte" name ="loginUserconnecte">
              <div class="form-group">
                <input type="text" class="form-control" name ="login" id="login" placeholder="Login">
              </div>
              <div class="form-group">
                <input type="email" class="form-control" name="email" id="email" placeholder="Email">
              </div>
              <div class="form-group">
                <input type="password" class="form-control" name="mdp" id="mdp" placeholder="Mot de Passe">
              </div>
              
                    <div class="form-group">
				<div class="input-group mb-3">
				  <select class="custom-select profileAddUser" id="inputGroupSelect02" name="profil">
				  <option value="null" selected disabled="disabled">Profil</option>
				  <c:forEach items="${profiles}" var="profil">
     					<option value="${profil.idProfil}" id="${profil.profil}">${profil.profil}</option>
      				</c:forEach>
				  </select>
				  
				</div>
                   <div class="form-group appAddUser" hidden="true">
				<div class="input-group mb-3">
				  <select class="custom-select id" id="inputGroupSelect02" name="application">
				    <option value="null" selected  disabled="disabled" hidden="true">Application</option>
				    <c:forEach items="${appsMod}" var="app">
     					<option value="${app.id_application}">${app.name}</option>
      				</c:forEach>
				  </select>
				</div>
              </div>
              
                     <div class="form-group appAddUserAdmin" hidden="true">
				<div class="input-group mb-3">
				 <input type="text" class="form-control" name ="application" id="login" value="Administration" disabled="disabled">
				</div>
              </div>
             
			
              </div>
              <button type="submit" class="btn btn-primary pull-right">Ajouter</button>
            </form>
      </div>

    </div>
  </div>
</div>
           </div>
      </div>
  			<jsp:include page="/WEB-INF/views/inc/footer.jsp" />
  
    </div>
  </div>
  

  <script src="<%=request.getContextPath()%>/resources/assets/js/jquery.js"></script>
  <script src="<%=request.getContextPath()%>/resources/assets/js/jQueryAjax.js"></script>
  <script type="text/javascript">
	  $(window).load(function(){
		  $('#utilisateursMenu').addClass('active');
			$('#arbreMenu').removeClass('active');
			$('#profilsMenu').removeClass('active');
			$('#applicationsMenu').removeClass('active');
			$('#dashboardMenu').removeClass('active');
			$('#demandesMenu').removeClass('active');
			$('#historiqueMenu').removeClass('active');
	   });

	  $('.appSelected').click(function(e) {
			e.preventDefault();
			var appSelected=$(this).attr('href');
			displayDataApp("utilisateurs",appSelected);
			$('.btAppSelected').text(appSelected);
			
		});

	  $('.profileAddUser').change(function (){
		   	if ($('.profileAddUser').val() == 2) {
		   		$('.appAddUser').attr('hidden',false);
		   		$('.appAddUserAdmin').attr('hidden',true);
		   	}else{
		   		$('.appAddUser').attr('hidden',true);
		   		$('.appAddUserAdmin').attr('hidden',false);
			}
	  });

	  $('.profileUpUser').change(function (){
		   	if ($('.profileUpUser').val() == 2) {
		   		$('.appUpUser').attr('hidden',false);
		   		$('.appUpUserAdmin').attr('hidden',true);
		   		
		   	}else{
		   		$('.appUpUser').attr('hidden',true);
		   		$('.appUpUserAdmin').attr('hidden',false);
		   		$('.appUpdated').val('Administration');
		   		
			}
	  });
  </script>
</body>

</html>