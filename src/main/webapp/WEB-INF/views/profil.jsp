<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!doctype html>
<html lang="en" >

<head>
  <title>Teal -Chatbot</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- Material Kit CSS -->
 <link href="<%=request.getContextPath()%>/resources/assets/css/material-dashboard.css?v=2.1.1" rel="stylesheet" />
    <link href="<%=request.getContextPath()%>/resources/assets/css/material-dashboard.min.css?v=2.1.1" rel="stylesheet" /> 
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>

<body class="profilPage">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white">
      <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
      <jsp:include page="/WEB-INF/views/inc/user.jsp" />
      <jsp:include page="/WEB-INF/views/inc/menu.jsp" />
    </div>
    <div class="main-panel">
      <!-- Navbar -->
        <jsp:include page="/WEB-INF/views/inc/header.jsp" />
  
      <div class="content">
        <div class="container-fluid">          
          
          <div class=" col-md-12">
              <div class="card">
                <div class="card-header card-header-warning" style="background: #899fb4">
                  <h4 class="card-title">Profils</h4>
                </div>
                <div class="card-body table-responsive">
                  <table class="table table-hover">
                    <thead class="text-warning">
                      <tr  style="color: #899fb4">
                      <th >Profil</th>
                      <th >Droits d'acc�s</th>
                      
                    </tr></thead>
                    <tbody>
                    	<tr>
                    		<td>Administrateur</td>
                    		<td>
                    			<ul>
                    				<li> Gestion des utilisateurs</li>
                    				<li> Gestion des demandes</li>
                    				<li> Gestion des l'arbre d�cisionnel</li>
                    			</ul>
                    		</td>
                    	</tr>
                     	<tr>
                    		<td>Mod�rateur</td>
                    		<td>
                    			<ul>
                    				<li> Gestion des demandes</li>
                    				<li> Gestion des l'arbre d�cisionnel</li>
                    			</ul>
                    		</td>
                    	</tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
                  
                 
 
          
           </div>
      </div>
  			<jsp:include page="/WEB-INF/views/inc/footer.jsp" />
  
    </div>
  </div>
  <script src="<%=request.getContextPath()%>/resources/assets/js/jquery.js"></script>
  <script src="<%=request.getContextPath()%>/resources/assets/js/jQueryAjax.js"></script>
  <script type="text/javascript">
	  $(window).load(function(){
		  $('#profilsMenu').addClass('active');
			$('#arbreMenu').removeClass('active');
			$('#utilisateursMenu').removeClass('active');
			$('#applicationsMenu').removeClass('active');
			$('#dashboardMenu').removeClass('active');
			$('#demandesMenu').removeClass('active');
			$('#historiqueMenu').removeClass('active');
			$('#appsHeader').attr('hidden',true);
	   });
  </script>
</body>

</html>