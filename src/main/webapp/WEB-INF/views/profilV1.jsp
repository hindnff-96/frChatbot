<!doctype html>
<html lang="en">

<head>
  <title>Teal -Chatbot</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- Material Kit CSS -->
 <link href="<%=request.getContextPath()%>/resources/assets/css/material-dashboard.css?v=2.1.1" rel="stylesheet" />
    <link href="<%=request.getContextPath()%>/resources/assets/css/material-dashboard.min.css?v=2.1.1" rel="stylesheet" /> 
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>

<body>
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white">
      <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
      <jsp:include page="/WEB-INF/views/inc/user.jsp" />
      <jsp:include page="/WEB-INF/views/inc/menu.jsp" />
    </div>
    <div class="main-panel">
      <!-- Navbar -->
        <jsp:include page="/WEB-INF/views/inc/header.jsp" />
  
      <div class="content">
        <div class="container-fluid">
  
          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#addModal" >Ajouter Profil</button>
          
          
          <div class=" col-md-12">
              <div class="card">
                <div class="card-header card-header-warning" style="background: #899fb4">
                  <h4 class="card-title">Profils</h4>
                </div>
                <div class="card-body table-responsive">
                  <table class="table table-hover">
                    <thead class="text-warning">
                      <tr  style="color: #899fb4">
                      <th >Profil</th>
                      <th >Droits d'acc�s</th>
                      <th >Modifier</th>
                      <th >Supprimer</th>
                      
                    </tr></thead>
                    <tbody>
                      <tr>
				        <td >Administrateur</td>
				        <td >- Consulter <br> - Consulter <br> - Consulter <br> - Consulter <br> - Consulter <br> - Consulter</td>
				        <td ><a href=#><i class="material-icons" data-toggle="modal" data-target="#updateModal">update</i></a></td>
				        <td ><a href=#><i class="material-icons" data-toggle="modal" data-target="#deleteModal">delete</i></a></td>
				        
				      </tr>
				      <tr>
				        <td >Mod�rateur</td>
				        <td >- Consulter <br> - Consulter <br> - Consulter <br> - Consulter <br> - Consulter <br> - Consulter</td>
				        <td ><a href=#><i class="material-icons" data-toggle="modal" data-target="#updateModal">update</i></a></td>
				        <td ><a href=#><i class="material-icons" data-toggle="modal" data-target="#deleteModal">delete</i></a></td>
				      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
                  
                 
                                    <!-- Update Modal -->      
  <div class="modal fade" id="updateModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Profils / Mod�rateur /Modifier profil</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 <form class="col" action="test.php">
              <div class="form-group">
                <input type="text" class="form-control" name ="profil" id="profil" placeholder="Profil">
              </div>
              <div class="form-group">
                <input type="email" class="form-control" name="email" id="email" placeholder="Email">
              </div>
             <p class="Muted Text" style="margin-top: 3%">Droits d'acc�s</p>

				    <div class="checkbox" style="margin-left: 2%">
				      <label><input type="checkbox" value="">Option 1</label>
				    </div>
				    <div class="checkbox" style="margin-left: 2%">
				      <label><input type="checkbox" value="">Option 2</label>
				    </div>
				    <div class="checkbox" style="margin-left: 2%">
				      <label><input type="checkbox" value="">Option 3</label>
				    </div>
				    <div class="checkbox" style="margin-left: 2%">
				      <label><input type="checkbox" value="">Option 4</label>
				    </div>
				    <div class="checkbox" style="margin-left: 2%">
				      <label><input type="checkbox" value="">Option 5</label>
				    </div>
				   
              <button type="submit" class="btn btn-primary pull-right">Modifier</button>
            </form>
      </div>

    </div>
  </div>
</div>
                 
                  <!-- Delete Modal -->

  <div class="modal fade" id="deleteModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Profils / Mod�rateur / Supprimer profil</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 Voulez-vous vraiment supprimer ce profil?
        	 <form class="col" action="test.php" style="padding: 5%">
        		 <button type="submit" class="btn btn-primary pull-right">Supprimer</button>
        	</form>
      </div>

    </div>
  </div>
</div>
                  
                   <!-- Add Modal -->      
  <div class="modal fade" id="addModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Profils / Ajouter profil</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 <form class="col" action="test.php">
              <div class="form-group">
                <input type="text" class="form-control" name ="profil" id="profil" placeholder="Profil">
              </div>
              <div class="form-group">
                <input type="email" class="form-control" name="email" id="email" placeholder="Email">
              </div>
             <p class="Muted Text" style="margin-top: 3%">Droits d'acc�s</p>

				    <div class="checkbox" style="margin-left: 2%">
				      <label><input type="checkbox" value="">Option 1</label>
				    </div>
				    <div class="checkbox" style="margin-left: 2%">
				      <label><input type="checkbox" value="">Option 2</label>
				    </div>
				    <div class="checkbox" style="margin-left: 2%">
				      <label><input type="checkbox" value="">Option 3</label>
				    </div>
				    <div class="checkbox" style="margin-left: 2%">
				      <label><input type="checkbox" value="">Option 4</label>
				    </div>
				    <div class="checkbox" style="margin-left: 2%">
				      <label><input type="checkbox" value="">Option 5</label>
				    </div>
				   
              <button type="submit" class="btn btn-primary pull-right">Ajouter</button>
            </form>
      </div>

    </div>
  </div>
</div>
          
           </div>
      </div>
  			<jsp:include page="/WEB-INF/views/inc/footer.jsp" />
  
    </div>
  </div>
</body>

</html>