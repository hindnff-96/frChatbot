<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!doctype html>
<html lang="en">

<head>
  <title>Teal -Chatbot</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- Material Kit CSS -->
 <link href="<%=request.getContextPath()%>/resources//assets/css/material-dashboard.css?v=2.1.1" rel="stylesheet" />
    <link href="<%=request.getContextPath()%>/resources//assets/css/material-dashboard.min.css?v=2.1.1" rel="stylesheet" /> 
    
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  

</head>

<body class="usersPage">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white">
      <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
      <jsp:include page="/WEB-INF/views/inc/user.jsp" />
      <jsp:include page="/WEB-INF/views/inc/menu.jsp" />
    </div>
    <div class="main-panel">
      <!-- Navbar -->
        <jsp:include page="/WEB-INF/views/inc/header.jsp" />
  
      <div class="content">
        <div class="container-fluid">

          <div class=" col-md-12">
              <div class="card">
                <div class="card-header card-header-warning" style="background: #899fb4">
                <span hidden="true" class="brancheS">${branche.titre}</span>
                 <span hidden="true" class="rootS">${root}</span>
                 <span hidden="true" class="principale">${branche.principale}</span>
                <span hidden="true" class="brancheId">${branche.id_branche}</span>
                <span hidden="true" class="brancheDescription">${branche.description}</span>
                 <span hidden="true" class="brancheApplication">${branche.application.name}</span>
                 <span hidden="true" class="IdbrancheApplication">${branche.application.id_application}</span>
                  <h4 class="card-title">
                  	Branche / ${branche.titre}
                  	<c:if test="${branche.principale==true}">
                  		<span style="float: right;"><b>Principale</b></span>
                  	</c:if>
                  </h4>
                </div>
                <div class="card-body table-responsive" >
                 <span style="float: right; width: 10%; margin: 5% 0% 5% 5%">
	                  		<a href=# style="padding: 1%"><i style='font-size:30px' class="material-icons updateBrunch" data-toggle="modal" data-target="#updateModal">edit</i></a>
	                  		<a href=# style="padding: 1%"><i style='font-size:30px' class="material-icons deleteBrunch" data-toggle="modal" data-target="#deleteModal">delete_forever</i></a>
	                  		
                </span>
                
                
                	<div style="padding: 5%; padding-bottom: 0%"><p>${branche.description}</p></div>
                	
                	<div class="card">
                <div class="card-header card-header-warning" style="background: #fd950d">
                 <h4 class="card-title">Sous branches</h4>
                </div>
                	<c:if test = "${fn:length(branche.decisionTree)>0}">
                	
                	<table class="table table-hover tableHistorique" style="margin: 0% 5% 0% 5%;">
                		  <thead class="text-warning">
                      <tr  style="color: #899fb4">
                      <th hidden="true">id</th>
                      <th>Titre</th>
                      <th>Description</th>
                      <th>Annuler</th>
                    </tr></thead>
                    <tbody>
                      <c:forEach items="${branche.decisionTree}" var="enfant">
     						<tr>
     							<td hidden="true">${enfant.id_super_sous_branche}</td>
				        		<td >${enfant.sousBranche.titre}</td>
				        		<td >${enfant.sousBranche.description}</td>
				        		<td><a href=#><i class="material-icons removeBrunch" data-toggle="modal" data-target="#removeModal">highlight_off</i></a></td>
				      		</tr>
      					</c:forEach>
                    </tbody>
                  </table>
                	</c:if>
                	
                	<c:if test = "${fn:length(branche.decisionTree)==0}">
                	<h4 style="margin: 2% 0% 0% 2%">Aucune sous branche trouv�e !</h4>
                	</c:if>
                	
                 	 <button type="button" class="btn btn-default addBrunch" style="margin: 3%; width: 20%;margin-bottom: 5%;background: #fd950d"  data-toggle="modal" data-target="#addModal" >Ajouter Sous Branche</button>
                 	</div>
                 	
                
                	<div class="card">
                <div class="card-header card-header-warning" style="background: #ea4643">
                 <h4 class="card-title">Etapes</h4>
                </div>
                	<c:if test = "${fn:length(etapes)>0}">
                	
                	<table class="table table-hover tableHistorique" style="margin: 0% 5% 0% 5%;">
                		  <thead class="text-warning">
                      <tr  style="color: #899fb4">
                      <th hidden="true">id</th>
                      <th>N� Etape</th>
                      <th>Processus</th>
                      <th>Image</th>
                      <th>Modifier</th>
                      <th>Supprimer</th>
                    </tr></thead>
                    <tbody>
                      <c:forEach items="${etapes}" var="step">
     						<tr>
     							<td hidden="true">${step.id_etape}</td>
				        		<td >${step.num_etape}</td>
				        		<td >${step.description}</td>
								<td><p><img alt="${step.image.titre}" src="<%=request.getContextPath()%>/resources/imagesChatbot/${step.image.titre}" width="20%" height="20%"/></p></td>
				        		<td ><a href=#><i class="material-icons updateStep" data-toggle="modal" data-target="#updateModalStep">update</i></a></td>
				        		<td><a href=#><i class="material-icons deleteStep" data-toggle="modal" data-target="#deleteModalStep">delete</i></a></td>
				      		</tr>
      					</c:forEach>
                    </tbody>
                  </table>
                	</c:if>
                	
                	<c:if test = "${fn:length(etapes)==0}">
                	<h4 style="margin: 2% 0% 0% 2%">Aucune �tape trouv�e !</h4>
                	</c:if>
                	
                 	 <button type="button" class="btn btn-default addStep" style="margin: 3%; width: 20%;margin-bottom: 5%;background: #ea4643"  data-toggle="modal" data-target="#addStep" >Ajouter Etape</button>
                 	</div>
                 	
                	<div class="card">
                <div class="card-header card-header-warning" style="background: #4fa953">
                 <h4 class="card-title">Mots Cl�s</h4>
                </div>
                <c:if test = "${fn:length(branche.keywords)>0}">
                	<table class="table table-hover" style="margin: 0% 5% 0% 5%;">
                		  <thead class="text-warning">
                      <tr  style="color: #899fb4">
                      <th hidden="true">id</th>
                      <th>Mot Cl�</th>
                      <th>Modifier</th>
                      <th>Supprimer</th>
                    </tr></thead>
                    <tbody>
                      <c:forEach items="${branche.keywords}" var="keyword">
     						<tr>
     							<td hidden="true">${keyword.id_motCle}</td>
				        		<td >${keyword.motCle}</td>
				        		<td ><a href=#><i class="material-icons updateKeyword" data-toggle="modal" data-target="#updateModalKeyword">update</i></a></td>
				        		<td><a href=#><i class="material-icons deleteKeyword" data-toggle="modal" data-target="#deleteModalKeyword">delete</i></a></td>
				      		</tr>
      					</c:forEach>
                    </tbody>
                  </table>
                	</c:if>
                	
                	        <c:if test = "${fn:length(branche.keywords)==0}">
                	<h4 style="margin: 2% 0% 0% 2%">Aucun mot cl� trouv� !</h4>
                	</c:if>
                 	 <button type="button" class="btn btn-default addKeyword" style="margin: 3%; width: 20%;margin-bottom: 5%; background: #4fa953"  data-toggle="modal" data-target="#addKeyWord" >Ajouter Mot Cl�</button>
                 	</div></div>
                 	
	                
                
               
               

      
              </div>
            </div>
   
    <!-- Add Keyword -->      
  <div class="modal fade" id="addKeyWord">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Branche / <span class="brancheSelected" style="color: #2a3f54"><b>Marr</b></span> / Ajouter Mot Cl�</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 <form class="col" action="brancheConf/addKeyword" method="post">
        	  <input type="hidden" class="form-control loginConnecte" name ="loginUserconnecte">
        	 	 <input type="hidden" class="brancheAddStep" name="brancheAddStep" value="branche"/>
        		 <input type="hidden" class="brancheAddStepName" name="brancheAddStepName"/>
			
			<div class="form-group">
                <input type="text" class="form-control" name ="keyword"  placeholder="Mot Cl�">
			</div>
              <button type="submit" class="btn btn-primary pull-right">Ajouter Mot Cl�</button>
            </form>   
      </div>
    </div>
  </div>
</div>
<!-- Update Keyword -->      
  <div class="modal fade" id="updateModalKeyword">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Branche / <span class="brancheSelected" style="color: #2a3f54"><b>Marr</b></span> / Modifier Mot Cl�</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 <form class="col" action="brancheConf/updateKeyword" method="post">
        	  <input type="hidden" class="form-control loginConnecte" name ="loginUserconnecte">
        	 <input type="hidden" class="keywordSlectedUpdate" name="keywordUpdated" value="branche"/>
				<input type="hidden" class="brancheUpKeywordName" name="brancheUpKeywordName" value="branche"/>
        	  <input type="hidden" class="brancheUpKeyword" name="brancheUpKeyword" value="branche"/>
              <div class="form-group">
                <input type="text" class="form-control keywordpUp" name ="keywordpUp"  placeholder="Mot Cl�">
              </div>
              
              <button type="submit" class="btn btn-primary pull-right">Modifier Mot Cl�</button>
            </form>
      </div>

    </div>
  </div>
</div>

<!-- Delete Keyword -->

  <div class="modal fade" id="deleteModalKeyword">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Branche / <span class="brancheSelected" style="color: #2a3f54"><b>Marr</b></span> / Supprimer Mot Cl�</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 Voulez-vous vraiment supprimer ce mot cl�?
        	 <form class="col" action="brancheConf/deleteKeyword" method="post" style="padding: 5%">
        	  <input type="hidden" class="form-control loginConnecte" name ="loginUserconnecte">
        	 	 <input type="hidden" class="keywordDeleted" name="keywordDeleted" value="branche"/>
				<input type="hidden" class="brancheDeeleteKeywordName" name="brancheDeeleteKeywordName" value="branche"/>
				<input type="hidden" class="brancheDeeleteKeyword" name="brancheDeeleteKeyword" value="branche"/>
        		 <button type="submit" class="btn btn-primary pull-right test">Supprimer Mot Cl�</button>
        	</form>
        	
      </div>

    </div>
  </div>
</div>
  
  
   <div class="modal fade" id="addKeyWord">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Branche / <span class="brancheSelected" style="color: #2a3f54"><b>Marr</b></span> / Ajouter Mot Cl�</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 <form class="col" action="brancheConf/addKeyword" method="post">
        	  <input type="hidden" class="form-control loginConnecte" name ="loginUserconnecte">
        	 	 <input type="hidden" class="brancheAddStep" name="brancheAddStep" value="branche"/>
        		 <input type="hidden" class="brancheAddStepName" name="brancheAddStepName"/>
			
			<div class="form-group">
                <input type="text" class="form-control" name ="keyword"  placeholder="Mot Cl�">
			</div>
              <button type="submit" class="btn btn-primary pull-right">Ajouter Mot Cl�</button>
            </form>   
      </div>
    </div>
  </div>
</div>
             <!-- Add Step -->      
  <div class="modal fade" id="addStep">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Branche / <span class="brancheSelected" style="color: #2a3f54"><b>Marr</b></span> / Ajouter Etape</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 <form class="col" action="brancheConf/addStep" method="post" enctype="multipart/form-data">
        	  <input type="hidden" class="form-control loginConnecte" name ="loginUserconnecte">
        	 	 <input type="hidden" class="brancheAddStep" name="brancheAddStep" value="branche"/>
        		 <input type="hidden" class="brancheAddStepName" name="brancheAddStepName"/>
			
			<div class="form-group">
                <input type="text" class="form-control" name ="num"  placeholder="N� Etape">
			</div>
			
			<div class="form-group">
			  <textarea class="form-control" rows="5" placeholder="Description / Processus" name="stepDescription"></textarea>
			</div>
             
             <div class="form-group">
             	<div class="custom-file">
				  <input type="file" class="custom-file-input" id="hFichier" name="image" accept=".jpg"/>
				  <label class="custom-file-label" for="hFichier">S�lectionner un fichier</label>
				</div>
             </div> 
              <button type="submit" class="btn btn-primary pull-right">Ajouter Etape</button>
            </form>
            
                    
              
      </div>

    </div>
  </div>
</div>


<!-- Update Step -->      
  <div class="modal fade" id="updateModalStep">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Branche / <span class="brancheSelected" style="color: #2a3f54"><b>Marr</b></span> / Modifier Etape</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 <form class="col" action="brancheConf/updateStep" method="post"  enctype="multipart/form-data">
        	  <input type="hidden" class="form-control loginConnecte" name ="loginUserconnecte">
        	 <input type="hidden" class="stepSlectedUpdate" name="stepUpdated" value="branche"/>
				<input type="hidden" class="brancheUpStepName" name="brancheUpStepName" value="branche"/>
        	  <input type="hidden" class="brancheUpStep" name="brancheUpStep" value="branche"/>
              <div class="form-group">
                <input type="text" class="form-control numStepUp" name ="num"  placeholder="N� Etape">
              </div>
              <div class="form-group">
              	<textarea class="form-control titreBrancheUp" rows="5" placeholder="Description / Processus" name="stepDescription"></textarea>
              </div>
              
              <div class="form-group">
             	<div class="custom-file">
				  <input type="file" class="custom-file-input" id="hFichier" name="image" accept=".jpg"/>
				  <label class="custom-file-label imageBrancheUp" for="hFichier">S�lectionner un fichier</label>
				</div>
             </div> 
              <button type="submit" class="btn btn-primary pull-right">Modifier Etape</button>
            </form>
      </div>

    </div>
  </div>
</div>

<!-- Delete Step -->

  <div class="modal fade" id="deleteModalStep">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Branche / <span class="brancheSelected" style="color: #2a3f54"><b>Marr</b></span> / Supprimer Etape</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 Voulez-vous vraiment supprimer cette etape?
        	 <form class="col" action="brancheConf/deleteStep" method="post" style="padding: 5%">
        	  <input type="hidden" class="form-control loginConnecte" name ="loginUserconnecte">
        	 	 <input type="hidden" class="stepDeleted" name="stepDeleted" value="branche"/>
				<input type="hidden" class="brancheDeeleteStepName" name="brancheDeeleteStepName" value="branche"/>
				<input type="hidden" class="brancheDeeleteStep" name="brancheDeeleteStep" value="branche"/>
        		 <button type="submit" class="btn btn-primary pull-right test">Supprimer Etape</button>
        	</form>
        	
      </div>

    </div>
  </div>
</div>


   <!-- Update Modal -->      
  <div class="modal fade" id="updateModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Branche / <span class="brancheSelected" style="color: #2a3f54"><b>Marr</b></span> / Modifier Branche</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 <form class="col" action="brancheConf/updateBranche" method="post">
        	  <input type="hidden" class="form-control loginConnecte" name ="loginUserconnecte">

        	 	 <input type="hidden" class="brunchSlectedUpdate" name="brancheUpdated" value="branche"/>
        		 <input type="hidden" class="brancheSelected" name="brancheUpdatedName"/>
        	   <input type="hidden" class="rootSelected" name="root"/>
        	  
              <div class="form-group">
                <input type="text" class="form-control titreBrancheUp" name ="titre" id="titre" placeholder="Titre">
              </div>
              <div class="form-group">
                <input type="text" class="form-control descriptionBrancheUp" name="description" id="description" placeholder="Description / Processus">
              </div>
				
				<label>T�che principale</label>
				<div class="radio">
				  <label><input type="radio" name="principale" value="true" class="principaleUpTrue">Oui</label>
				</div>
				<div class="radio">
				  <label><input type="radio" name="principale" value="false" class="principaleUpFalse">Non</label>
				</div>
				


              <button type="submit" class="btn btn-primary pull-right">Modifier</button>
            </form>
      </div>

    </div>
  </div>
</div>

<!-- Delete Modal -->

  <div class="modal fade" id="deleteModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Branche / <span class="brancheSelected" style="color: #2a3f54"><b>Marr</b></span> / Supprimer branche</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 Voulez-vous vraiment supprimer cette branche?
        	 <form class="col" action="brancheConf/deleteBranche" method="post" style="padding: 5%">
        	  <input type="hidden" class="form-control loginConnecte" name ="loginUserconnecte">
        	 	 <input type="hidden" class="brunchSlectedDelete" name="brancheDEleted" value="branche"/>
        		 <input type="hidden" class="brancheSelected" name="brancheDEletedName"/>
        		 <input type="hidden" class="rootSelected" name="root"/>
        		 <button type="submit" class="btn btn-primary pull-right test">Supprimer</button>
        	</form>
        	
      </div>

    </div>
  </div>
</div>
          <!-- Add Modal -->      
  <div class="modal fade" id="addModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Branche / <span class="brancheSelected" style="color: #2a3f54"><b>Marr</b></span> / Ajouter Branche</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 
        <a href=# style="padding: 1%; float: left" class="newBrunch"  data-toggle="modal" data-target="#newBrunchModal">Nouvelle Branche</a>
	    <a href=# style="padding: 1%; float: right" class="existantBrunch" data-toggle="modal" data-target="#existantBrunchModal">Branche existante</a>      
                    
              
      </div>

    </div>
  </div>
</div>


<!-- REmove from a brunch Modal -->

  <div class="modal fade" id="removeModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Branche / <span class="brancheSelected" style="color: #2a3f54"><b>Marr</b></span> / Annuler sous branche</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 Voulez-vous vraiment annuler cette relation?
        	 <form class="col" action="brancheConf/removeRelation" method="post" style="padding: 5%">
        	  <input type="hidden" class="form-control loginConnecte" name ="loginUserconnecte">
        		 <input type="hidden" class="rootSelected" name="root"/>
        		 <input type="hidden" class="idRelation" name="idRelation"/>
        		 <input type="hidden" class="superBranche" name="superBranche"/>
        		 <button type="submit" class="btn btn-primary pull-right test">Annuler Relation</button>
        	</form>
        	
      </div>

    </div>
  </div>
</div>

<div class="modal fade" id="newBrunchModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Branche / <span class="brancheSelected" style="color: #2a3f54"><b>Marr</b></span> / Ajouter nouvelle Branche</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 <form class="col" action="brancheConf/addNvBranche" method="post">
        	  <input type="hidden" class="form-control loginConnecte" name ="loginUserconnecte">
        	 	 <input type="hidden" class="brunchSlectedAdd" name="brunchSlectedAdd" value="branche"/>
        		 <input type="hidden" class="brancheSelected" name="brancheAddedName"/>
        	   <input type="hidden" class="rootSelected" name="root"/>
        	  
              <div class="form-group">
                <input type="text" class="form-control" name ="titre" id="titre" placeholder="Titre">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="description" id="description" placeholder="Description / Processus">
              </div>

              <button type="submit" class="btn btn-primary pull-right">Ajouter</button>
            </form>
      </div>

    </div>
  </div>
</div>

<div class="modal fade" id="existantBrunchModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Branche / <span class="brancheSelected" style="color: #2a3f54"><b>Marr</b></span> / Ajouter Branche existante</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 <form class="col" action="brancheConf/addExBranche" method="post">
        	  <input type="hidden" class="form-control loginConnecte" name ="loginUserconnecte">
        	 	 <input type="hidden" class="brunchSlectedAdd" name="brunchSlectedAdd" value="branche"/>
        		 <input type="hidden" class="brancheSelected" name="brancheAddedName"/>
        	   <input type="hidden" class="rootSelected" name="root"/>
        	  
              S�lectionnez une branche :
              
              <div class="form-group appUpRacine">
				<div class="input-group mb-3">
				  <select class="custom-select id" id="inputGroupSelect02" name="branche">
				    <option value="1" selected  hidden="true">Branche</option>
				    <c:forEach items="${branchesApp}" var="branche">
     					<option value="${branche.id_branche}">${branche.titre}</option>
      				</c:forEach>
				  </select>
				</div>
              </div>

              <button type="submit" class="btn btn-primary pull-right">Ajouter</button>
            </form>
      </div>

    </div>
  </div>
</div>



           </div>
      </div>
  			<jsp:include page="/WEB-INF/views/inc/footer.jsp" />
  
    </div>
  </div>
  
 <script>
      $("input[type=file]").change(function (e){
          $(this).next('.custom-file-label').text(e.target.files[0].name);
          $('.imageBrancheUp').text(e.target.files[0].name);
       })
       
       
    </script>
  <script src="<%=request.getContextPath()%>/resources/assets/js/jquery.js"></script>
  <script src="<%=request.getContextPath()%>/resources/assets/js/jQueryAjax.js"></script>
  <script type="text/javascript">
  $(window).load(function(){
	  $('#arbreMenu').addClass('active');
		$('#utilisateursMenu').removeClass('active');
		$('#profilsMenu').removeClass('active');
		$('#applicationsMenu').removeClass('active');
		$('#dashboardMenu').removeClass('active');
		$('#demandesMenu').removeClass('active');
		$('#historiqueMenu').removeClass('active');
		$('#appsHeader').attr('hidden',true);		
   });


  </script>
</body>

</html>