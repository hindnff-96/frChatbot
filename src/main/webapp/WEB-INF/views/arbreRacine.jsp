<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!doctype html>
<html lang="en">

<head>
  <title>Teal -Chatbot</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- Material Kit CSS -->
 <link href="<%=request.getContextPath()%>/resources//assets/css/material-dashboard.css?v=2.1.1" rel="stylesheet" />
    <link href="<%=request.getContextPath()%>/resources//assets/css/material-dashboard.min.css?v=2.1.1" rel="stylesheet" /> 
    
    
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
   <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/assets/css/chart.css"/>
        <style>
        span.title {
            font-weight: normal;
            font-style: italic;
            color: #404040;
        }
        </style>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>
        
        $(function() {
            $("#organisation").orgChart({container: $("#main")});
        });
        </script>
  
</head>

<body class="racinesPage">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white">
      <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <span hidden="true" id="racineSelected">${titre}</span>
      <jsp:include page="/WEB-INF/views/inc/user.jsp" />
      <jsp:include page="/WEB-INF/views/inc/menu.jsp" />
    </div>
    <div class="main-panel">
      <!-- Navbar -->
        <jsp:include page="/WEB-INF/views/inc/header.jsp" />
  
      <div class="content">
        <div class="container-fluid">
   
          <div class=" col-md-12">
              <div class="card">
                <div class="card-header card-header-warning" style="background: #899fb4">
                  <h4 class="card-title">Racines / ${titre}</h4>
                </div>
            <div id="left" hidden="true">

            <ul id="organisation">
                
            </ul>
        
        </div>

        <div id="content">
            <div id="main">
            </div>
            
            <div class="text">
            </div>

        </div>
		
      </div>
</div> 
          
<div class="modal fade" id="consultationModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Racines / ${titre} <span class="brunchSlected" style="color: #2a3f54"><b> Maria Jonas</b> </span></h4>
        
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 
      </div>

    </div>
  </div>
</div>

</div>
      </div>
  			<jsp:include page="/WEB-INF/views/inc/footer.jsp" />
  
    </div>
  </div>
  

  <script src="<%=request.getContextPath()%>/resources/assets/js/jquery.js"></script>
  <script src="<%=request.getContextPath()%>/resources/assets/js/jQueryAjax.js"></script>
    <script src="<%=request.getContextPath()%>/resources/assets/js/pagination.js"></script>
 <script src="<%=request.getContextPath()%>/resources/assets/js/chart.js"></script> 
   <script src="<%=request.getContextPath()%>/resources/assets/js/ajaxChart.js"></script> 
  
  <script type="text/javascript">
  
	  $(window).load(function(){
		  $('#arbreMenu').addClass('active');
		  $('#profilsMenu').removeClass('active');
			$('#applicationsMenu').removeClass('active');
			$('#utilisateursMenu').removeClass('active');
			$('#dashboardMenu').removeClass('active');
			$('#demandesMenu').removeClass('active');
			$('#historiqueMenu').removeClass('active');
			$('#appsHeader').attr('hidden',true);	
			displayTree($('#racineSelected').text());
			
	   });
  </script> 
</body>

</html>