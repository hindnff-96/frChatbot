<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!doctype html>
<html lang="en">

<head>
  <title>Teal -Chatbot</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
      <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.min.js"></script>
  
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- Material Kit CSS -->
<link href="<%=request.getContextPath()%>/resources/assets/css/material-dashboard.css?v=2.1.1" rel="stylesheet" />
    <link href="<%=request.getContextPath()%>/resources/assets/css/material-dashboard.min.css?v=2.1.1" rel="stylesheet" /> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/assets/css/stylePagination.css">
   
  </head>

<body class="dashboardPage">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white">
      <jsp:include page="/WEB-INF/views/inc/user.jsp" />
      <jsp:include page="/WEB-INF/views/inc/menu.jsp" />
    </div>
    <div class="main-panel">
      <!-- Navbar -->
        <jsp:include page="/WEB-INF/views/inc/header.jsp" />
  
      <div class="content">
        <div class="container-fluid">          
          <div class="row">
          
          <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">timeline</i>
                  </div>
                  <p class="card-category">Utilisations</p>
                  <h3 class="card-title" id="utilisationChatbot">0</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">update</i> <span class="btAppSelected">Mantis</span> -Chatbot-
                  </div>
                </div>
              </div>
            </div>
            
          
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">search</i>
                  </div>
                  <p class="card-category">Recherches</p>
                  <h3 class="card-title" id="recherchesChatbot">0</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">update</i> <span class="btAppSelected">Mantis</span> -Chatbot-
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-danger card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">notifications</i>
                  </div>
                  <p class="card-category">Demandes</p>
                  <h3 class="card-title" id="demandesChatbot">0</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">update</i> <span class="btAppSelected">Mantis</span> -Chatbot-
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">games</i>
                  </div>
                  <p class="card-category">Branches</p>
                  <h3 class="card-title" id="branchesChatbot">0</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">update</i> <span class="btAppSelected">Mantis</span> -Chatbot-
                  </div>
                </div>
              </div>
            </div> 
          </div>         
          </div>
          
            <div id="divChartCount">
              <div class="card card-stats">
                
                
<!--                <div  id="chartContainer" style="height: 300px; max-width: 96%; overflow-y: auto; overflow-x: hidden;margin: 2%"></div>
 -->                
 
 					<canvas id="myChart" width="1600" height="900"></canvas>
 					
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">update</i> <span class="btAppSelected">Mantis</span> -Chatbot-
                  </div>
                </div>
              </div>
            </div>
            
            
            
            
            
            
            </div>
          
            <div class="row" style="margin: 2%">
            	
            
            
             <div class="col-xl-6 col-md-12">
              <div class="card">
                <div class="card-header card-header-warning" style="background: #899fb4">
                  <h4 class="card-title">Historique des recherches</h4>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-hover histChatbotBackOffice">
                    <thead class="text-warning">
                      <tr  style="color: #899fb4">
                      <th>Application</th>
                      <th>Recherche</th>
                    </tr></thead>
                    <tbody>
                    
                    <c:forEach items="${historiqueChatbot}" var="historiqueChatbot">
     					<tr>
	     					<td>${historiqueChatbot.forApplication.name}</td> 
	                        <td>${historiqueChatbot.recherche}</td>
     					</tr>
      				</c:forEach>  
                    </tbody>
                  </table>  
                  
  				        <ul id="pagination-demo-chatbot" class="pagination-lg"></ul>         
                </div>
                
                 <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">update</i> <span class="btAppSelected">Mantis</span> -Chatbot-
                  </div>
                </div>
              </div>
            </div>
            
            <div class="col-xl-6 col-md-12">
              <div class="card">
                <div class="card-header card-header-warning" style="background: #899fb4">
                  <h4 class="card-title">Utilisation du Chatbot par application</h4>
                </div>
<!--                 <div id="chartContainerUses" style="height: 300px; max-width: 96%; margin:2px;"></div>
 -->                
  				<div class="card-body table-responsive">
 
  					<canvas id="myChartUses" width="1600" height="900"></canvas>
 
              
              </div>
              
              
            </div>
            </div>
         
          
           </div>
      </div>
  			<jsp:include page="/WEB-INF/views/inc/footer.jsp" />
  
    </div>
  </div>


  <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
  
 <script src="<%=request.getContextPath()%>/resources/assets/js/jquery.js"></script>
  <script src="<%=request.getContextPath()%>/resources/assets/js/twbsPagination.min.js"></script>
  <script src="<%=request.getContextPath()%>/resources/assets/js/jQueryAjax.js"></script>
      <script src="<%=request.getContextPath()%>/resources/assets/js/pagination.js"></script>
 
  

  <script type="text/javascript">

  function displayChartTest(data){
	  var utilisation=0;
	  if(data.length>0){
		 
			  var dataPoints = [];
			  var dates =[];
			  $.each(data, function(key, value){
			        dataPoints.push(parseInt(value.count));
			        utilisation=utilisation+parseInt(value.count);
			        dates.push(new Date(value.date).toLocaleDateString('fr', {day:'numeric',month:'short', year:'numeric'}));	 
			    }); 
			  
		  }else{
			  var dataPoints = [0];
			  var dates =[];
			}
	  $('#utilisationChatbot').html(utilisation);
	  var ctx = $('#myChart');
	  var myChart = new Chart(ctx, {
		  responsive : true,
		  scaleOverride:true,
		  scaleStepWidth:1,
      	scaleOverride: true,
          scaleSteps: 2,
	    type: 'line',
	    options: {
	        scales: {
	            yAxes: [{
		            
	                ticks: {
		                
	                    beginAtZero: true
	                }
	            }],
				xAxes: [{
		            
					displayFormats:"dd mm yyyy"
	            }]
	           
	        }
	    },
	    data: {
	      labels: dates,
	      datasets: [
	        { 
	        	
	          data: dataPoints,
	          label: "Utilisation du chatbot",
	          borderColor: "#3e95cd",
	          fill: true
	        }
	      ]
	    }
	  });
		$('#myChart').attr('style','height: 300px; width: 96%; overflow: auto;margin: 2%'); 
	  }

  var app=$('.appSelected').attr('href');
	 
  $('#pagination-demo-chatbot').twbsPagination({
	    first:'Premier',
	    next: 'Suivant',
	    prev: 'Pr&eacutec&eacutedent',
	    last:'Dernier',
	    onPageClick: function(event, page) {
	    	changerPageHistChatbotBOD(page);
	    }
	});
	
	  $(window).load(function(){
		  $('#dashboardMenu').addClass('active');
			$('#arbreMenu').removeClass('active');
			$('#utilisateursMenu').removeClass('active');
			$('#applicationsMenu').removeClass('active');
			$('#profilsMenu').removeClass('active');
			$('#demandesMenu').removeClass('active');
			$('#historiqueMenu').removeClass('active');		
			$('.btAppSelected').text('Administration');
			//var app=$('.appSelected').attr('href');

			//displayPaginationBODashboard(app,1);
			displayPaginationChatbotRechercheDashboard("Administration",1);
			changerPageHistChatbotBOD(1);

			
			displayStatsBranches(app);
			displayStatsDemandes(app);
			displayStatsRecherches(app);
			$.ajax({
			       type: 'GET',
			       url: 'dashboard/chatbotChat/'+app,
			       ContentType: "application/json",
			       success : function(data) {
			       	console.log(data);
			       	displayChartTest(data);
							
					},
					error : function(request, status, error) {
						console.log(request.getAllResponseHeaders());
					},
					done : function(e) {
						console.log("DONE");
					}
			     });


			$.ajax({
			       type: 'GET',
			       url: 'dashboard/applicationUse',
			       ContentType: "application/json",
			       success : function(data) {
			       	console.log(data);
			       	displayChartUses(data)
							
					},
					error : function(request, status, error) {
						console.log(request.getAllResponseHeaders());
					},
					done : function(e) {
						console.log("DONE");
					}
			     });
			
			});

	  var dynamicColors = function() {
		    var r = Math.floor(Math.random() * 255);
		    var g = Math.floor(Math.random() * 255);
		    var b = Math.floor(Math.random() * 255);
		    return "rgb(" + r + "," + g + "," + b + ")";
		}
		
		function displayChartUses(data){
				  var dataPoints = [];
				  var dates =[];
				  var colors=[]
				  $.each(data, function(key, value){
				        dataPoints.push(parseInt(value.count));
				        dates.push(value.date);
 				        colors.push(dynamicColors()); 
			    }); 

				 
				  var ctx = $('#myChartUses');
				  var myChart = new Chart(ctx, {

				    type: 'doughnut',
				    data: {
				      labels: dates,
				      datasets: [
				        { 
				          data: dataPoints,
 				          backgroundColor: colors,
			               borderColor: 'rgba(200, 200, 200, 0.75)',
			               hoverBorderColor: 'rgba(200, 200, 200, 1)',
				         /*   backgroundColor: [
				        	   'rgb(54, 162, 235)',
		                        'rgb(201, 203, 207)',
		                        'rgb(0,0,255)',
		                        'rgb(0, 163, 51)',
		                        'rgb(255, 159, 64)',
		                        'rgb(255, 205, 86)',
		                        
		                        'rgb(153, 102, 255)'
		                        
		                    ], */ 
				        }
				      ]
				    }
				  });
					
				  myChart.render();

			}

	   
	   

		  function updateChartByApp(app){			  
			  $.ajax({
			       type: 'GET',
			       url: 'dashboard/chatbotChat/'+app,
			       ContentType: "application/json",
			       success : function(data) {
			       	console.log(data);
/* 			       	displayChart(data);
 */			       	displayChartTest(data);							
					},
					error : function(request, status, error) {
						console.log(request.getAllResponseHeaders());
					},
					done : function(e) {
						console.log("DONE");
					}
			     });

					
			  }
		
	
	  $('.appSelected').click(function(e) {
			e.preventDefault();
			var appSelected=$(this).attr('href');
			displayDataDashboard(appSelected);
			$('.btAppSelected').text(appSelected);
			
			/* //displayPaginationBODashboard(appSelected,1);
			displayPaginationChatbotRechercheDashboard(appSelected,1);

			changerPageHistBOD(1);
			
			//changerPageHistChatbotBOD(1,appSelected); */


			displayPaginationChatbotRechercheDashboard(appSelected,1);
			changerHistChatbotBOD(appSelected,1);

			
			updateChartByApp(appSelected);
			displayStatsBranches(appSelected);
			displayStatsDemandes(appSelected);
			displayStatsRecherches(appSelected);
			
		});
		
	 
		
  </script>
  
  <script type="text/javascript">

 


  </script>
</body>

</html>