<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page session="true"%>
<!doctype html>
<html lang="en">

<head>
  <title>Teal -Chatbot</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- Material Kit CSS -->
<link href="<%=request.getContextPath()%>/resources/assets/css/material-dashboard.css?v=2.1.1" rel="stylesheet" />
    <link href="<%=request.getContextPath()%>/resources/assets/css/material-dashboard.min.css?v=2.1.1" rel="stylesheet" /> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  </head>

<body>

  
      <div class="content">
        <div class="container-fluid">
			<div>
				<p align="center" style="margin-top: 2%;"><img width="5%" height="5%" src="<%=request.getContextPath()%>/resources/assets/img/tree.png" alt="Arbre décisionnel"/></p>
				<p align="center" ">TDT - Teal Decision Tree - </p>			
			</div>
	
          <div class="col-lg-6" style="margin: 0 auto; margin-top: 5%; margin-bottom: 5%">
              <div class="card">
                <div class="card-header card-header-warning" style="background: #899fb4">
                  <h4 class="card-title">Authentification</h4>
                </div>
                <div class="card-body table-responsive">
                
      <form action="getConnection" method="POST">
		  <div class="form-group">
		    <label >Login: </label>
		    <input type="text" class="form-control"  name="login">
		  </div>
		  <div class="form-group">
		    <label >Mot de passe: </label>
		    <input type="password" class="form-control" name="password">
		  </div>
		  <!-- <div class="checkbox">
		    <label><input type="checkbox"> Garder ma session active</label>
		  </div> -->
		  <button type="submit" class="btn btn-default" style="background: #2d4c5e">Se connecter</button>
		</form>
          
           </div>
      </div>
        </div>
              </div>
            </div>
  			<jsp:include page="/WEB-INF/views/inc/footer.jsp" />
  
   
  <script src="<%=request.getContextPath()%>/resources/assets/js/jquery.js"></script>
  
</body>

</html>