<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!doctype html>
<html lang="en">

<head>
  <title>Teal -Chatbot</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- Material Kit CSS -->
 <link href="<%=request.getContextPath()%>/resources//assets/css/material-dashboard.css?v=2.1.1" rel="stylesheet" />
    <link href="<%=request.getContextPath()%>/resources//assets/css/material-dashboard.min.css?v=2.1.1" rel="stylesheet" /> 
    
    
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  
  <style type="text/css">
  
  	.pagination{
  		display:-ms-flexbox;
  		display:flex;
  		padding-left:0;
  		list-style:none;
  		border-radius:.25rem
  	}
  	.page-link{
  		position:relative;
  		display:block;
  		padding:.5rem .75rem;
  		margin-left:-1px;
  		line-height:1.25;
  		color:#007bff;
  		background-color:#fff;
  		border:1px solid #dee2e6;
  	}
  	.page-link:hover{
  		z-index:2;
  		color:#0056b3;
  		text-decoration:none;
  		background-color:#e9ecef;
  		border-color:#dee2e6
  	}
  	.page-link:focus{
  		z-index:2;
  		outline:0;
  		box-shadow:0 0 0 .2rem rgba(0,123,255,.25)
  	}
  	.page-link:not(:disabled):not(.disabled){
  		cursor:pointer
  	}
  	.page-item:first-child .page-link{
  		margin-left:0;
  		border-top-left-radius:.25rem;
  		border-bottom-left-radius:.25rem
  	}
  	.page-item:last-child .page-link{
  		border-top-right-radius:.25rem;
  		border-bottom-right-radius:.25rem
  	}
  	.page-item.active .page-link{
  		z-index:1;
  		color:#fff;
  		background-color:#007bff;
  		border-color:#007bff
  	}
  	.page-item.disabled .page-link{
  		color:#6c757d;
  		pointer-events:none;
  		cursor:auto;
  		background-color:#fff;
  		border-color:#dee2e6
  	}
  	.pagination-lg .page-link{
  		padding:.5rem 1rem;
  		font-size:0.8rem;
  		line-height:1.5
  	}
  	.pagination-lg .page-item:first-child .page-link{
	  	border-top-left-radius:.3rem;
	  	border-bottom-left-radius:.3rem
  	}
  	.pagination-lg .page-item:last-child .page-link{
  		border-top-right-radius:.3rem;
  		border-bottom-right-radius:.3rem
  	}
  	.pagination-sm .page-link{
  		padding:.25rem .5rem;
  		font-size:.875rem;line-height:1.5
  	}
  	.pagination-sm .page-item:first-child .page-link{
  		border-top-left-radius:.2rem;
  		border-bottom-left-radius:.2rem
  	}
  	.pagination-sm .page-item:last-child .page-link{
  		border-top-right-radius:.2rem;
  		border-bottom-right-radius:.2rem
  	}
  </style>
</head>

<body class="racinesPage">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white">
      <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
      <jsp:include page="/WEB-INF/views/inc/user.jsp" />
      <jsp:include page="/WEB-INF/views/inc/menu.jsp" />
    </div>
    <div class="main-panel">
      <!-- Navbar -->
        <jsp:include page="/WEB-INF/views/inc/header.jsp" />
  
      <div class="content">
        <div class="container-fluid">
    	<form class="navbar-form col-md-3" style="float: right; ">
              <span class="bmd-form-group"><div class="input-group no-border">
                <input type="text" value="" id="search-racine" class="form-control searchRacine" placeholder="Rechercher" autocomplete="off">&nbsp;
                
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div></span>
            </form>
            
            
          <button type="button" class="btn btn-default" style="margin: 3%"  data-toggle="modal" data-target="#addModal" >Ajouter Racine</button>
          
          
          <div class=" col-md-12">
              <div class="card">
                <div class="card-header card-header-warning" style="background: #899fb4">
                  <h4 class="card-title">Racines</h4>
                </div>
                <div class="racines">
        <c:forEach var="i" begin="0" end="6" step="6"> 
        	<div class="row" style="margin-left: 6%; margin-right: 6%;">
	        	<c:set var="j" value="1"/>
	            <c:forEach  items="${branches}" var="branche" begin="${i}" end="${i+6}" step="1">
					 <c:if test = "${j==1}">
				         <div class="col-lg-2 col-md-6 col-sm-6">
				         	<a href="<%=request.getContextPath()%>/arbreDecisionnel/racine/${branche.titre }">
					              <div class="card card-stats">
					                <div class="card-header card-header-warning">
					                  <p class="card-title">${branche.titre } </p>                  
					                </div>
					                <div class="card-footer">
					                  <div class="stats">
					                   <span> ${branche.application.name } </span>
					                  </div>
					                </div>
					              </div>
				              </a>
				          </div>
				      </c:if>
				      <c:if test = "${j==2}">
				          <div class="col-lg-2 col-md-6 col-sm-6">
				          	<a href="<%=request.getContextPath()%>/arbreDecisionnel/racine/${branche.titre }">
					              <div class="card card-stats">
					                <div class="card-header card-header-primary">
					                  <p class="card-title">${branche.titre } </p>                  
					                </div>
					                <div class="card-footer">
					                  <div class="stats">
					                   <span> ${branche.application.name } </span>
					                  </div>
					                </div>
					              </div>
				              </a>
				          </div>
				      </c:if>
				      <c:if test = "${j==3}">
				          <div class="col-lg-2 col-md-6 col-sm-6">
				      		<a href="<%=request.getContextPath()%>/arbreDecisionnel/racine/${branche.titre }">
				              <div class="card card-stats">
				                <div class="card-header card-header-danger">
				                  <p class="card-title">${branche.titre } </p>                  
				                </div>
				                <div class="card-footer">
				                  <div class="stats">
				                   <span> ${branche.application.name } </span>
				                  </div>
				                </div>
				              </div>
				              </a>
				          </div>
				      </c:if>
				      <c:if test = "${j==4}">
				          <div class="col-lg-2 col-md-6 col-sm-6">
				          <a href="<%=request.getContextPath()%>/arbreDecisionnel/racine/${branche.titre }">
				              <div class="card card-stats">
				                <div class="card-header card-header-info">
				                  <p class="card-title">${branche.titre } </p>                  
				                </div>
				                <div class="card-footer">
				                  <div class="stats">
				                   <span> ${branche.application.name } </span>
				                  </div>
				                </div>
				              </div>
				              </a>
				          </div>
				      </c:if>
				      <c:if test = "${j==5}">
				          <div class="col-lg-2 col-md-6 col-sm-6">
				          <a href="<%=request.getContextPath()%>/arbreDecisionnel/racine/${branche.titre }">
				              <div class="card card-stats">
				                <div class="card-header card-header-success">
				                  <p class="card-title">${branche.titre } </p>                  
				                </div>
				                <div class="card-footer">
				                  <div class="stats">
				                   <span> ${branche.application.name } </span>
				                  </div>
				                </div>
				              </div>
				              </a>
				          </div>
				      </c:if>
				      <c:if test = "${j==6}">
				         <div class="col-lg-2 col-md-6 col-sm-6">
				         <a href="<%=request.getContextPath()%>/arbreDecisionnel/racine/${branche.titre }">
				              <div class="card card-stats">
				                <div class="card-header card-header-secondary" style="background: #2a3f54">
				                  <p class="card-title">${branche.titre } </p>                  
				                </div>
				                <div class="card-footer">
				                  <div class="stats">
				                   <span> ${branche.application.name } </span>
				                  </div>
				                </div>
				              </div>
				              </a>
				          </div>
				      </c:if>
				      <c:set var="j" value="${j+1}"/>
				</c:forEach>
			</div>		
      	</c:forEach>
      	</div>
		<nav aria-label="Page navigation example" style="margin-left: 10%" hidden="true">
            <ul class="pagination paginationRacines">
    
  			</ul>
  		</nav>
  		
  		 <ul id="pagination-demo" class="pagination-lg" style="margin: 2% 2% 2% 6%"></ul>
      </div>
   
   
   
   
   
   
   
   
   <%-- <!-- Update Modal -->      
  <div class="modal fade" id="updateModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Utilisateurs / <span class="userSlected" style="color: #2a3f54"><b> Maria Jonas</b> </span> / Modifier utilisateur</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 <form class="col" action="updateUser" method="post">
        	  <input type="hidden" class="form-control loginUpdated" name ="loginDatabase">
        	  <input type="hidden" class="form-control loginConnecte" name ="loginUserconnecte">
        	  
        	  
              <div class="form-group">
                <input type="text" class="form-control loginUpdated" name ="login" id="login" placeholder="Login">
              </div>
              <div class="form-group">
                <input type="email" class="form-control emailUpdated" name="email" id="email" placeholder="Email">
              </div>
              <div class="form-group">
                <input type="password" class="form-control mdpUpdated" name="mdp" id="mdp" placeholder="Mot de Passe">
              </div>
              
              <div class="form-group ">
				<div class="input-group mb-3">
				  <select class="custom-select id profileUpUser" id="inputGroupSelect02" name="profil">
				    <option value="1" selected  class="profileUpdated" hidden="true">Profil</option>
				    <c:forEach items="${profiles}" var="profil">
     					<option value="${profil.idProfil}">${profil.profil}</option>
      				</c:forEach>
				  </select>
				</div>
              </div>
              
              
              <div class="form-group appUpUser">
				<div class="input-group mb-3">
				  <select class="custom-select id" id="inputGroupSelect02" name="application">
				    <option value="1" selected  class="appUpdated" hidden="true">Application</option>
				    <c:forEach items="${appsMod}" var="app">
     					<option value="${app.id_application}">${app.name}</option>
      				</c:forEach>
				  </select>
				</div>
              </div>
              
              <div class="form-group appUpUserAdmin" hidden="true">
				<div class="input-group mb-3">
				 <input type="text" disabled="disabled" class="form-control appUpdated" name ="application"  value="Administration">
				</div>
              </div>
              
              <button type="submit" class="btn btn-primary pull-right">Modifier</button>
            </form>
      </div>

    </div>
  </div>
</div>

<!-- Delete Modal -->

  <div class="modal fade" id="deleteModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Utilisateurs / <span class="userSlected" style="color: #2a3f54"><b>Maria Jonas</b></span> / Supprimer utilisateur</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 Voulez-vous vraiment supprimer cet utilisateur?
        	 <form class="col" action="deleteUser" method="post" style="padding: 5%">
        	  <input type="hidden" class="form-control loginConnecte" name ="loginUserconnecte">
        	 	 <input type="hidden" class="userSlectedDelete" name="login" value="login"/>
        		 <button type="submit" class="btn btn-primary pull-right test">Supprimer</button>
        	</form>
        	
      </div>

    </div>
  </div>
</div> --%>
          <!-- Add Modal -->      
  <div class="modal fade" id="addModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Racines / Ajouter Racine</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 <form class="col" action="addRoot" method="post">
        	  <input type="hidden" class="form-control loginConnecte" name ="loginUserconnecte">
              <div class="form-group">
                <input type="text" class="form-control" name ="racine" id="racine" placeholder="Racine">
              </div>
              <div class="form-group appUpRacine">
				<div class="input-group mb-3">
				  <select class="custom-select id" id="inputGroupSelect02" name="application">
				    <option value="1" selected  class="appUpdated" hidden="true">Application</option>
				    <c:forEach items="${appsAddRacine}" var="app">
     					<option value="${app.id_application}">${app.name}</option>
      				</c:forEach>
				  </select>
				</div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Ajouter</button>
            </form>
      </div>

    </div>
  </div>
</div>
           </div>
      </div>
  			<jsp:include page="/WEB-INF/views/inc/footer.jsp" />
  
    </div>
  </div>
  

  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script src="<%=request.getContextPath()%>/resources/assets/js/jquery.js"></script>
  <script src="<%=request.getContextPath()%>/resources/assets/js/twbsPagination.min.js"></script>
  <script src="<%=request.getContextPath()%>/resources/assets/js/jQueryAjax.js"></script>
  <script src="<%=request.getContextPath()%>/resources/assets/js/pagination.js"></script>
  
 
  <script type="text/javascript">

  $('#pagination-demo').twbsPagination({
	    first:'Premier',
	    next: 'Suivant',
	    prev: 'Pr&eacutec&eacutedent',
	    last:'Dernier',
	    onPageClick: function(event, page) {
	    	changerPageRacine(page);
	    }
	});

	
	  $(window).load(function(){
		  $('#arbreMenu').addClass('active');
			$('#utilisateursMenu').removeClass('active');
			$('#profilsMenu').removeClass('active');
			$('#applicationsMenu').removeClass('active');
			$('#dashboardMenu').removeClass('active');
			$('#demandesMenu').removeClass('active');
			$('#historiqueMenu').removeClass('active');
			displayPaginationRacines('Administration',1);
			$('.btAppSelected').text('Administration');
	   });

	  $('.appSelected').click(function(e) {
			e.preventDefault();
			var appSelected=$(this).attr('href');
			displayPaginationRacines(appSelected,1);
			$('.btAppSelected').text(appSelected);
			changerPageRacine(1,appSelected);
			
		});

  </script>
</body>

</html>