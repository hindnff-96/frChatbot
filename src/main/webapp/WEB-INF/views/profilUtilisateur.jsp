<!doctype html>
<html lang="en">

<head>
  <title>Teal -Chatbot</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- Material Kit CSS -->
  <link href="<%=request.getContextPath()%>/resources//assets/css/material-dashboard.css?v=2.1.1" rel="stylesheet" />
    <link href="<%=request.getContextPath()%>/resources//assets/css/material-dashboard.min.css?v=2.1.1" rel="stylesheet" />
  
 
</head>

<body class="profilUtilisateurHeader">
 
	<script type="text/javascript">
  		document.getElementById("utilisateurs").className += " active";
  	</script> 
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white">
      <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
      <jsp:include page="/WEB-INF/views/inc/user.jsp" />
      <jsp:include page="/WEB-INF/views/inc/menu.jsp" />
    </div>
    <div class="main-panel">
      <!-- Navbar -->
        <jsp:include page="/WEB-INF/views/inc/header.jsp" />
  
      <div class="content">
        <div class="container-fluid">
    <div class="card">
                <div class="card-header card-header-primary" style="background: #2a3f54">
                  <h4 class="card-title">Profil</h4>
                  <p class="card-category">${user.login}</p>
                </div>
                <div class="card-body">
                  <form method="post" action="up/updateProfile">
                    <div class="col-md-4">
                        <div class="form-group bmd-form-group">
                          <label class="bmd-label-floating">Login</label>
                          <input type="text" class="form-control" name="login" value="${user.login}" style="margin-left: 40%">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group bmd-form-group">
                          <label class="bmd-label-floating">Email</label>
                          <input type="email" class="form-control" name="email" value="${user.email}" style="margin-left: 40%">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group bmd-form-group">
                          <label class="bmd-label-floating">Mot de passe</label>
                          <input type="password" class="form-control" name="mdp" value="${user.mdp}" style="margin-left: 40%">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group bmd-form-group">
                          <label class="bmd-label-floating ">Profil </label>
                          <input type="text" class="form-control" disabled="" value="${user.profil.profil}" style="margin-left: 40%">
                        </div>
                      </div>
                    <button type="submit" class="btn btn-primary pull-right">Modifier Profil</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
         
          
           </div>
      </div>
  			<jsp:include page="/WEB-INF/views/inc/footer.jsp" />
  
    </div>
  </div>
  <script src="<%=request.getContextPath()%>/resources/assets/js/jquery.js"></script>
<script src="<%=request.getContextPath()%>/resources/assets/js/jQueryAjax.js"></script>
<script type="text/javascript">
	$('#appsHeader').attr('hidden',true);	
</script>
</body>

</html>