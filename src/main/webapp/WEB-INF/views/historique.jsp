<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!doctype html>
<html lang="en">

<head>
  <title>Teal -Chatbot</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
     
    
  
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- Material Kit CSS -->
<link href="<%=request.getContextPath()%>/resources/assets/css/material-dashboard.css?v=2.1.1" rel="stylesheet" />
    <link href="<%=request.getContextPath()%>/resources/assets/css/material-dashboard.min.css?v=2.1.1" rel="stylesheet" /> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/assets/css/stylePagination.css">

 
  </head>

<body class="historiquePage">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white">
      <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
      <jsp:include page="/WEB-INF/views/inc/user.jsp" />
      <jsp:include page="/WEB-INF/views/inc/menu.jsp" />
    </div>
    <div class="main-panel">
      <!-- Navbar -->
        <jsp:include page="/WEB-INF/views/inc/header.jsp" />
  
      <div class="content">
        <div class="container-fluid">
                    
          <div class=" col-md-12">
       <div class="card">
                <div class="card-header card-header-warning" style="background: #899fb4">
                  <h4 class="card-title">Historique</h4>
                </div>
                <div class="card-body table-responsive container">
                  <table class="table table-hover tableHistorique">
                    <thead class="text-warning">
                      <tr  style="color: #899fb4">
                      <th>Application</th>
                      <th>Action</th>
                      <th>Date</th>
                    </tr></thead>
                    <tbody>
                      <c:forEach items="${historique}" var="historique">
     						<tr>
				        		<td >${historique.forApplication.name}</td>
				        		<td >${historique.action}</td>
				        		<td >${historique.date}</td>
				      		</tr>
      					</c:forEach>
                    </tbody>
                  </table>
                  <nav aria-label="Page navigation example" hidden="true">
                  <ul class="pagination paginationHist">
    
  				</ul>
  				</nav>
  				
  				 <ul id="pagination-demo" class="pagination-lg"></ul>


                </div>
              </div>
            </div>
       
         </div>
         
           
      </div>
  			<jsp:include page="/WEB-INF/views/inc/footer.jsp" />
  
    </div>
  </div>
  

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script src="<%=request.getContextPath()%>/resources/assets/js/jquery.js"></script>
  <script src="<%=request.getContextPath()%>/resources/assets/js/twbsPagination.min.js"></script>
  <script src="<%=request.getContextPath()%>/resources/assets/js/jQueryAjax.js"></script>
  <script src="<%=request.getContextPath()%>/resources/assets/js/pagination.js"></script>
  
  <script type="text/javascript">

  $('#pagination-demo').twbsPagination({
	    first:'Premier',
	    next: 'Suivant',
	    prev: 'Pr&eacutec&eacutedent',
	    last:'Dernier',
	    onPageClick: function(event, page) {
	    	changerPageHist(page);
	    }
	});
	
	  $(window).load(function(){
		  $('#historiqueMenu').addClass('active');
			$('#arbreMenu').removeClass('active');
			$('#utilisateursMenu').removeClass('active');
			$('#applicationsMenu').removeClass('active');
			$('#dashboardMenu').removeClass('active');
			$('#demandesMenu').removeClass('active');
			$('#profilsMenu').removeClass('active');
			displayPagination("Administration");
			$('.btAppSelected').text('Administration');
	   });

	  $('.appSelected').click(function(e) {
			e.preventDefault();
			var appSelected=$(this).attr('href');
			displayDataAppHistorique(appSelected);
			$('.btAppSelected').text(appSelected);
			displayPagination(appSelected);
			
		});

	 
  </script>
</body>

</html>