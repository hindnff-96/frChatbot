<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!doctype html>
<html lang="en">

<head>
  <title>Teal -Chatbot</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- Material Kit CSS -->
 <link href="<%=request.getContextPath()%>/resources//assets/css/material-dashboard.css?v=2.1.1" rel="stylesheet" />
    <link href="<%=request.getContextPath()%>/resources//assets/css/material-dashboard.min.css?v=2.1.1" rel="stylesheet" /> 
    
    
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/assets/css/stylePagination.css">

  
</head>

<body class="usersPage">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white">
      <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
      <jsp:include page="/WEB-INF/views/inc/user.jsp" />
      <jsp:include page="/WEB-INF/views/inc/menu.jsp" />
    </div>
    <div class="main-panel">
      <!-- Navbar -->
        <jsp:include page="/WEB-INF/views/inc/header.jsp" />
  
      <div class="content">
        <div class="container-fluid">
      
          
          <div class=" col-md-12">
              <div class="card">
                <div class="card-header card-header-warning" style="background: #899fb4">
                  <h4 class="card-title">Demandes</h4>
                </div>
                <div class="card-body table-responsive">
                  <table class="table table-hover tableDemandes">
                  </table>
                  <nav aria-label="Page navigation example" hidden="true">
                  <ul class="pagination paginationDemande">
    
  				</ul>
  				</nav>
  				
  				<ul id="pagination-demo" class="pagination-lg"></ul>
                </div>
              </div>
            </div>
<!-- Update Modal -->

  <div class="modal fade" id="updateModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Demande / <span class="demandeSlected" style="color: #2a3f54"><b>Maria Jonas</b></span></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	Avez-vous r�solu cette demande?
        	 <form class="col" action="demanderesolue" method="post" style="padding: 5%">
        	  <input type="hidden" class="form-control loginConnecte" name ="loginUserconnecte">
        	 	 <input type="hidden" class="form-control demandeSelectedResol" name="demande"/>
        		 <button type="submit" class="btn btn-primary pull-right test">Demande r�solue</button>
        	</form>
        	
      </div>

    </div>
  </div>
</div>
           </div>
      </div>
  			<jsp:include page="/WEB-INF/views/inc/footer.jsp" />
  
    </div>
  </div>
  

   <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script src="<%=request.getContextPath()%>/resources/assets/js/jquery.js"></script>
  <script src="<%=request.getContextPath()%>/resources/assets/js/twbsPagination.min.js"></script>
  <script src="<%=request.getContextPath()%>/resources/assets/js/jQueryAjax.js"></script>
    <script src="<%=request.getContextPath()%>/resources/assets/js/pagination.js"></script>
  <script type="text/javascript">
  $('#pagination-demo').twbsPagination({
	    first:'Premier',
	    next: 'Suivant',
	    prev: 'Pr&eacutec&eacutedent',
	    last:'Dernier',
	    onPageClick: function(event, page) {
	       
	    	changerPageDEmande(page);
	    }
	});
	
	  $(window).load(function(){
		  $('#demandesMenu').addClass('active');
			$('#arbreMenu').removeClass('active');
			$('#profilsMenu').removeClass('active');
			$('#applicationsMenu').removeClass('active');
			$('#dashboardMenu').removeClass('active');
			$('#utilisateursMenu').removeClass('active');
			$('#historiqueMenu').removeClass('active');
			
			displayPaginationDemandes('Administration');
			$('.btAppSelected').text('Administration');
	   });

	  $('.appSelected').click(function(e) {
			e.preventDefault();
			var appSelected=$(this).attr('href');
			if(appSelected=='Administration'){
				displayDataDemandes('all');
			}else{
				displayDataDemandes(appSelected);

			}
			$('.btAppSelected').text(appSelected);
			displayPaginationDemandes(appSelected);
		});

	  
  </script>
</body>

</html>