<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!doctype html>
<html lang="en">

<head>
  <title>Teal -Chatbot</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
 

  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- Material Kit CSS -->
 <link href="<%=request.getContextPath()%>/resources//assets/css/material-dashboard.css?v=2.1.1" rel="stylesheet" />
    <link href="<%=request.getContextPath()%>/resources//assets/css/material-dashboard.min.css?v=2.1.1" rel="stylesheet" /> 
    
    
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  
  
</head>

<body class="appsPage">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white">
      <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
      <jsp:include page="/WEB-INF/views/inc/user.jsp" />
      <jsp:include page="/WEB-INF/views/inc/menu.jsp" />
    </div>
    <div class="main-panel">
      <!-- Navbar -->
        <jsp:include page="/WEB-INF/views/inc/header.jsp" />
  

  
      <div class="content">
        <div class="container-fluid">
    	<form class="navbar-form col-md-3" style="float: right;">
              <span class="bmd-form-group"><div class="input-group no-border">
                <input type="text" value="" id="search-app" class="form-control searchapp" placeholder="Rechercher" autocomplete="off">&nbsp;
                
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div></span>
            </form>
            
            
          <button type="button" class="btn btn-default" style="margin: 3%"  data-toggle="modal" data-target="#addModal" >Ajouter Application</button>
          
            
          <div class=" col-md-12">
              <div class="card">
                <div class="card-header card-header-warning" style="background: #899fb4">
                  <h4 class="card-title">Applications</h4>
                </div>
                <div class="card-body table-responsive">
                  <table class="table table-hover tableApps">
                    <thead class="text-warning">
                      <tr  style="color: #899fb4">
                      <th hidden="true"></th>
                      <th>Application</th>
                      <th>Description</th>
                      <th>Modifier</th>
                      <th>Supprimer</th>
                      
                    </tr></thead>
                    <tbody id="feedback">
                    <c:forEach items="${apps}" var="app">
     						<tr>
				        		<td class ="loginSelected">${app.name}</td>
				        		<td class ="loginSelected">${app.description}</td>
				        		<td ><a href=#><i class="material-icons updateApp" data-toggle="modal" data-target="#updateModal">update</i></a></td>
				        		<td><a href=#><i class="material-icons deleteApp" data-toggle="modal" data-target="#deleteModal">delete</i></a></td>
				      		</tr>
      					</c:forEach>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
   
   <!-- Update Modal -->      
  <div class="modal fade" id="updateModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Applications / <span class="appSlected" style="color: #2a3f54"><b> Maria Jonas</b> </span> / Modifier application</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 <form class="col" action="updateApp" method="post">
        	  <input type="hidden" class="form-control appUpdated" name ="appUpdated">
        	  <input type="hidden" class="form-control loginConnecte" name ="loginConnecte">
        	  
        	  
              <div class="form-group">
                <input type="text" class="form-control appUpdated" name ="name" id="name" placeholder="Nom">
                <input type="text" class="form-control descriptionappUpdated" name ="description" id="description" placeholder="Description">
              </div>
          
              <button type="submit" class="btn btn-primary pull-right">Modifier</button>
            </form>
      </div>

    </div>
  </div>
</div>

<!-- Delete Modal -->

  <div class="modal fade" id="deleteModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Applications / <span class="appSlected" style="color: #2a3f54"><b>Maria Jonas</b></span> / Supprimer application</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 Voulez-vous vraiment supprimer cette application?
        	 <form class="col" action="deleteApp" method="post" style="padding: 5%">
        	  <input type="hidden" class="form-control loginConnecte" name ="loginUserconnecte">
        	 	 <input type="hidden" class="appSlectedDelete" name="appSelected" value="app"/>
        		 <button type="submit" class="btn btn-primary pull-right test">Supprimer</button>
        	</form>
        	
      </div>

    </div>
  </div>
</div>
          <!-- Add Modal -->      
  <div class="modal fade" id="addModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Application / Ajouter application</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        	 <form class="col" action="addApp" method="post">
        	  <input type="hidden" class="form-control loginConnecte" name ="loginUserconnecte">
              <div class="form-group">
                <input type="text" class="form-control" name ="name" id="name" placeholder="Nom">
                <input type="text" class="form-control " name ="description" id="description" placeholder="Description">
                
              </div>
        
              <button type="submit" class="btn btn-primary pull-right">Ajouter</button>
            </form>
      </div>

    </div>
  </div>
</div>
           </div>
      </div>
  			<jsp:include page="/WEB-INF/views/inc/footer.jsp" />
  
    </div>
  </div>
  

<script src="<%=request.getContextPath()%>/resources/assets/js/jquery.js"></script>
<script src="<%=request.getContextPath()%>/resources/assets/js/jQueryAjax.js"></script>


  <script type="text/javascript">
 
	  $(window).load(function(){
		  $('#applicationsMenu').addClass('active');
		  $('#profilsMenu').removeClass('active');
			$('#arbreMenu').removeClass('active');
			$('#utilisateursMenu').removeClass('active');
			$('#dashboardMenu').removeClass('active');
			$('#demandesMenu').removeClass('active');
			$('#historiqueMenu').removeClass('active');
			$('#appsHeader').attr('hidden',true);		
	   });
  </script>
</body>

</html>