//Racines
function paginationRacinesSearch(titre,page){
	var pageS=page-1;
		$.ajax({
	        type: 'GET',
	        url: 'arbreDecisionnel/racines/search/'+titre+'/pages?page='+pageS,
	        success : function(data) {
	        	console.log(data+"ok");
	        	displayPaginationRacinesSearch(data,titre);	
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
	      });
}

function displayPaginationRacinesSearch(data, search){
	if(data!=""){
		var test="";
		if(data>1){
			for(var i=0;i<data;i++){
				var page=i+1;
				test=test+'<li class="page-item"><a class="page-link" href="#" onclick=\"{searchRacine(\''+search+'\',\''+page+'\')}\">'+page+'</a></li>';
			}
		}
		
		$('.paginationRacines').html(test);
		$('.paginationRacines').show();
	}
}



function displayPaginationRacines(app,page){
	var pageS=page-1;
		$.ajax({
	        type: 'GET',
	        url: 'arbreDecisionnel/racines/'+app+'/pages?page='+pageS,
	        success : function(data) {
	        	console.log(data);
				displayPaginationDataRacines(data);		
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
	      });
}

function displayPaginationDataRacines(data){
	if(data!=""){
		var test="";
			if(data>0){
				$('#pagination-demo').show();
				if(data>4){
					
					$('#pagination-demo').twbsPagination("changeTotalPages", data, 1);
					$('#pagination-demo').twbsPagination("changeVisiblePages", 4);
				}else{
					$('#pagination-demo').twbsPagination("changeTotalPages", data, 1);
				}
			}else{
				$('#pagination-demo').hide();

			}
		
		//$('.paginationHist').html(test);
	}else{
		//var json = "<h4>Aucune Action trouv&eacute!e</h4>";
		$('#pagination-demo').hide();
	}
	
	
	
	/*if(data!=""){
		var test="";
		if(data>1){
			for(var i=0;i<data;i++){
				var page=i+1;
				test=test+'<li class="page-item"><a class="page-link" href="#" onclick=\"{changerPageRacine(\''+page+'\',\''+app+'\')}\">'+page+'</a></li>';
			}
		}
		
		$('.paginationRacines').html(test);
	}*/
}

function changerPageRacine(page){
	var app=$('.btAppSelected').text();
	var pageS=page-1;
	$.ajax({
        type: 'GET',
        url: 'arbreDecisionnel/racines/'+app+'?page='+pageS,
        success : function(data) {
        	console.log(data);
        	displayDataRacines(data);		
		},
		error : function(e) {
			console.log("ERROR: ", e);
		},
		done : function(e) {
			console.log("DONE");
		}
      });
}

function displayDataRacines(data) {
	var test="";
	if(data!=""){
		var i=0;
		$.each([0,6], function(index, value) {
			test=test+'<div class=\"row\" style=\"margin-left: 6%; margin-right: 6%;\">';
			var j=1;
			if(j<7){
				$.each(data, function(i, item) {
						if(value==0 && j<7){
							test=test+'<div class=\"col-lg-2 col-md-6 col-sm-6\"><a href=\"'+racine+'/arbreDecisionnel/racine/'+item.titre+'\"><div class=\"card card-stats\">';
							if(j==1){
								test=test+'<div class=\"card-header card-header-warning\">';
							}else if(j==2){
								test=test+'<div class=\"card-header card-header-primary\">';
							}else if(j==3){
								test=test+'<div class=\"card-header card-header-danger\">';
							}else if(j==4){
								test=test+'<div class=\"card-header card-header-info\">';
							}else if(j==5){
								test=test+'<div class=\"card-header card-header-success\">';
							}else if(j==6){
								test=test+'<div class=\"card-header card-header-secondary\" style=\"background: #2a3f54\">';
							}
							test=test+'<p class=\"card-title\">'+item.titre+'</p></div><div class=\"card-footer\">'
							+'<div class=\"stats\"><span> '+item.application.name+'</span></div></div></div></a></div>';	
						}
						if(value==6 && j>6){
							test=test+'<div class=\"col-lg-2 col-md-6 col-sm-6\"><a href=\"'+racine+'/arbreDecisionnel/racine/'+item.titre+'\"><div class=\"card card-stats\">';
							if(j==7){
								test=test+'<div class=\"card-header card-header-warning\">';
							}else if(j==8){
								test=test+'<div class=\"card-header card-header-primary\">';
							}else if(j==9){
								test=test+'<div class=\"card-header card-header-danger\">';
							}else if(j==10){
								test=test+'<div class=\"card-header card-header-info\">';
							}else if(j==11){
								test=test+'<div class=\"card-header card-header-success\">';
							}else if(j==12){
								test=test+'<div class=\"card-header card-header-secondary\" style=\"background: #2a3f54\">';
							}
							test=test+'<p class=\"card-title\">'+item.titre+'</p></div><div class=\"card-footer\">'
							+'<div class=\"stats\"><span> '+item.application.name+'</span></div></div></div></a></div>';	
							
						}
						
						j=j+1;
				});
			}else{
				test=test+"";
			}
			test=test+'</div>';
		});
	}else{
		test="<p style:\"margin-left: 50%\"><h4 style:\"margin-left: 2%;\">&nbsp;&nbsp;Aucune Racine trouv&eacutee!</h4></p>"
	}
	$('.racines').html(test);
}

//Dashboard
	//Historique BackOffice
function displayPaginationChatbotRechercheDashboard(app,page){
	var pageS=page-1;
	$.ajax({
        type: 'GET',
        url: 'dashboard/histChatbot/'+app+'/pages?page='+pageS,
        success : function(data) {
        	console.log(data);
        	displayPaginationDataChatbotRechercheDashboard(data,app);
        	
		},
		error : function(e) {
			console.log("ERROR: ", e);
		},
		done : function(e) {
			console.log("DONE");
		}
      });
}

function displayPaginationDataChatbotRechercheDashboard(data,app){
	if(data!=""){
		$('#pagination-demo-chatbot').show();
		var test="";
		if(data>0){
				if(data>4){
					$('#pagination-demo-chatbot').twbsPagination("changeTotalPages", data, 1);
				}else{
					$('#pagination-demo-chatbot').twbsPagination("changeTotalPages", data, 1);
				}
				
			
		
		}else{
			$('#pagination-demo-chatbot').hide();
		}
		
		//$('.paginationHistBODashboard').html(test);
	}
}

function changerPageHistChatbotBOD(page){
	var pageS=page-1;
	$.ajax({
        type: 'GET',
        url: 'dashboard/chatbot/'+app+'?page='+pageS,
        success : function(data) {
        	console.log(data);
        	displayHistChatbotDataBOD(data);		
		},
		error : function(e) {
			console.log("ERROR: ", e);
		},
		done : function(e) {
			console.log("DONE");
		}
      });
}

function changerHistChatbotBOD(appS,page){
	var pageS=page-1;
	$.ajax({
        type: 'GET',
        url: 'dashboard/chatbot/'+appS+'?page='+pageS,
        success : function(data) {
        	console.log(data);
        	displayHistChatbotDataBOD(data);		
		},
		error : function(e) {
			console.log("ERROR: ", e);
		},
		done : function(e) {
			console.log("DONE");
		}
      });
}

function displayHistChatbotDataBOD(data){
	if(data!=""){
		
	    var test="";
		$.each(data, function(i, item) {test=test+
			
			'<tr><td>'+item.application.name+'</td><td>'+item.recherche+'</td></tr>'			 		
		});   
       
		 var html="<thead class=\"text-warning\">" +
	 		"<tr  style=\"color: #899fb4\">" +
	 		"<th>Application</th><th>Recherche</th>" +
	 		"</tr></thead><tbody>" +test+"</tbody>";
		$('.histChatbotBackOffice').html(html);	
		
	}else{
		var json = "<h4>Aucune Recherche trouv&eacutee!</h4>";
		$('.histChatbotBackOffice').html(json);	
	}
}

function displayPaginationBODashboard(app,page){
	var pageS=page-1;
		$.ajax({
			
	        type: 'GET',
	        url: 'dashboard/histBackOffice/'+app+'/pages?page='+pageS,
	        success : function(data) {
	        	console.log(data);
				displayPaginationDataBODashboard(data,app);		
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
	      });
}

function displayPaginationDataBODashboard(data){
	if(data!=""){
		$('#pagination-demo').show();
		var test="";
		if(data>0){
				if(data>4){
					$('#pagination-demo').twbsPagination("changeTotalPages", data, 1);
				}else{
					$('#pagination-demo').twbsPagination("changeTotalPages", data, 1);
				
			}
			
		}else{
			$('#pagination-demo').hide();
		}
		
		$('.paginationHistBODashboard').html(test);
		
	}else{
		var json = "<h4>Aucune Action trouv&eacutee!</h4>";
		$('.paginationHistBODashboard').html(json);
		$('#pagination-demo').hide();
	}
}





function changerPageHistBOD(page,app){
	var pageS=page-1;
	var app=$('.btAppSelected').text();
	$.ajax({
        type: 'GET',
        url: 'dashboard/'+app+'?page='+pageS,
        success : function(data) {
        	console.log(data);
        	displayHistDataBOD(data);		
		},
		error : function(e) {
			console.log("ERROR: ", e);
		},
		done : function(e) {
			console.log("DONE");
		}
      });
}

function displayHistDataBOD(data) {
	
	if(data!=""){
	    var test="";
		$.each(data, function(i, item) {test=test+
			'<tr><td>'+item.forApplication.name+'</td><td>'+item.createdBy.login+'</td><td>'+item.action+'</td></tr>'			 		
		});   
       
		 var html="<thead class=\"text-warning\">" +
	 		"<tr  style=\"color: #899fb4\">" +
	 		"<th>Application</th><th>Login</th><th>Action</th>" +
	 		"</tr></thead><tbody>" +test+"</tbody>";
		$('.histBackOffice').html(html);	
		
	}else{
		var json = "<h4>Aucune Action trouv&eacutee!</h4>";
		$('.histBackOffice').html(json);	
	}
}
//Historique
	//Historique d'un utilisateur + Historique d'un utilisateur par application
function displayPagination(app){
	var user=$('.loginUserconnecte').text();
		$.ajax({
	        type: 'GET',
	        url: user+'/'+app+'/pages?page=0',
	        success : function(data) {
				displayPaginationData(data);
				console.log(data);
				changerPageHist(1);
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
	      });
}



function displayPaginationData(data) {
	if(data!=""){
		var test="";
			if(data>0){
				$('#pagination-demo').show();
				if(data>4){
					
					$('#pagination-demo').twbsPagination("changeTotalPages", data, 1);
					$('#pagination-demo').twbsPagination("changeVisiblePages", 4);
				}else{
					$('#pagination-demo').twbsPagination("changeTotalPages", data, 1);
				}
			}else{
				$('#pagination-demo').hide();

			}
		
		//$('.paginationHist').html(test);
	}else{
		//var json = "<h4>Aucune Action trouv&eacute!e</h4>";
		$('#pagination-demo').hide();
	}
}


function changerPageHist(page){
	var app=$('.btAppSelected').text();
	var user=$('.loginUserconnecte').text();
	var pageS=page-1;
		$.ajax({
	        type: 'GET',
	        url: user+'/'+app+'?page='+pageS,
	        success : function(data) {
				displayHistData(data);				
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
	      });
	
	
}

function displayHistoriquePagination(app){
	var user=$('.loginUserconnecte').text();
		$.ajax({
	        type: 'GET',
	        url: user+'/'+app,
	        success : function(data) {
				console.log("SUCCESS: ", data);
				displayHistData(data);				
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
	      });
}

	//Historique du BackOffice 
function displayHistoriqueBOPagination(app){
	
		$.ajax({
	        type: 'GET',
	        url: 'application/'+app,
	        success : function(data) {
				console.log("SUCCESS: ", data);
				displayHistData(data);	
				
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
	      });
}


function displayHistData(data) {
	if(data!=""){
		
	    var test="";
		$.each(data, function(i, item) {test=test+
			'<tr><td>'+item.forApplication.name+'</td><td>'+item.action+'</td><td>'+item.date+'</td></tr>'			 		
		});          
		 var html="<thead class=\"text-warning\">" +
	 		"<tr  style=\"color: #899fb4\">" +
	 		"<th>Application</th><th>Action</th><th>Date</th>" +
	 		"</tr></thead><tbody>" +test+"</tbody>";
		$('.tableHistorique').html(html);	
	}else{
		var json = "<h4>Aucune Action trouv&eacutee!</h4>";
		$('.tableHistorique').html(json);	
	}
}


///Demandes
function displayPaginationDemandes(app){
	
		$.ajax({
	        type: 'GET',
	        url: 'demandes/'+app+'/pages?page=0',
	        success : function(data) {
	        	console.log(data);
				displayPaginationDataDemandes(data);		
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
	      });
	
		
}

function displayPaginationDataDemandes(data) {
	if(data!=""){
		$('#pagination-demo').show();
		var test="";
		if(data>0){
			
			if(data>4){
				$('#pagination-demo').twbsPagination("changeTotalPages", data, 1);
				$('#pagination-demo').twbsPagination("changeVisiblePages", 4);
			}else{
				$('#pagination-demo').twbsPagination("changeTotalPages", data, 1);
			}
		}else{
			$('#pagination-demo').hide();
		}
	}else{		
		$('#pagination-demo').hide();
	}
}

function changerPageDEmande(page){
	var app=$('.btAppSelected').text();
	var pageS=page-1;
	if(app!='Administration'){
		$.ajax({
	        type: 'GET',
	        url: 'demandes/'+app+'?page='+pageS,
	        success : function(data) {
				console.log("SUCCESS: ", data);
				displayDemandes(data);				
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
	      });	
	}else{
		var pageS=page-1;
		$.ajax({
	        type: 'GET',
	        url: 'demandes/all?page='+pageS,
	        success : function(data) {
				console.log("SUCCESS: ", data);
				displayDemandes(data);				
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
	      });	
	}
	
	
	
}
