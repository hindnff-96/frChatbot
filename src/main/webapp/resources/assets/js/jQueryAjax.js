
var racine='http://localhost:8080/backoffice';

function displayStatsBranches(app){
		$.ajax({
	        type: 'GET',
	        url: 'dashboard/statistique/branches/'+app,
	        success : function(data) {
				console.log("SUCCESS: ", data);
				$('#branchesChatbot').html(data);
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
	      });
	
}

function displayStatsDemandes(app){
	$.ajax({
        type: 'GET',
        url: 'dashboard/statistique/demandes/'+app,
        success : function(data) {
			console.log("SUCCESS: ", data);
			$('#demandesChatbot').html(data);
		},
		error : function(e) {
			console.log("ERROR: ", e);
		},
		done : function(e) {
			console.log("DONE");
		}
      });

}

function displayStatsRecherches(app){
	$.ajax({
        type: 'GET',
        url: 'dashboard/statistique/recherches/'+app,
        success : function(data) {
			console.log("SUCCESS: ", data);
			$('#recherchesChatbot').html(data);
		},
		error : function(e) {
			console.log("ERROR: ", e);
		},
		done : function(e) {
			console.log("DONE");
		}
      });

}



//Branches
//Keywords
$('.addKeyword').click(function(e) {
    $('.brancheSelected').text($('.brancheS').text());
    $('.brancheAddStepName').val($('.brancheS').text());
    $('.brancheAddStep').val($('.brancheId').text());
});

$('.updateKeyword').click(function(e) {
	
	 var id_keyword = $(this).parents("td").prevAll().eq(1).text();
	 var keyword = $(this).parents("td").prevAll().eq(0).text();
  $('.brancheUpKeywordName').val($('.brancheS').text());
  $('.keywordSlectedUpdate').val(id_keyword);
  $('.keywordpUp').val(keyword);
  $('.brancheSelected').text($('.brancheS').text());
  $('.brancheUpKeyword').val($('.brancheId').text());
});

$('.deleteKeyword').click(function(e) {
	
	var id_keyword = $(this).parents("td").prevAll().eq(2).text();
  $('.brancheSelected').text($('.brancheS').text());
  $('.brancheDeeleteKeywordName').val($('.brancheS').text());
  $('.keywordDeleted').val(id_keyword);
  $('.brancheDeeleteKeyword').val($('.brancheId').text());
});

//Steps
$('.addStep').click(function(e) {
    $('.brancheSelected').text($('.brancheS').text());
    $('.brancheAddStepName').val($('.brancheS').text());
    $('.brancheAddStep').val($('.brancheId').text());
});



$('.updateStep').click(function(e) {
	 var id_step = $(this).parents("td").prevAll().eq(3).text();
	 var titre = $(this).parents("td").prevAll().eq(1).text();
	 var num = $(this).parents("td").prevAll().eq(2).text();
	 var image = $(this).parents("td").prevAll().eq(0).find('img').attr('alt');
	 if(image!=""){
		 $("input[type=file]").next('.custom-file-label').text(image);
	 }else{
		 $("input[type=file]").next('.custom-file-label').attr('placeholder','Sélectionner un fichier');
		 $("input[type=file]").next('.custom-file-label').empty();
	 }
	
    $('.brancheUpStepName').val($('.brancheS').text());
    $('.stepSlectedUpdate').val(id_step);
    $('.numStepUp').val(num);
    $('.titreBrancheUp').val(titre);
    $('.brancheSelected').text($('.brancheS').text());
    $('.brancheUpStep').val($('.brancheId').text());
});


$('.deleteStep').click(function(e) {
	var id_step = $(this).parents("td").prevAll().eq(4).text();
    $('.brancheSelected').text($('.brancheS').text());
    $('.brancheDeeleteStepName').val($('.brancheS').text());
    $('.stepDeleted').val(id_step);
    $('.brancheDeeleteStep').val($('.brancheId').text());
});
//DElete
$('.deleteBrunch').click(function(e) {
    $('.brancheSelected').text($('.brancheS').text());
    $('.brancheSelected').val($('.brancheS').text());
    $('.brunchSlectedDelete').val($('.brancheId').text());
    $('.rootSelected').val($('.rootS').text());
});




$('.updateBrunch').click(function(e) {
    $('.brancheSelected').text($('.brancheS').text());
    $('.brancheSelected').val($('.brancheS').text());
    $('.brunchSlectedUpdate').val($('.brancheId').text());
    $('.descriptionBrancheUp').val($('.brancheDescription').text());
    $('.titreBrancheUp').val($('.brancheS').text());
    $('.appUpdatedBranche').text($('.brancheApplication').text());
    $('.appUpdatedBranche').val($('.IdbrancheApplication').text());
    $('.rootSelected').val($('.rootS').text());
    var principale =$('.principale').text();
    if(principale=='true'){
    	 $('.principaleUpTrue').attr('checked','checked');
    }else{
    	$('.principaleUpFalse').attr('checked','checked');
    }
   
});


$('.removeBrunch').click(function(e) {
	var superB = $(this).parents("td").prevAll().eq(2).text();
	$('.rootSelected').val($('.rootS').text());
    $('.brancheSelected').text($('.brancheS').text());
    $('.superBranche').val($('.brancheId').text());
    $('.idRelation').val(superB);
});

$('.addBrunch').click(function(e) {
    $('.brancheSelected').text($('.brancheS').text());
    $('.brancheSelected').val($('.brancheS').text());
    $('.brunchSlectedDelete').val($('.brancheId').text());
});

$('.newBrunch').click(function(e) {
	 $("#addModal .close").click();
	 $('.brancheSelected').text($('.brancheS').text());
	 $('.brancheSelected').val($('.brancheS').text());
	  $('.rootSelected').val($('.rootS').text());
	 $('.brunchSlectedAdd').val($('.brancheId').text());
});

$('.existantBrunch').click(function(e) {
	 $("#addModal .close").click();
	 $('.brancheSelected').text($('.brancheS').text());
	 $('.brancheSelected').val($('.brancheS').text());
	  $('.rootSelected').val($('.rootS').text());
	 $('.brunchSlectedAdd').val($('.brancheId').text());
});

////Rcaines
//Search
function searchRacine(root,page){
	var pageS=page-1;
	if(root!=""){
		$.ajax({
	        type: 'GET',
	        url: 'arbreDecisionnel/racines/search/'+root+'?page='+pageS,
	        success : function(data) {
				console.log("SUCCESS: ", data);
				displayDataRacines(data);
				paginationRacinesSearch(root,1);
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
	      });
	}else{
		changerPageRacine(1,'Administration');
		displayPaginationRacines($('.btAppSelected').text(),1)
	}
}

$('#search-racine').keyup(function(){
	var root=$(this).val();
	searchRacine(root,1);
});

///////User Header
$('.loginUserconnecte').text("adminTest");
$('.profilUserconnecte').text("Administrateur");
$('.profilUserconnecteUser').text("Administrateur");
$('.loginUserconnecteUser').text("adminTest");

$('.loginConnecte').val("adminTest");
var login=$('.loginUserconnecte').text();
$('#historiqueLien').prop('href',racine+'/historique/'+login+'?page=0');
$('.profilUserDisplay').prop('href',racine+'/profil/'+login);
////Gestion de la visualisation par application
/*$('.appSelected').click(function(e) {
	e.preventDefault();
	var appSelected=$(this).attr('href');
	alert(appSelected);
	
	
});*/

function displayDataAppHistorique(app){
	var user=$('.loginUserconnecte').text();
		$.ajax({
	        type: 'GET',
	        url: user+'/'+app+'?page=0',
	        success : function(data) {
				console.log("SUCCESS: ", data);
				displayHistData(data);	
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
	      });
}

/*function displayHistData(data) {
	if(data!=""){
	    var test="";
		$.each(data, function(i, item) {test=test+
			'<tr><td>'+item.forApplication.name+'</td><td>'+item.action+'</td><td>'+item.date+'</td></tr>'			 		
		});   
       
		 var html="<thead class=\"text-warning\">" +
	 		"<tr  style=\"color: #899fb4\">" +
	 		"<th>Application</th><th>Action</th><th>Date</th>" +
	 		"</tr></thead><tbody>" +test+"</tbody>";
		$('.tableHistorique').html(html);	
	}else{
		var json = "<h4>Aucune Action trouv&eacute!e</h4>";
		$('.tableHistorique').html(json);	
	}
}*/

function displayDataApp(lien,app) {
	if(app!="Administration"){
		$.ajax({
	        type: 'GET',
	        url: lien+'/'+app,
	        success : function(data) {
				console.log("SUCCESS: ", data);
				if(lien="utilisateurs"){
					displayUser(data);
				}else if(lien.equals("historique")){
					displayHistData(data);
				}
				
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
	      });
	}else{
		$.ajax({
	        type: 'GET',
	        url: lien+'/'+'all',
	        success : function(data) {
				console.log("SUCCESS: ", data);
				if(lien="utilisateurs"){
					displayUser(data);
				}else if(lien.equals("historique")){
					displayHistData(data);
				}
				
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
	      });
	}


}
//Gestion des utilisateurs
	//Update
$('.updateUser').click(function(e) {		
	var id_profil = $(this).parents("td").prevAll().eq(5).text();
	var id_app = $(this).parents("td").prevAll().eq(6).text();
	var mdp = $(this).parents("td").prevAll().eq(0).text();
	var email = $(this).parents("td").prevAll().eq(1).text();
	var profil = $(this).parents("td").prevAll().eq(2).text();
	var app = $(this).parents("td").prevAll().eq(3).text();
	var login = $(this).parents("td").prevAll().eq(4).text();

	
	$('.loginUpdated').val(login);
	$('.emailUpdated').val(email);
	$('.mdpUpdated').val(mdp);
	$('.profileUpdated').text(profil);
	$('.profileUpdated').val(id_profil);	
	$('.appUpdated').text(app);
    $('.userSlected').text(login);
    if(id_app=='1'){
    	$('.appUpUser').attr('hidden',true);
   		$('.appUpUserAdmin').attr('hidden',false);
   		$('.appUpdated').val('Administration');

    }else{
    	$('.appUpUser').attr('hidden',false);
   		$('.appUpUserAdmin').attr('hidden',true);
   		$('.appUpdated').val(id_app);

    }

});

	//DElete
$('.deleteUser').click(function(e) {
	var login = $(this).parents("td").prevAll().eq(5).text();
    $('.userSlected').text(login);
    $('.userSlectedDelete').val(login);
});

	//Search
$('#search-user').keyup(function(){
	if($(this).val()!=""){
	$.ajax({
        type: 'GET',
        url: 'getUsers/'+$(this).val(),
        success : function(data) {
			console.log("SUCCESS: ", data);
			displayUser(data);
		},
		error : function(e) {
			console.log("ERROR: ", e);
		},
		done : function(e) {
			console.log("DONE");
		}
      });
	}else{
		$.ajax({
	        type: 'GET',
	        url: 'getUsers',
	        success : function(data) {
				console.log("SUCCESS: ", data);
				displayUser(data);
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
	      });
	}
});


function displayUser(data) {
	if(data!=""){
		
	    var test="";
		$.each(data, function(i, item) {
			test=test+
			'<tr><td class =\"idSelected\" hidden=\"true\">'+item.profil.idProfil+'</td><td class =\"loginSelected\">'+item.login+'</td><td >'+item.application.name+'</td><td >'+item.profil.profil+'</td><td >'+item.email+'</td><td >'+item.mdp+'</td><td ><a href=#><i class=\"material-icons updateUser\" data-toggle=\"modal\" data-target=\"#updateModal\">update</i></a></td><td><a href=#><i class=\"material-icons deleteUser\" data-toggle=\"modal\" data-target=\"#deleteModal\">delete</i></a></td></tr>'			 
		});
		
		 var html="<thead class=\"text-warning\">" +
	 		"<tr  style=\"color: #899fb4\">" +
	 		"<th hidden=\"true\"></th><th>Login</th><th>Application</th><th>Profil</th><th>Email</th><th>Mot de passe</th><th>Modifier</th><th>Supprimer</th>" +
	 		"</tr></thead><tbody id=\"feedback\">" +test+"</tbody>";
		$('.tableUsers').html(html);	
	}else{
		var json = "<h4>Aucun utilisateur trouv&eacute;!</h4>";
		$('.tableUsers').html(json);	
	}
}

///Gestion des applications
//Update
$('.updateApp').click(function(e) {		
	
	var name = $(this).parents("td").prevAll().eq(1).text();
	var description = $(this).parents("td").prevAll().eq(0).text();
	$('.appUpdated').val(name);
    $('.appSlected').text(name);
    $('.descriptionappUpdated').val(description);

});

	//DElete
$('.deleteApp').click(function(e) {
	var name = $(this).parents("td").prevAll().eq().text();
    $('.appSlected').text(name);
    $('.appSlectedDelete').val(name);
});

	//Search
$('#search-app').keyup(function(){
	if($(this).val()!=""){
	$.ajax({
        type: 'GET',
        url: 'getApps/'+$(this).val(),
        success : function(data) {
			console.log("SUCCESS: ", data);
			displayApp(data);
		},
		error : function(e) {
			console.log("ERROR: ", e);
		},
		done : function(e) {
			console.log("DONE");
		}
      });
	}else{
		$.ajax({
	        type: 'GET',
	        url: 'getApps',
	        success : function(data) {
				console.log("SUCCESS: ", data);
				displayApp(data);
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
	      });
	}
});


function displayApp(data) {
	if(data!=""){
	    var test="";
		$.each(data, function(i, item) {test=test+
			'<tr><td class =\"loginSelected\">'+item.name+'</td><td ><a href=#><i class=\"material-icons updateApp\" data-toggle=\"modal\" data-target=\"#updateModal\">update</i></a></td><td><a href=#><i class=\"material-icons deleteApp\" data-toggle=\"modal\" data-target=\"#deleteModal\">delete</i></a></td></tr>'			 		
		});
		
		 var html="<thead class=\"text-warning\">" +
	 		"<tr  style=\"color: #899fb4\">" +
	 		"<th hidden=\"true\"></th><th>Application</th><th>Modifier</th><th>Supprimer</th>" +
	 		"</tr></thead><tbody id=\"feedback\">" +test+"</tbody>";
		$('.tableApps').html(html);	
	}else{
		var json = "<h4>Aucune application trouv&eacute;e!</h4>";
		$('.tableApps').html(json);	
	}
}

//Demandes
//Update
function displayDataDemandes(app){
		
	$.ajax({
	        type: 'GET',
	        url: 'demandes/'+app+'?page=0',
	        success : function(data) {
				console.log("SUCCESS: ", data);
				displayDemandes(data);	
				displayPaginationDemandes(app);
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
	      });
}

function displayDemandes(data) {
	if(data!=""){
	    var test="";
		$.each(data, function(i, item) {
			if(item.traitee!=0){
				test=test+
				'<tr style="background: #d2e8c7;"><td class =\"appSelected\" hidden=\"true\">'+item.id_demande+'</td><td>'+item.application.name+'</td><td>'+item.demande+'</td><td>'+item.date+'</td><td>'+item.email+'</td><td ><a class=\"material-icons\">done</a></td></tr>'			 
			}else{
				test=test+
				'<tr style="background: #fcdcd9;"><td class =\"appSelected\" hidden=\"true\">'+item.id_demande+'</td><td>'+item.application.name+'</td><td>'+item.demande+'</td><td>'+item.date+'</td><td>'+item.email+'</td><td onclick=\"{demandeResUp(\''+item.id_demande+'\',\''+item.demande+'\')}\"><a href=#><i class=\"material-icons\" data-toggle=\"modal\" data-target=\"#updateModal\" >thumb_up</i></a></td></tr>'			 
			}
		});			        
		 var html="<thead class=\"text-warning\">" +
	 		"<tr  style=\"color: #899fb4\">" +
	 		"<th hidden=\"true\"></th><th>Application</th><th>Demande</th><th>Date</th><th>Email</th><th>Demande r&eacutesolue</th>" +
	 		"</tr></thead><tbody id=\"feedback\">" +test+"</tbody>";
		$('.tableDemandes').html(html);	
		
		
	}else{
		var json = "<h4>Aucune demande trouv&eacute;e!</h4>";
		$('.tableDemandes').html(json);	
	}
}

/*$('.demandeRes').click(function(e) {	
	alert("ok");
	var name = $(this).parents("td").prevAll().eq(2).text();
	alert(name);
	var application=$(this).parents("td").prevAll().eq(3).text();
	$('.demandeSlected').text(name);
	$('.demandeSelectedResol').val(name);
	$('.applicatonName').val(application);

});*/


function demandeResUp(id_demande,demande){
	$('.demandeSlected').text(demande);
	$('.demandeSelectedResol').val(id_demande);
}

function displayDataDashboard(app){
		$.ajax({
	        type: 'GET',
	        url: 'dashboard/'+app+'?page=0',
	        success : function(data) {
				console.log("SUCCESS: ", data);
				displayDashboard(data);				
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
	      });

	
}

function displayDashboard(data) {
if(data!=""){
    var test="";
	$.each(data, function(i, item) {
		test=test+
		'<tr><td>'+item.forApplication.name+'</td><td>'+item.createdBy.login+'</td><td>'+item.action+'</td></tr>'			 
	});	
  
	 var html="<thead class=\"text-warning\">" +
 		"<tr  style=\"color: #899fb4\">" +
 		"</th><th>Application</th><th>Login</th><th>Action</th>" +
 		"</tr></thead><tbody id=\"feedback\">" +test+"</tbody>";
	$('.histBackOffice').html(html);	
	
	
}else{
	var json = "<h4>Aucune historique trouv&eacute;e!</h4>";
	$('.histBackOffice').html(json);	
}
}

